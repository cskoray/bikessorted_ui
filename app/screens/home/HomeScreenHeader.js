import React, { useEffect, useState } from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import colors from "../../config/colors";

export default function HomeScreenHeader(props) {
  const { data } = props;

  const [profilePicture, setProfilePicture] = useState(
    "https://solidbike.s3.eu-west-2.amazonaws.com/users/profiles/default/default.png"
  );
  const [name, setName] = useState("");
  const [points, setPoints] = useState(0);

  useEffect(() => {
    if (data) {
      setProfilePicture(data.profilePicture);
      setName(data.name);
      setPoints(data.point);
    }
  }, [data]);

  return (
    <View style={styles.top}>
      <View style={styles.prof}>
        <Image
          style={styles.profImage}
          source={{
            uri: profilePicture,
          }}
        />
        <Text style={styles.txtName}>Hi, {name}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  top: {
    flex: 0.5,
    width: "100%",
    backgroundColor: colors.darkGray,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      height: 8,
    },
    shadowOpacity: 0.2,
  },
  prof: {
    flexDirection: "row",
    paddingLeft: "15%",
    top: 5,
    
  },
  profImage: {
    width: 45,
    height: 45,
    borderRadius: 24,
    overflow: "hidden",
  },
  txtName: {
    color: colors.newWhite,
    padding: 20,
    fontSize: 16,
    fontFamily: "popRegular",
  },
  announce: {
    color: colors.newWhite,
    left: 20,
  },
});
