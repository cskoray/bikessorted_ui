import React from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  Modal,
  Button,
} from "react-native";
import Constants from "expo-constants";
import { CardField } from "@stripe/stripe-react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import colors from "../config/colors";

export default function SwitchPaymentMethod(props) {
  return (
    <Modal animationType="slide" visible={props.modalVisible}>
      <KeyboardAwareScrollView>
        <View
          style={{
            marginTop: Constants.statusBarHeight,
          }}
        >
          <TouchableOpacity
            style={{
              padding: 0,
              margin: 0,
              width: "100%",
              left: 0,
              flexDirection: "row",
              alignContent: "center",
              marginBottom: 20,
            }}
            onPress={props.closeModel}
          >
            <Image
              source={require("../assets/png/ArrowCloseCircle.png")}
              style={styles.ImageIconStyle}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "popSemiBold",
              fontSize: 16,
              alignSelf: "center",
            }}
          >
            Payment Methods
          </Text>
          {props.methods &&
            props.methods.paymentMethods.map((method) => (
              <TouchableOpacity
                onPress={() => props.pickCard(method)}
                key={method.last4}
              >
                <View style={styles.card}>
                  {method.brand === "visa" ? (
                    <Image
                      style={styles.image}
                      tint="light"
                      source={require("../assets/png/Visa.png")}
                      resizeMode="center"
                    />
                  ) : (
                    <Image
                      style={styles.image}
                      tint="light"
                      source={require("../assets/png/master.png")}
                      resizeMode="center"
                    />
                  )}
                  <View style={styles.detailsContainer}>
                    <Text style={styles.title} numberOfLines={1}>
                      Ending {method.last4}
                    </Text>
                  </View>
                  <Image
                    style={styles.image2}
                    tint="light"
                    source={require("../assets/rightarrow.png")}
                    resizeMode="contain"
                  />
                </View>
              </TouchableOpacity>
            ))}
          <Text
            style={{
              alignSelf: "center",
              top: 20,
              fontFamily: "popSemiBold",
              fontSize: 18,
            }}
          >
            or
          </Text>
          <Text
            style={{
              alignSelf: "center",
              top: 20,
              fontFamily: "popSemiBold",
              fontSize: 18,
            }}
          >
            add new
          </Text>
          <CardField
            postalCodeEnabled={false}
            placeholder={{
              number: "4242 4242 4242 4242",
            }}
            cardStyle={{
              backgroundColor: "#FFFFFF",
              textColor: "#000000",
            }}
            style={{
              width: "100%",
              height: 50,
              marginVertical: 30,
            }}
          />
          <Button
            onPress={props.savePaymentMethod}
            title="Save"
            loading={props.loading}
          />
        </View>
      </KeyboardAwareScrollView>
    </Modal>
  );
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  card: {
    backgroundColor: colors.newWhite,
    marginBottom: 20,
    overflow: "hidden",
    flexDirection: "row",
    top: 20,
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: 40,
    height: 40,
    alignSelf: "center",
  },
  title: {
    fontSize: 16,
    fontFamily: "popRegular",
  },
  image2: {
    width: 30,
    height: 15,
    alignSelf: "center",
    left: 90,
  },
});
