import { useEffect, useState } from "react";
import * as TaskManager from "expo-task-manager";
import * as Location from "expo-location";
import authStorage from "../auth/storage";
import locationApi from "../api/location";

export default riderLocation = () => {
  const [location, setLocation] = useState();
  const [user, setUser] = useState();

  const TASK_FETCH_LOCATION = "TASK_FETCH_LOCATION";

  TaskManager.defineTask(
    TASK_FETCH_LOCATION,
    async ({ data: { locations }, error }) => {
      if (error) {
        console.error(error);
        return;
      }
      const [location] = locations;
      try {
        if (user && user.scope === "RIDER") {
          let loc = {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          };
          locationApi.updateRiderLocation(loc);
        }
      } catch (err) {
        console.error(err);
      }
    }
  );

  Location.startLocationUpdatesAsync(TASK_FETCH_LOCATION, {
    accuracy: Location.Accuracy.Highest,
    distanceInterval: 10, // minimum change (in meters) betweens updates
    deferredUpdatesInterval: 10000, // minimum interval (in milliseconds) between updates
    showsBackgroundLocationIndicator: false,
    foregroundService: {
      notificationTitle: "Using your location",
      notificationBody:
        "If you turn location service off, you cannot get new job notifications.",
    },
    pausesUpdatesAutomatically: false,
    activityType: Location.ActivityType.AutomotiveNavigation,
  });

  useEffect(() => {
    const getUser = async () => {
      const user = await authStorage.getUser();
      if (user) {
        setUser(user);
      }
    };
    const config = async () => {
      const { granted: bgGranted } =
        await Location.requestBackgroundPermissionsAsync();
      if (!bgGranted) {
        return alert(
          "Location Access Required",
          "App requires location even when the App is backgrounded."
        );
      }
    };
    config();
    getUser();
  }, []);

  return location;
};
