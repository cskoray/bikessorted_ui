import React from "react";
import { Image, View, StyleSheet, TouchableOpacity } from "react-native";

import Text from "./Text";
import colors from "../config/colors";
function WalletCard({ brand, number, onPress }) {
  return (
    <View
      style={{
        flex: 1,
      }}
    >
      <TouchableOpacity
        onPress={onPress}
        style={{
          elevation: 15,
          shadowRadius: 2,
          shadowOffset: {
            width: 2,
            height: 2,
          },
          shadowOpacity: 0.2,
          borderRadius: 10,
        }}
      >
        <View style={styles.card}>
          {brand === "visa" ? (
            <Image
              style={styles.image}
              tint="light"
              source={require("../assets/png/Visa.png")}
              resizeMode="contain"
            />
          ) : (
            <Image
              style={styles.image}
              tint="light"
              source={require("../assets/png/master.png")}
              resizeMode="contain"
            />
          )}
          <View style={styles.detailsContainer}>
            <Text style={styles.title} numberOfLines={1}>
              Ending {number}
            </Text>
          </View>
          <Image
            style={styles.image2}
            tint="light"
            source={require("../assets/rightarrow.png")}
            resizeMode="contain"
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: colors.white,
    overflow: "hidden",
    flexDirection: "row",
    margin: 10,
    padding: 5,
    borderRadius: 10,
  },
  detailsContainer: {
    padding: 20,
  },
  image2: {
    width: 30,
    height: 15,
    alignSelf: "center",
    left: 150,
  },
  image: {
    width: 40,
    height: 40,
    alignSelf: "center",
  },
  title: {
    fontSize: 16,
    fontFamily: "popRegular",
  },
});

export default WalletCard;
