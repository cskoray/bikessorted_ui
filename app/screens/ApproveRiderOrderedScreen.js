import React, { useEffect, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import { Avatar } from "react-native-paper";
import { useConfirmSetupIntent } from "@stripe/stripe-react-native";

import colors from "../config/colors";
import bookingApi from "../api/booking";
import routes from "../navigation/routes";
import Screen from "../components/Screen";
import paymentApi from "../api/payment";
import SwitchPaymentMethod from "./SwitchPaymentMethod";

function ApproveRiderOrderedScreen({ route, navigation }) {
  const [tip, setTip] = useState(0);
  const [tipType, setTipType] = useState("Z");
  const [paymentMethodId, setPaymentMethodId] = useState("-");
  const [methods, setMethods] = useState();
  const [last4, setLast4] = useState();
  const [brand, setBrand] = useState();
  const [saveLoading, setSaveLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [actionTaken, setActionTaken] = useState(false);
  const { confirmSetupIntent, loading } = useConfirmSetupIntent();
  const [originalTotal, setOriginalTotal] = useState(route.params.total);
  const [total, setTotal] = useState(route.params.total);
  const { services, riderName, bookingKey, pic } = route.params;

  useEffect(() => {
    const getMethods = async () => {
      setSaveLoading(true);
      const result = await paymentApi.getPaymentMethods();
      setSaveLoading(false);
      if (result.data && result.data.paymentMethods.length > 0) {
        setMethods(result.data);
        setLast4(result.data.paymentMethods[0].last4);
        setBrand(result.data.paymentMethods[0].brand);
        setPaymentMethodId(result.data.paymentMethods[0].id);
      }
    };
    getMethods();
  }, []);

  const reject = async () => {
    const result = await bookingApi.rejectNotSureServiceRiderOrder();
    navigation.navigate(routes.HOME);
  };

  const accept = async () => {
    setActionTaken(true);
    const result = await bookingApi.approveNotSureServiceRiderOrder(
      paymentMethodId,
      tipType
    );
    if (result.ok) {
      alert("Thanks for booking! Now technician can fix your bicycle");
      navigation.navigate(routes.HOME);
    } else {
      alert(
        "Seems we are having troubles processing your booking! Please try again in a few mins"
      );
      navigation.navigate(routes.HOME);
    }
  };

  const addTip = (val, tipType) => {
    if (tip === val) {
      setTip(0);
      setTotal(originalTotal);
      setTipType("Z");
    } else {
      setTip(val);
      setTotal(originalTotal + val);
      setTipType(tipType);
    }
  };

  const change = () => {
    setModalVisible(true);
  };

  const pickCard = (method) => {
    setPaymentMethodId(method.id);
    setLast4(method.last4);
    setModalVisible(false);
  };

  const savePaymentMethod = async () => {
    setSaveLoading(true);
    const result = await paymentApi.setupIntent();
    setSaveLoading(false);
    const clientSecret = result.data;
    const { setupIntent, error } = await confirmSetupIntent(clientSecret, {
      type: "Card",
    });
    if (error) {
      console.log(error);
    }
    if (setupIntent && setupIntent.status === "Succeeded") {
      setSaveLoading(true);
      const result = await paymentApi.getPaymentMethods();
      setSaveLoading(false);
      if (result.data) {
        setMethods(result.data);
        const newMethod = result.data.paymentMethods.filter(
          (m) => m.id !== result.data.paymentMethodId
        );
        setLast4(newMethod.last4);
        setBrand(newMethod.brand);
        setPaymentMethodId(newMethod.id);
        setModalVisible(false);
      }
    } else {
      alert("something went wrong");
      setModalVisible(false);
    }
  };
  const closeModel = () => {
    setModalVisible(false);
  };

  return (
    <Screen>
      <View
        style={{
          flexDirection: "row",
          padding: 15,
          backgroundColor: colors.darkGray,
        }}
      >
        <Avatar.Image
          source={{
            uri: pic,
          }}
          size={35}
        />
        <Text
          style={{
            fontSize: 18,
            fontFamily: "popRegular",
            color: colors.white,
            left: 15,
            alignSelf: "center",
          }}
        >
          Technician {riderName} ordered below services
        </Text>
      </View>
      <View style={{ top: 10 }}>
        {services.map((service) => (
          <View
            style={{
              alignSelf: "center",
              backgroundColor: colors.newWhite,
              padding: 10,
              marginTop: 20,
              elevation: 15,
              shadowRadius: 5,
              shadowOffset: {
                width: 5,
                height: 10,
              },
              shadowOpacity: 0.2,
              width: "95%",
              borderRadius: 10,
              flexDirection: "row",
            }}
          >
            <Text
              style={{
                fontFamily: "popSemiBold",
                fontSize: 18,
              }}
            >
              {service.serviceName}
            </Text>
            <Text
              style={{
                fontFamily: "popRegular",
                fontSize: 18,
                marginLeft: "auto",
              }}
            >
              £{service.amount}
            </Text>
          </View>
        ))}
        <View
          style={{
            top: 15,
            width: "100%",
            backgroundColor: colors.white,
          }}
        >
          <Text
            style={{
              fontSize: 16,
              fontFamily: "popSemiBold",
              left: 15,
            }}
          >
            Tip your service technician
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={tip === 1 ? styles.tipSelected : styles.tip}
              onPress={() => addTip(1, "A")}
            >
              <Text style={tip === 1 ? styles.tipTxtSelected : styles.tipTxt}>
                £ 1
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={tip === 2 ? styles.tipSelected : styles.tip}
              onPress={() => addTip(2, "B")}
            >
              <Text style={tip === 2 ? styles.tipTxtSelected : styles.tipTxt}>
                £ 2
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={tip === 5 ? styles.tipSelected : styles.tip}
              onPress={() => addTip(5, "C")}
            >
              <Text style={tip === 5 ? styles.tipTxtSelected : styles.tipTxt}>
                £ 5
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            backgroundColor: colors.darkGray,
            flexDirection: "row",
            padding: 10,
            top: 20,
          }}
        >
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popSemiBold",
              fontSize: 22,
              alignSelf: "center",
              left: 10,
            }}
          >
            Total
          </Text>
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 20,
              alignSelf: "center",
              marginLeft: "auto",
              right: 10,
            }}
          >
            £{total}
          </Text>
        </View>
      </View>
      <View
        style={{
          backgroundColor: colors.white,
          margin: 10,
          elevation: 15,
          shadowRadius: 5,
          shadowOffset: {
            width: 5,
            height: 10,
          },
          shadowOpacity: 0.2,
          borderRadius: 10,
          top: 50,
        }}
      >
        <TouchableOpacity
          onPress={change}
          style={{ backgroundColor: colors.white, borderRadius: 10 }}
        >
          <Text
            style={{
              left: "80%",
              fontSize: 16,
              fontFamily: "popSemiBold",
              color: colors.blue,
            }}
          >
            change
          </Text>
        </TouchableOpacity>
        <View
          style={{
            width: "90%",
          }}
        >
          <Text style={{ fontFamily: "popSemiBold", fontSize: 16, left: 15 }}>
            Payment Methods
          </Text>
          <TouchableOpacity>
            <View style={styles.card}>
              {brand === "visa" ? (
                <Image
                  style={styles.image}
                  tint="light"
                  source={require("../assets/png/Visa.png")}
                  resizeMode="contain"
                />
              ) : (
                <Image
                  style={styles.image}
                  tint="light"
                  source={require("../assets/png/master.png")}
                  resizeMode="contain"
                />
              )}
              <View style={styles.detailsContainer}>
                <Text
                  numberOfLines={1}
                  style={{ fontSize: 18, fontFamily: "popSemiBold" }}
                >
                  Ending {last4}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ flexDirection: "row", top: 20 }}>
        <TouchableOpacity
          style={!actionTaken ? styles.acceptBtn : styles.acceptBtnDisabled}
          onPress={accept}
        >
          <Text
            style={
              !actionTaken ? styles.acceptBtnTxt : styles.acceptBtnDisabledTxt
            }
          >
            Accept
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={!actionTaken ? styles.cancelBtn : styles.cancelBtnDisabled}
          onPress={reject}
        >
          <Text
            style={
              !actionTaken ? styles.cancelBtnTxt : styles.cancelBtnDisabledTxt
            }
          >
            Reject
          </Text>
        </TouchableOpacity>
      </View>
      <SwitchPaymentMethod
        methods={methods}
        modalVisible={modalVisible}
        loading={loading}
        closeModel={closeModel}
        pickCard={pickCard}
        savePaymentMethod={savePaymentMethod}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  acceptBtn: {
    padding: 20,
    margin: 20,
    width: "55%",
    backgroundColor: colors.blue,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
    },
    shadowOpacity: 0.8,
    borderRadius: 10,
    top: 50,
  },
  acceptBtnTxt: {
    color: colors.newWhite,
    fontFamily: "popSemiBold",
    fontSize: 22,
    alignSelf: "center",
  },
  acceptBtnDisabledTxt: {
    color: colors.black,
    fontFamily: "popSemiBold",
    fontSize: 20,
    alignSelf: "center",
  },
  acceptBtnDisabled: {
    padding: 20,
    margin: 20,
    width: "55%",
    backgroundColor: colors.light,
    borderRadius: 10,
    top: 50,
  },
  cancelBtn: {
    padding: 20,
    margin: 20,
    width: "30%",
    backgroundColor: colors.darkGray,
    borderRadius: 10,
    top: 50,
    marginLeft: "auto",
  },
  cancelBtnDisabled: {
    padding: 20,
    margin: 20,
    width: "30%",
    backgroundColor: colors.light,
    borderRadius: 10,
    top: 50,
    marginLeft: "auto",
  },
  cancelBtnTxt: {
    color: colors.newWhite,
    fontFamily: "popSemiBold",
    fontSize: 20,
    alignSelf: "center",
  },
  cancelBtnDisabledTxt: {
    color: colors.black,
    fontFamily: "popSemiBold",
    fontSize: 20,
    alignSelf: "center",
  },
  card: {
    marginBottom: 20,
    overflow: "hidden",
    flexDirection: "row",
    top: 10,
    backgroundColor: colors.newWhite,
    width: "100%",
    margin: 5,
    borderRadius: 10,
  },
  detailsContainer: {
    padding: 20,
    left: 10,
  },
  image: {
    width: 40,
    height: 40,
    alignSelf: "center",
    left: 10,
  },
  tip: {
    width: 50,
    height: 50,
    backgroundColor: colors.newWhite,
    justifyContent: "center",
    margin: 10,
    borderRadius: 5,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.2,
  },
  tipSelected: {
    width: 50,
    height: 50,
    backgroundColor: colors.darkGray,
    justifyContent: "center",
    margin: 10,
    borderRadius: 5,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.4,
  },
  tipTxt: {
    fontSize: 20,
    fontFamily: "popRegular",
    alignSelf: "center",
    color: colors.black,
  },
  tipTxtSelected: {
    fontSize: 20,
    fontFamily: "popRegular",
    alignSelf: "center",
    color: colors.white,
  },
});

export default ApproveRiderOrderedScreen;
