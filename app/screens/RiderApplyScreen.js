import React, { useState, useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, Image } from "react-native";

import Screen from "../components/Screen";
import colors from "../config/colors";
import routes from "../navigation/routes";
import usersApi from "../api/users";

function RiderApplyScreen({ navigation }) {
  const [applied, setApplied] = useState(false);
  const [isAppliedBefore, setIsAppliedBefore] = useState(false);

  useEffect(() => {
    const isApplied = async () => {
      const result = await usersApi.isApplied();
      setIsAppliedBefore(result.data.applied);
    };
    isApplied();
  }, []);

  const apply = () => {
    if (!applied || !isAppliedBefore) {
      usersApi.applyAsRider();
      setApplied(true);
    }
  };

  const close = () => {
    navigation.navigate(routes.HOME);
  };
  return (
    <Screen>
      <TouchableOpacity
        style={{
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          marginBottom: 20,
          height: 60,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
        </>
      </TouchableOpacity>
      <Text style={{ fontFamily: "popSemiBold", fontSize: 20, padding: 10 }}>
        Become a technician
      </Text>
      <Text style={{ fontFamily: "popRegular", fontSize: 16, left: 15 }}>
        {"\u2022"} Have a new profession
      </Text>
      <Text style={{ fontFamily: "popRegular", fontSize: 16, left: 15 }}>
        {"\u2022"} Make extra money
      </Text>
      <Text style={{ fontFamily: "popRegular", fontSize: 16, left: 15 }}>
        {"\u2022"} Work anytime
      </Text>
      <Text style={{ fontFamily: "popRegular", fontSize: 16, left: 15 }}>
        {"\u2022"} Get paid fast
      </Text>
      <Image
        source={require("../assets/png/LondonCity.png")}
        style={{ width: 300, height: 200, alignSelf: "center", top: 20 }}
        resizeMode="contain"
      />
      <TouchableOpacity
        onPress={apply}
        style={!applied && !isAppliedBefore ? styles.btnBlue : styles.btnDark}
        disabled={applied || isAppliedBefore}
      >
        {!applied && !isAppliedBefore ? (
          <Text
            style={{
              color: colors.white,
              fontFamily: "popBold",
              fontSize: 16,
              alignSelf: "center",
            }}
          >
            Apply to become a technician
          </Text>
        ) : (
          <>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popBold",
                fontSize: 16,
                alignSelf: "center",
              }}
            >
              Thanks!
            </Text>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popBold",
                fontSize: 16,
                alignSelf: "center",
              }}
            >
              We have received your application
            </Text>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popBold",
                fontSize: 16,
                alignSelf: "center",
                top: 30,
              }}
            >
              We will get back to you
            </Text>
          </>
        )}
      </TouchableOpacity>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: "10%",
    justifyContent: "center",
  },
  ImageIconStyle: {
    margin: 10,
    width: 40,
    height: 40,
    top: 0,
  },
  btnBlue: { backgroundColor: colors.blue, padding: "10%", margin: 30 },
  btnDark: { backgroundColor: colors.darkGray, padding: "10%", margin: 30 },
});

export default RiderApplyScreen;
