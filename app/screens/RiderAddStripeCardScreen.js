import React, { useState } from "react";
import {
  Button,
  TouchableOpacity,
  Image,
  StyleSheet,
  Text,
} from "react-native";
import { CardField, useConfirmSetupIntent } from "@stripe/stripe-react-native";

import routes from "../navigation/routes";
import Screen from "../components/Screen";
import paymentApi from "../api/payment";
import profileApi from "../api/profile";
import useApi from "../hooks/useApi";
import colors from "../config/colors";

function RiderAddStripeCardScreen({ navigation }) {
  const { confirmSetupIntent, loading } = useConfirmSetupIntent();
  const getDetailsApi = useApi(profileApi.getDetails);
  const [saveLoading, setSaveLoading] = useState(false);

  const savePaymentMethod = async () => {
    setSaveLoading(true);
    const result = await paymentApi.setupIntent();
    setSaveLoading(false);
    const clientSecret = result.data;
    const { setupIntent, error } = await confirmSetupIntent(clientSecret, {
      type: "Card",
    });
    if (error) {
      console.log(error);
    }

    const params = { refer: "newPaymentMethod" };
    navigation.navigate(routes.RiderWallet, params);
  };

  const onPressAction = () => {
    navigation.navigate(routes.RiderWallet);
  };

  return (
    <Screen>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          alignContent: "center",
          marginBottom: 20,
        }}
        onPress={onPressAction}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />

          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            New Payment Method
          </Text>
        </>
      </TouchableOpacity>
      <CardField
        postalCodeEnabled={false}
        placeholder={{
          number: "4242 4242 4242 4242",
        }}
        cardStyle={{
          backgroundColor: "#FFFFFF",
          textColor: "#000000",
        }}
        style={{
          width: "100%",
          height: 50,
          marginVertical: 30,
        }}
      />
      <Button
        onPress={savePaymentMethod}
        title="Save payment method"
        loading={loading}
        disabled={loading}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
});

export default RiderAddStripeCardScreen;
