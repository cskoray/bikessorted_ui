import client from "./client";

const getPublishableKey = () => client.get("/payment/publishable-key");

const getPaymentMethods = () => client.get("/payment/get-payment-methods");

const getPayouts = (from, to) => {
  const params = { from: from, to: to };
  return client.post("/admin/payout", params);
};

export const paymentIntent = (servicesKeys) => {
  return client.post("/payment/create-payment-intent", servicesKeys);
};

export const setupIntent = () => {
  return client.get("/payment/setup-intent");
};

export const setupIntentRider = () => {
  return client.get("/payment/rider/setup-intent");
};

export const chargeRiderSetupCosts = () => {
  return client.get("/payment/rider/charge");
};

export const removePaymentMethod = (id) => {
  const params = { id: id };
  return client.post("/payment/remove", params);
};

export default {
  paymentIntent,
  getPublishableKey,
  setupIntent,
  setupIntentRider,
  getPaymentMethods,
  removePaymentMethod,
  getPayouts,
  chargeRiderSetupCosts,
};
