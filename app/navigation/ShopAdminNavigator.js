import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";

import colors from "../config/colors";
import ShopAdminHomeNavigator from "./ShopAdminHomeNavigator";
import ShopAdminDrawerContent from "../screens/drawer/ShopAdminDrawerContent";

const Drawer = createDrawerNavigator();

const ShopAdminNavigator = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <ShopAdminDrawerContent {...props} />}
      drawerStyle={{
        backgroundColor: colors.blue,
        width: "80%",
        shadowRadius: 5,
        shadowOffset: {
          width: 5,
          height: 10,
        },
        shadowOpacity: 0.2,
        flex: 1,
      }}
      hideStatusBar="true"
    >
      <Drawer.Screen name="Booking" component={ShopAdminHomeNavigator} />
    </Drawer.Navigator>
  );
};

export default ShopAdminNavigator;
