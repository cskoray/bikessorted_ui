import React, { useEffect } from "react";
import { StyleSheet, View, Image } from "react-native";
import { Avatar, Title, Drawer } from "react-native-paper";

import { DrawerItem } from "@react-navigation/drawer";
import colors from "../../config/colors";
import profileApi from "../../api/profile";
import routes from "../../navigation/routes";
import useAuth from "../../auth/useAuth";

function AdminDrawerContent(props) {
  const getDetailsApi = useApi(profileApi.getDetails);
  const auth = useAuth();

  useEffect(() => {
    getDetailsApi.request();
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
          <View style={{ flexDirection: "row", alignContent: "center" }}>
            <Avatar.Image
              source={{
                uri: getDetailsApi.data.profilePicture,
              }}
              size={50}
            />
            <View
              style={{
                marginLeft: 15,
                flexDirection: "column",
                alignContent: "center",
              }}
            >
              <Title style={styles.title}>{getDetailsApi.data.name}</Title>
            </View>
          </View>
          <Image
            source={require("../../assets/png/frame.png")}
            style={{
              height: 75,
              width: 75,
              margin: 0,
              position: "absolute",
              top: 48,
              left: 12,
            }}
            resizeMode="contain"
          />
          <View style={styles.row}></View>
        </View>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Home"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.AdminHome);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Riders"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.Admin_Riders);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Users"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.Admin_Users);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Bookings"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.Admin_Bookings);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Payouts"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.Admin_Payouts);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section
          style={{ top: 30, borderTopWidth: 1, borderBottomWidth: 1 }}
        >
          <DrawerItem
            icon={() => (
              <Image
                source={require("../../assets/png/Iconly-Light-outline-Add-User.png")}
                style={{ height: 25, width: 25 }}
                resizeMode="contain"
              />
            )}
            label="Add New Rider"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular", left: -25 }}
            onPress={() => {
              props.navigation.navigate(routes.Admin_New_Rider);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section
          style={{ top: 30, borderTopWidth: 1, borderBottomWidth: 1 }}
        >
          <DrawerItem
            icon={() => (
              <Image
                source={require("../../assets/png/Iconly-Light-outline-Category.png")}
                style={{ height: 25, width: 25 }}
                resizeMode="contain"
              />
            )}
            label="Add New Shop"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular", left: -25 }}
            onPress={() => {
              props.navigation.navigate(routes.Admin_New_Shop);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
      </View>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={() => (
            <Image
              source={require("../../assets/png/Iconly-Light-outline-Logout.png")}
              style={{ height: 15, width: 15 }}
              resizeMode="contain"
            />
          )}
          label="logout"
          labelStyle={{ fontSize: 13, fontFamily: "popRegular" }}
          onPress={() => auth.logOut()}
          inactiveTintColor={colors.newWhite}
        />
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    backgroundColor: colors.blue,
  },
  userInfoSection: {
    paddingTop: 70,
    paddingLeft: 30,
    backgroundColor: colors.darkGray,
    borderBottomColor: colors.newWhite,
    borderBottomWidth: 0.5,
  },
  title: {
    fontSize: 16,
    marginTop: 15,
    left: 10,
    fontWeight: "bold",
    color: colors.newWhite,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    color: colors.newWhite,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  bottomDrawerSection: {
    backgroundColor: colors.blue,
    height: 80,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

export default AdminDrawerContent;
