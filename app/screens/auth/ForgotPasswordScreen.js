import React, { useState, useEffect } from "react";
import {
  Animated,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  View,
} from "react-native";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { Platform } from "react-native";
import CountDown from "react-native-countdown-component";

import authApi from "../../api/auth";
import Screen from "../../components/Screen";
import colors from "../../config/colors";
import authStorage from "../../auth/storage";
import routes from "../../navigation/routes";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";

const { Value, Text: AnimatedText } = Animated;
const CELL_COUNT = 6;
const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({ hasValue, index, isFocused }) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

function ForgotPasswordScreen({ route, navigation }) {
  const [user, setUser] = useState();
  const [userStatus, setUserStatus] = useState("");
  const [userTriedCodeStatus, setUserTriedCodeStatus] = useState("");
  const [codeGenerated, setCodeGenerated] = useState(false);
  const [value, setValue] = useState("");
  const [resendDisabled, setResendDisabled] = useState(true);
  const [timez, setTimez] = useState("0");
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  const [verificationFailed, setVerificationFailed] = useState(false);
  const auth = useAuth();

  useEffect(() => {
    if (route && route.params) {
      const verifyUserStatus = async () => {
        const result = await authApi.verifyUserStatus(route.params.userInfo);
        if (result.ok) {
          setUserStatus(result.data.status);
          if (result.data.status === "ACTIVE") {
            const result = await authApi.generatePasswordCode(
              route.params.userInfo
            );
            if (result.ok) {
              return setCodeGenerated(true);
            } else {
              return setUserStatus("NOT_FOUND");
            }
          }
        } else {
          return setUserStatus("NOT_FOUND");
        }
      };
      verifyUserStatus();
    }
  }, [navigation]);

  const handleSubmit = async (code) => {
    setUserTriedCodeStatus("");
    if (code) {
      const result = await authApi.verifyPasswordCode(
        route.params.userInfo,
        code
      );
      if (result && result.ok) {
        if (result.data.status === "VALID") {
          const params = { code: code, email: route.params.userInfo };
          navigation.navigate("ChangePassword", params);
        } else if (result.data.status === "SUSPENDED") {
          setUserStatus("SUSPENDED");
        } else {
          setUserTriedCodeStatus(result.data.status);
        }
      }
    }
  };

  const handleCodeAndroid = async () => {
    setUserTriedCodeStatus("");
    if (value) {
      const result = await authApi.verifyPasswordCode(
        route.params.userInfo,
        value
      );
      if (result && result.ok) {
        if (result.data.status === "VALID") {
          const params = { code: value, email: route.params.userInfo };
          navigation.navigate("ChangePassword", params);
        } else if (result.data.status === "SUSPENDED") {
          setUserStatus("SUSPENDED");
        } else {
          setUserTriedCodeStatus(result.data.status);
        }
      }
    }
  };

  const close = () => {
    navigation.navigate(routes.LOGIN);
    setUser(null);
    authStorage.removeToken();
  };

  const timeUpCancel = async () => {
    setResendDisabled(false);
  };

  const resend = async () => {
    let n = Number(timez);
    n = n + 1;
    setTimez(String(n));
    setResendDisabled(true);
    if (n >= 3) {
      const result = await authApi.suspendUser("PASSWORD");
      if (result && result.ok) {
        auth.logIn(result.data);
        const user = await authStorage.getUser();
        setUser(user);
      }
    } else {
      const result = await authApi.resendForgotPassword(route.params.userInfo);
      if (result && !result.ok && result.status === 400) {
        return setUserStatus("INVALID");
      }
    }
  };

  const renderCell = ({ index, symbol, isFocused }) => {
    const hasValue = Boolean(symbol);
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: ["#3557b7", "#f7fafe"],
          })
        : animationsColor[index].interpolate({
            inputRange: [0, 1],
            outputRange: ["#fff", "#f7fafe"],
          }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [55, 8],
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.2, 1],
          }),
        },
      ],
    };

    // Run animation on next event loop tik
    // Because we need first return new style prop and then animate this value
    setTimeout(() => {
      animateCell({ hasValue, index, isFocused });
    }, 0);

    return (
      <AnimatedText
        key={index}
        style={[styles.cell, animatedCellStyle]}
        onLayout={getCellOnLayoutHandler(index)}
      >
        {symbol || (isFocused ? <Cursor /> : null)}
      </AnimatedText>
    );
  };

  return (
    <>
      {userStatus && userStatus === "ACTIVE" ? (
        <Screen style={styles.container}>
          <CountDown
            id={timez}
            size={19}
            until={59}
            onFinish={timeUpCancel}
            digitStyle={{
              backgroundColor: colors.light,
              bottom: 9,
            }}
            timeLabelStyle={{ fontWeight: "normal" }}
            separatorStyle={{ bottom: 10, color: colors.gray }}
            timeToShow={["M", "S"]}
            timeLabels={{ m: null, s: null }}
            digitTxtStyle={{ color: colors.gray }}
            showSeparator
          />
          {userTriedCodeStatus === "INVALID" && (
            <Text style={{ alignSelf: "center", color: "red" }}>
              invalid code
            </Text>
          )}
          {Platform.OS === "ios" ? (
            <>
              <OTPInputView
                style={{ width: "100%", height: 100 }}
                pinCount={6}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled={(code) => handleSubmit(code)}
              />
              <Text style={{ alignSelf: "center" }}>
                We have sent a verification code to your email
              </Text>
              <TouchableOpacity onPress={resend} disabled={resendDisabled}>
                <Text
                  style={
                    resendDisabled
                      ? styles.resendDisabled
                      : styles.resendEnabled
                  }
                >
                  resend code
                </Text>
              </TouchableOpacity>
            </>
          ) : (
            <>
              <CodeField
                ref={ref}
                {...props}
                value={value}
                onChangeText={setValue}
                cellCount={CELL_COUNT}
                rootStyle={styles.codeFiledRoot}
                keyboardType="number-pad"
                textContentType="oneTimeCode"
                renderCell={renderCell}
              />
              <TouchableOpacity onPress={handleCodeAndroid}>
                <View style={styles.nextButton}>
                  <Text style={styles.nextButtonText}>Verify</Text>
                </View>
              </TouchableOpacity>
            </>
          )}
        </Screen>
      ) : (
        <>
          {userStatus === "INVALID" ? (
            <Screen style={styles.limitContainer}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  left: 0,
                  width: "50%",
                  flexDirection: "row",
                  justifyContent: "center",
                  top: 20,
                }}
                onPress={close}
              >
                <>
                  <Image
                    source={require("../../assets/png/Iconly-Light-outline-Home.png")}
                    style={styles.ImageIconStyle}
                    resizeMode="contain"
                  />
                </>
              </TouchableOpacity>
              <View style={{ top: 50 }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  {route.params.userInfo} is not activated yet
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 10,
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  Please contact
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 20,
                    fontFamily: "popBold",
                    fontSize: 24,
                  }}
                >
                  info@rubiebikes.com
                </Text>
              </View>
            </Screen>
          ) : userStatus === "NOT_FOUND" ? (
            <Screen style={styles.limitContainer}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  left: 0,
                  width: "50%",
                  flexDirection: "row",
                  justifyContent: "center",
                  top: 20,
                }}
                onPress={close}
              >
                <>
                  <Image
                    source={require("../../assets/png/Iconly-Light-outline-Home.png")}
                    style={styles.ImageIconStyle}
                    resizeMode="contain"
                  />
                </>
              </TouchableOpacity>
              <View style={{ top: 50 }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  {route.params.userInfo} is not found
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 10,
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  Please contact
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 20,
                    fontFamily: "popBold",
                    fontSize: 24,
                  }}
                >
                  info@rubiebikes.com
                </Text>
              </View>
            </Screen>
          ) : (
            <Screen style={styles.limitContainer}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  left: 0,
                  width: "50%",
                  flexDirection: "row",
                  justifyContent: "center",
                  top: 20,
                }}
                onPress={close}
              >
                <>
                  <Image
                    source={require("../../assets/png/Iconly-Light-outline-Home.png")}
                    style={styles.ImageIconStyle}
                    resizeMode="contain"
                  />
                </>
              </TouchableOpacity>
              <View style={{ top: 50 }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  {route.params.userInfo} is suspended
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 10,
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  Please contact
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 20,
                    fontFamily: "popBold",
                    fontSize: 24,
                  }}
                >
                  info@rubiebikes.com
                </Text>
              </View>
            </Screen>
          )}
        </>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
    justifyContent: "center",
  },
  limitContainer: {
    padding: 10,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
  },
  resendEnabled: {
    alignSelf: "center",
    color: colors.blue,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  resendDisabled: {
    alignSelf: "center",
    color: colors.gray,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
    backgroundColor: colors.newWhite,
    borderRadius: 20,
    right: "100%",
  },
  underlineStyleBase: {
    width: 50,
    height: 50,
    backgroundColor: colors.gray,
    borderRadius: 5,
    alignContent: "flex-start",
    borderWidth: 0,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
  },

  underlineStyleHighLighted: {
    backgroundColor: colors.light,
    borderWidth: 0,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
  },
});

export default ForgotPasswordScreen;
