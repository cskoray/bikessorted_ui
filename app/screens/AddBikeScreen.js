import React, { useState, useEffect } from "react";
import { StyleSheet, Image, TouchableOpacity, View } from "react-native";
import * as Yup from "yup";
import {
  Form,
  FormField,
  FormPicker as Picker,
  SubmitButton,
} from "../components/forms";

import BikeTypePickerItem from "../components/BikeTypePickerItem";
import TyreTypePickerItem from "../components/TyreTypePickerItem";
import Screen from "../components/Screen";
import FormImagePicker from "../components/forms/FormImagePicker";
import bikesApi from "../api/bikes";
import UploadScreen from "./UploadScreen";
import routes from "../navigation/routes";
import colors from "../config/colors";

const validationSchema = Yup.object().shape({
  name: Yup.string().required().min(1).label("name"),
  type: Yup.object().required().label("type"),
  tyreType: Yup.object().required().label("tyreType"),
  images: Yup.array().min(1, "Please select at least one image."),
});

function AddBikeScreen({ route, navigation }) {
  const { refresh } = route.params;
  const [uploadVisible, setUploadVisible] = useState(false);
  const [progress, setProgress] = useState(0);
  const getBikeTypesApi = useApi(bikesApi.getBikeTypes);
  const getTyreTypesApi = useApi(bikesApi.getTyreTypes);
  const [tyreServiceSelected, setTyreServiceSelected] = useState(false);

  useEffect(() => {
    getBikeTypesApi.request();
    getTyreTypesApi.request();
  }, []);

  const back = () => {
    const params = {
      setTyreServiceSelected: () => setTyreServiceSelected(false),
    };
    navigation.navigate(routes.BIKES, params);
  };

  const handleSubmit = async (bike, { resetForm }) => {
    setProgress(0);
    setUploadVisible(true);

    const result = await bikesApi.addBike(bike, (progress) =>
      setProgress(progress)
    );
    if (result && result.ok) {
      return setUploadVisible(false);
    }
    refresh();
    resetForm();
    const params = {
      setTyreServiceSelected: () => setTyreServiceSelected(false),
    };
    navigation.navigate(routes.BIKES, params);
  };

  return (
    <Screen style={styles.container}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          flexDirection: "row",
          alignContent: "center",
          marginBottom: 20,
        }}
        onPress={back}
      >
        <Image
          source={require("../assets/png/ArrowLeftCircle.png")}
          style={styles.ImageIconStyle}
        />
      </TouchableOpacity>
      <View style={{ flex: 1 }}>
        <UploadScreen
          onDone={() => setUploadVisible(false)}
          progress={progress}
          visible={uploadVisible}
        />
        <Form
          enableReinitialize
          initialValues={{
            name: "",
            tyreType: "",
            type: "",
            images: [],
          }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
          onDone={refresh}
        >
          <FormImagePicker name="images" />
          <FormField
            maxLength={25}
            name="name"
            backColor={colors.newWhite}
            noIcon="true"
            placeholder="name your bike"
            width="100%"
          />
          <Picker
            pickerType="bike"
            items={getBikeTypesApi.data.types}
            name="type"
            numberOfColumns={3}
            PickerItemComponent={BikeTypePickerItem}
            placeholder="type of bike"
            width="100%"
          />
          <Picker
            pickerType="tyre"
            items={getTyreTypesApi.data.types}
            name="tyreType"
            numberOfColumns={3}
            PickerItemComponent={TyreTypePickerItem}
            placeholder="tyre types"
            width="100%"
          />
          <SubmitButton
            title="Add Bike"
            style={{
              backgroundColor: "black",
              borderRadius: 0,
              height: 60,
              padding: 20,
              alignSelf: "center",
            }}
          />
        </Form>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    padding: 15,
  },
  ImageIconStyle: {
    width: 40,
    height: 40,
    top: 0,
  },
});
export default AddBikeScreen;
