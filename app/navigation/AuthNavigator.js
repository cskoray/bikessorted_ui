import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import LoginScreen from "../screens/auth/LoginScreen";
import RegisterScreen from "../screens/auth/RegisterScreen";
import AddPhoneNumberScreen from "../screens/auth/AddPhoneNumberScreen";
import CheckEmailActivatedScreen from "../screens/auth/CheckEmailActivatedScreen";
import CheckPhoneNumberActivatedScreen from "../screens/auth/CheckPhoneNumberActivatedScreen";
import ForgotPasswordScreen from "../screens/auth/ForgotPasswordScreen";
import ChangePasswordScreen from "../screens/auth/ChangePasswordScreen";
import GetEmailForgotPasswordScreen from "../screens/auth/GetEmailForgotPasswordScreen";

const Stack = createStackNavigator();

const AuthNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Login" component={LoginScreen} />
    <Stack.Screen name="Register" component={RegisterScreen} />
    <Stack.Screen
      name="CheckEmailActivated"
      component={CheckEmailActivatedScreen}
    />
    <Stack.Screen name="AddPhoneNumber" component={AddPhoneNumberScreen} />
    <Stack.Screen
      name="CheckPhoneNumberActivated"
      component={CheckPhoneNumberActivatedScreen}
    />
    <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
    <Stack.Screen name="ChangePassword" component={ChangePasswordScreen} />
    <Stack.Screen
      name="GetEmailForgotPassword"
      component={GetEmailForgotPasswordScreen}
    />
  </Stack.Navigator>
);

export default AuthNavigator;
