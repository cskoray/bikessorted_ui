import React, { useEffect, useState, useRef } from "react";
import {
  Alert,
  AppState,
  Image,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import Modal from "react-native-modal";
import { Avatar } from "react-native-paper";

import Constants from "expo-constants";
import profileApi from "../api/profile";
import servicesApi from "../api/services";
import useApi from "../hooks/useApi";
import routes from "../navigation/routes";
import DrawerToggle from "./drawer/DrawerToggle";
import HomeScreenHeader from "./home/HomeScreenHeader";
import AllTypeServicesComponent from "./home/AllTypeServicesComponent";
import EmergencyHelpPopup from "./home/EmergencyHelpPopup";
import RoadStaticImageView from "./home/RoadStaticImageView";
import ServicePopup from "./home/ServicePopup";
import ServiceDetailPopup from "./home/ServiceDetailPopup";
import useAuth from "../auth/useAuth";
import useNotification from "../hooks/useNotification";
import * as Notifications from "expo-notifications";
import colors from "../config/colors";
import ActivityIndicatorSmall from "../components/ActivityIndicatorSmall";
import InfoIndicator from "../components/InfoIndicator";
import bookingApi from "../api/booking";
import RateRiderPopup from "./home/RateRiderPopup";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

function HomeScreen({ navigation }) {
  const { booking, setBooking } = useAuth();
  const getDetailsApi = useApi(profileApi.getDetails);
  const getEmergencyServiceApi = useApi(servicesApi.getEmergencyService);
  const getServicesApi = useApi(servicesApi.getServices);
  const serviceRefs = useRef([]);
  const [activeBookingsModal, setActiveBookingsModal] = useState(false);

  const [bookingToRate, setBookingToRate] = useState();
  const [activeBookings, setActiveBookings] = useState();
  const [requestedBookings, setRequestedBookings] = useState();
  const [laterActiveBookings, setLaterActiveBookings] = useState();
  const [riderOrdered, setRiderOrdered] = useState();
  const [ratingPopup, setRatingPopup] = useState(false);
  const [emergencySelected, setEmergencySelected] = useState(false);
  const [servicePopUpSelected, setServicePopUpSelected] = useState(false);
  const [serviceDetailSelected, setServiceDetailSelected] = useState();
  const [arrival, setArrival] = useState();
  const [serviceToOrder, setServiceToOrder] = useState([]);
  const [serviceDetailPopUpSelected, setServiceDetailPopUpSelected] =
    useState(false);
  const notification = useNotification();
  const [notif, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  const checkIfRatingNeeded = async () => {
    const result = await bookingApi.getLastNotRatedBooking();
    if (result && result.data) {
      setRatingPopup(true);
      setBookingToRate(result.data);
    }
  };

  useEffect(() => {
    getUserBookings();
    const unsubscribe = navigation.addListener("focus", () => {
      setServiceDetailSelected();
      setServiceToOrder([]);
      setEmergencySelected(false);
      setServicePopUpSelected(false);
      getUserBookings();
      getDetailsApi.request();
      checkIfRatingNeeded();
    });
    const stt = AppState.addEventListener("change", handleAppStateChange);
    getDetailsApi.request();
    getEmergencyServiceApi.request();
    getServicesApi.request();

    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        if (response.notification.request.content.data.type === "USER") {
          if (
            response.notification.request.content.data.notificationType ===
            "book"
          ) {
            const name = response.notification.request.content.data.name;
            const pic = response.notification.request.content.data.pic;
            const params = { name, pic };
            navigation.navigate(routes.BookingConfirmation, params);
          } else if (
            response.notification.request.content.data.notificationType ===
            "cancel"
          ) {
            const cancelReason =
              response.notification.request.content.data.cancelReason;
            const params = { cancelReason };
            navigation.navigate(routes.BookingCancelled, params);
          } else if (
            response.notification.request.content.data.notificationType ===
            "notSureRiderOrder"
          ) {
            const services =
              response.notification.request.content.data.services;
            const riderName =
              response.notification.request.content.data.riderName;
            const bookingKey =
              response.notification.request.content.data.bookingKey;
            const total = response.notification.request.content.data.total;
            const pic = response.notification.request.content.data.pic;
            const params = { services, riderName, bookingKey, total, pic };
            navigation.navigate(routes.ApproveRiderOrdered, params);
          }
        }
      });
    return () => {
      if (stt) {
        stt.remove(); //NotificationSubscription("change", handleAppStateChange);
      }
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  const handleAppStateChange = async (nextAppState) => {
    if (nextAppState === "active" || nextAppState === "inactive") {
      getUserBookings();
      getDetailsApi.request();
      checkIfRatingNeeded();
    }
  };

  const getUserBookings = async () => {
    const requestedBookins = await bookingApi.getRequestedBookings();
    const activeBookins = await bookingApi.getActiveBookings();
    const laterActiveBookins = await bookingApi.getLaterActiveBookings();

    if (requestedBookins && requestedBookins.ok) {
      setRequestedBookings(requestedBookins.data);
    }
    if (activeBookins && activeBookins.ok) {
      setActiveBookings(activeBookins.data);
    }
    if (laterActiveBookins && laterActiveBookins.ok) {
      setLaterActiveBookings(laterActiveBookins.data);
    }
    if (
      (activeBookins && activeBookins.ok) ||
      (laterActiveBookins && laterActiveBookins.ok)
    ) {
      const riderOrd = await bookingApi.getRiderOrdered();
      if (riderOrd) {
        if (riderOrd.ok) {
          setRiderOrdered(riderOrd.data);
        }
      }
    }
  };

  const cancelEmergencySelected = () => {
    setEmergencySelected(false);
  };

  const orderEmergencyNow = () => {
    if (booking) {
      booking.service.services = getEmergencyServiceApi.data[0];
      booking.service.serviceKeys = getEmergencyServiceApi.data[0].serviceKey;
      booking.service.type = "INSTANT";
      booking.points.amount = getDetailsApi.data.point;
      setBooking(booking);
    }
    navigation.navigate(routes.BIKES);
  };

  const cancelRating = async () => {
    setRatingPopup(false);
    bookingApi.dontAskRatingAgain(bookingToRate.bookingKey);
  };

  const rateRider = async (rate, yesNoSelection, name) => {
    let yesNoTxt = "You have not paid any cash";
    let title = "Rating " + name + " " + rate + "/5";
    if (yesNoSelection === "yes") {
      yesNoTxt = "You paid or offered to pay cash";
    }

    Alert.alert(
      title,
      yesNoTxt,
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Confirm",
          onPress: async () => {
            setRatingPopup(false);
            bookingApi.rateBooking(
              bookingToRate.bookingKey,
              rate,
              yesNoSelection
            );
          },
        },
      ],
      { cancelable: false }
    );
  };

  const cancelServiceSelected = () => {
    setServicePopUpSelected(false);
    setServiceToOrder([]);
  };

  const cancelServiceDetailPopUpSelected = () => {
    setServiceDetailPopUpSelected(false);
  };

  const serviceSelected = (service) => {
    setServiceDetailSelected(service);
    setServiceDetailPopUpSelected(true);
  };

  const ifServiceSelected = (serviceKey) => {
    if (serviceToOrder.length === 0) return false;
    return serviceToOrder.find((service) => service.serviceKey === serviceKey);
  };

  const goToRiderOrdered = () => {
    const services = riderOrdered.services;
    const riderName = riderOrdered.riderName;
    const bookingKey = riderOrdered.bookingKey;
    const total = riderOrdered.total;
    const pic = riderOrdered.pic;
    const params = {
      services,
      riderName,
      bookingKey,
      total,
      pic,
    };
    console.log(params);
    navigation.navigate(routes.ApproveRiderOrdered, params);
  };

  const showActiveBookingsModal = () => {
    setActiveBookingsModal(true);
  };

  const closeActiveBookingsModal = () => {
    setActiveBookingsModal(false);
  };

  const goToActiveBookings = (bkn) => {
    setActiveBookingsModal(false);
    const bookingKey = bkn.bookingKey;
    const totalAmount = bkn.totalAmount;
    const latitude = bkn.latitude;
    const longitude = bkn.longitude;
    const type = bkn.type;
    const bikeType = bkn.bikeType;
    const tyreSize = bkn.tyreSize;
    const name = bkn.name;
    const riderPic = bkn.pic;
    const createdDate = bkn.createdDate;
    const dueDate = bkn.dueDate;
    const services = bkn.services;
    const riderRate = bkn.riderRate;
    const params = {
      bookingKey,
      totalAmount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      riderPic,
      riderRate,
      createdDate,
      dueDate,
      services,
    };
    if (bkn.type === "INSTANT") {
      navigation.navigate(routes.ActiveBookingDetail, params);
    } else {
      navigation.navigate(routes.ActiveLaterBookingDetail, params);
    }
  };

  const goToActiveBooking = () => {
    let bkn = activeBookings.bookings[0];
    const bookingKey = bkn.bookingKey;
    const totalAmount = bkn.totalAmount;
    const latitude = bkn.latitude;
    const longitude = bkn.longitude;
    const type = bkn.type;
    const bikeType = bkn.bikeType;
    const tyreSize = bkn.tyreSize;
    const name = bkn.name;
    const riderPic = bkn.pic;
    const createdDate = bkn.createdDate;
    const dueDate = bkn.dueDate;
    const services = bkn.services;
    const riderRate = bkn.riderRate;
    const params = {
      bookingKey,
      totalAmount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      riderPic,
      riderRate,
      createdDate,
      dueDate,
      services,
    };
    navigation.navigate(routes.ActiveBookingDetail, params);
  };

  const goToLaterActiveBooking = () => {
    let bkn = laterActiveBookings.bookings[0];
    const bookingKey = bkn.bookingKey;
    const totalAmount = bkn.totalAmount;
    const latitude = bkn.latitude;
    const longitude = bkn.longitude;
    const type = bkn.type;
    const bikeType = bkn.bikeType;
    const tyreSize = bkn.tyreSize;
    const name = bkn.name;
    const riderPic = bkn.pic;
    const createdDate = bkn.createdDate;
    const dueDate = bkn.dueDate;
    const services = bkn.services;
    const riderRate = bkn.riderRate;
    const params = {
      bookingKey,
      totalAmount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      riderPic,
      riderRate,
      createdDate,
      dueDate,
      services,
    };
    navigation.navigate(routes.ActiveLaterBookingDetail, params);
  };

  const goToRequestedBooking = () => {
    const bookingKey = requestedBookings.bookingKey;
    const params = {
      bookingKey,
    };
    navigation.navigate(routes.BookingConfirmation, params);
  };

  const servicesSelected = () => {
    if (serviceToOrder.length === 0) {
      alert("please select at least one service");
    } else {
      if (booking) {
        let keys = [];
        for (let i = 0; i < serviceToOrder.length; i++) {
          keys.push(serviceToOrder[i].serviceKey);
        }
        booking.service.services = serviceToOrder;
        booking.service.serviceKeys = keys;
        booking.service.type = "LATER";
        booking.points.amount = getDetailsApi.data.point;
        setBooking(booking);
        navigation.navigate(routes.CALENDAR);
      }
    }
  };

  return (
    <View style={{ paddingTop: Constants.statusBarHeight, flex: 1 }}>
      {(activeBookings && activeBookings.bookings.length > 1) ||
      (laterActiveBookings && laterActiveBookings.bookings.length > 1) ||
      (activeBookings &&
        activeBookings.bookings.length >= 1 &&
        laterActiveBookings &&
        laterActiveBookings.bookings.length >= 1) ? (
        <TouchableOpacity
          style={{
            width: "100%",
            flex: 0.6,
            backgroundColor: colors.yellow,
            flexDirection: "row",
          }}
          onPress={showActiveBookingsModal}
        >
          <ActivityIndicatorSmall visible={true} />
          <Text
            style={{
              alignSelf: "center",
              left: 60,
              fontFamily: "popSemiBold",
              fontSize: 20,
            }}
          >
            Active bookings
          </Text>
          <Image
            source={require("../assets/png/Iconly-Light-outline-Arrow-RightCircle.png")}
            style={{
              resizeMode: "contain",
              width: 23,
              height: 26,
              left: 80,
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>
      ) : activeBookings && activeBookings.bookings.length === 1 ? (
        <TouchableOpacity
          style={{
            width: "100%",
            flex: 0.6,
            backgroundColor: colors.yellow,
            flexDirection: "row",
          }}
          onPress={goToActiveBooking}
        >
          <ActivityIndicatorSmall visible={true} />
          <Text
            style={{
              alignSelf: "center",
              left: 60,
              fontFamily: "popRegular",
            }}
          >
            your service is on the way!
          </Text>
          <Image
            source={require("../assets/png/Iconly-Light-outline-Arrow-RightCircle.png")}
            style={{
              resizeMode: "contain",
              width: 23,
              height: 26,
              left: 80,
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>
      ) : (
        laterActiveBookings &&
        laterActiveBookings.bookings.length === 1 && (
          <TouchableOpacity
            style={{
              width: "100%",
              flex: 0.6,
              backgroundColor: colors.yellow,
              flexDirection: "row",
            }}
            onPress={goToLaterActiveBooking}
          >
            <ActivityIndicatorSmall visible={true} />
            <Text
              style={{
                alignSelf: "center",
                left: 60,
                fontFamily: "popRegular",
              }}
            >
              Service is due{" "}
            </Text>
            <Text
              style={{
                alignSelf: "center",
                left: 60,
                fontFamily: "popSemiBold",
              }}
            >
              {laterActiveBookings.bookings[0].dueDate}
            </Text>
            <Image
              source={require("../assets/png/Iconly-Light-outline-Arrow-RightCircle.png")}
              style={{
                resizeMode: "contain",
                width: 23,
                height: 26,
                left: 80,
                alignSelf: "center",
              }}
            />
          </TouchableOpacity>
        )
      )}
      {requestedBookings && (
        <TouchableOpacity
          style={{
            width: "100%",
            flex: 0.6,
            backgroundColor: colors.blue,
            flexDirection: "row",
          }}
          onPress={goToRequestedBooking}
        >
          <ActivityIndicatorSmall visible={true} />
          <Text
            style={{
              alignSelf: "center",
              left: 60,
              fontFamily: "popRegular",
              color: colors.white,
            }}
          >
            finding the nearest technician for you!
          </Text>
          <Image
            source={require("../assets/png/Iconly-Light-outline-Arrow-RightCircle.png")}
            style={{
              resizeMode: "contain",
              width: 23,
              height: 26,
              left: 80,
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>
      )}
      {riderOrdered && (
        <TouchableOpacity
          style={{
            width: "100%",
            flex: 0.6,
            backgroundColor: colors.info,
            flexDirection: "row",
          }}
          onPress={goToRiderOrdered}
        >
          <InfoIndicator visible={true} />
          <Text
            style={{
              alignSelf: "center",
              left: 60,
              fontFamily: "popSemiBold",
              color: colors.white,
            }}
          >
            Check services ordered for you!
          </Text>
          <Text
            style={{
              alignSelf: "center",
              left: 70,
              fontFamily: "popSemiBold",
              fontSize: 22,
              color: colors.white,
            }}
          >
            {">>"}
          </Text>
        </TouchableOpacity>
      )}
      <DrawerToggle navigation={navigation} />
      <HomeScreenHeader data={getDetailsApi.data} />
      <AllTypeServicesComponent
        setArrival={setArrival}
        setEmergencySelected={setEmergencySelected}
        setServicePopUpSelected={setServicePopUpSelected}
      />
      <RoadStaticImageView />
      {emergencySelected && (
        <EmergencyHelpPopup
          cancelEmergencySelected={cancelEmergencySelected}
          arrival={arrival}
          data={getEmergencyServiceApi.data}
          orderEmergencyNow={orderEmergencyNow}
        />
      )}
      {servicePopUpSelected && (
        <ServicePopup
          cancelServiceSelected={cancelServiceSelected}
          serviceSelected={serviceSelected}
          serviceRefs={serviceRefs}
          ifServiceSelected={ifServiceSelected}
          setServiceToOrder={setServiceToOrder}
          data={getServicesApi.data}
          serviceToOrder={serviceToOrder}
          servicesSelected={servicesSelected}
        />
      )}
      {serviceDetailPopUpSelected && serviceDetailSelected && (
        <ServiceDetailPopup
          cancelServiceDetailPopUpSelected={cancelServiceDetailPopUpSelected}
          serviceDetailSelected={serviceDetailSelected}
        />
      )}
      {ratingPopup && bookingToRate && (
        <RateRiderPopup
          rider={bookingToRate}
          cancelRating={cancelRating}
          rateRider={rateRider}
        />
      )}
      <StatusBar style="dark" />
      <Modal
        animationIn="zoomInUp"
        isVisible={activeBookingsModal}
        hasBackdrop={true}
      >
        <View
          style={{
            flex: 0.6,
            backgroundColor: colors.white,
            elevation: 15,
            shadowRadius: 10,
            shadowOffset: {
              width: 2,
              height: 2,
            },
            shadowOpacity: 0.2,
            borderRadius: 10,
          }}
        >
          <View
            style={{
              flex: 0.1,
              backgroundColor: colors.white,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
            }}
          >
            <TouchableOpacity
              style={{ marginVertical: 5, flexDirection: "row" }}
              onPress={closeActiveBookingsModal}
            >
              <Text
                style={{
                  fontFamily: "popSemiBold",
                  fontSize: 20,
                  left: 15,
                  alignSelf: "center",
                }}
              >
                Active bookings
              </Text>
              <Image
                source={require("../assets/png/ArrowCloseCircle.png")}
                style={{
                  width: 30,
                  height: 30,
                  alignSelf: "center",
                  marginLeft: "auto",
                  right: 5,
                }}
              />
            </TouchableOpacity>
          </View>
          <ScrollView style={{ flex: 1 }}>
            {activeBookings &&
              activeBookings.bookings &&
              activeBookings.bookings.length > 0 &&
              activeBookings.bookings.map((booking) => (
                <View
                  style={{
                    flexDirection: "row",
                    backgroundColor: colors.newWhite,
                    margin: 10,
                    elevation: 15,
                    shadowRadius: 5,
                    shadowOffset: {
                      width: 2,
                      height: 10,
                    },
                    shadowOpacity: 0.2,
                    borderRadius: 10,
                    padding: 10,
                  }}
                  key={booking.bookingKey}
                >
                  <View
                    style={{
                      padding: 5,
                      width: "30%",
                    }}
                  >
                    <Avatar.Image
                      source={{
                        uri: booking.pic,
                      }}
                      size={45}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: "popSemiBold",
                        color: colors.darkGray,
                      }}
                    >
                      {booking.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: "popSemiBold",
                        color: colors.darkGray,
                        top: 5,
                      }}
                    >
                      £{booking.totalAmount}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => goToActiveBookings(booking)}
                    style={{ marginLeft: "auto" }}
                  >
                    <View>
                      <Text
                        style={{
                          fontFamily: "popSemiBold",
                          fontSize: 20,
                          left: 5,
                        }}
                      >
                        {booking.dueDate}
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                        }}
                      >
                        <View style={{ width: "60%" }}>
                          {booking.services.map((service) => (
                            <Text
                              style={{
                                fontFamily: "popRegular",
                                fontSize: 22,
                                left: 5,
                              }}
                            >
                              {service.name}{" "}
                            </Text>
                          ))}
                        </View>
                        <Image
                          source={require("../assets/png/Iconly-Light-outline-Arrow-Right-2.png")}
                          style={{
                            resizeMode: "contain",
                            width: 23,
                            height: 26,
                            alignSelf: "center",
                            marginLeft: "auto",
                            backgroundColor: colors.darkGray,
                            borderRadius: 5,
                          }}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              ))}
            {laterActiveBookings &&
              laterActiveBookings.bookings &&
              laterActiveBookings.bookings.length > 0 &&
              laterActiveBookings.bookings.map((booking) => (
                <View
                  style={{
                    flexDirection: "row",
                    backgroundColor: colors.newWhite,
                    margin: 10,
                    elevation: 15,
                    shadowRadius: 5,
                    shadowOffset: {
                      width: 2,
                      height: 10,
                    },
                    shadowOpacity: 0.2,
                    borderRadius: 10,
                    padding: 10,
                  }}
                  key={booking.bookingKey}
                >
                  <View
                    style={{
                      padding: 5,
                      width: "30%",
                    }}
                  >
                    <Avatar.Image
                      source={{
                        uri: booking.pic,
                      }}
                      size={45}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: "popSemiBold",
                        color: colors.darkGray,
                      }}
                    >
                      {booking.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: "popSemiBold",
                        color: colors.darkGray,
                        top: 5,
                      }}
                    >
                      £{booking.totalAmount}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => goToActiveBooking(booking)}
                    style={{ marginLeft: "auto" }}
                  >
                    <View>
                      <Text
                        style={{
                          fontFamily: "popSemiBold",
                          fontSize: 20,
                          left: 5,
                        }}
                      >
                        {booking.dueDate}
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                        }}
                      >
                        <View style={{ width: "60%" }}>
                          {booking.services.map((service) => (
                            <Text
                              style={{
                                fontFamily: "popRegular",
                                fontSize: 22,
                                left: 5,
                              }}
                            >
                              {service.name}{" "}
                            </Text>
                          ))}
                        </View>
                        <Image
                          source={require("../assets/png/Iconly-Light-outline-Arrow-Right-2.png")}
                          style={{
                            resizeMode: "contain",
                            width: 23,
                            height: 26,
                            alignSelf: "center",
                            marginLeft: "auto",
                            backgroundColor: colors.darkGray,
                            borderRadius: 5,
                          }}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              ))}
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
}

export default HomeScreen;
