import React from "react";
import LottieView from "lottie-react-native";
import { View, StyleSheet } from "react-native";

function RiderBookingDetailInfoIndicator({ visible = false }) {
  if (!visible) return null;

  return (
    <View style={styles.overlay}>
      <LottieView
        autoPlay
        loop
        source={require("../assets/animations/info.json")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  overlay: {
    position: "absolute",
    height: "100%",
    opacity: 1,
    width: "40%",
    zIndex: 1,
    left:-10
  },
});

export default RiderBookingDetailInfoIndicator;
