import React from "react";
import { View, Image } from "react-native";

import colors from "../config/colors";

function Icon({
  name,
  backgroundColor = colors.darkGray,
  alignSelf = "center",
}) {
  return (
    <View
      style={{
        backgroundColor,
        justifyContent: "center",
        alignItems: "center",
        alignSelf,
      }}
    >
      <Image source={name} style={{ width: 11, height: 13 }} />
    </View>
  );
}

export default Icon;
