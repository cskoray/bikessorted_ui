import client from "./client";

const isValidLocation = (lat, long) =>
  client.get(`/location/${lat}/${long}/valid`);

export const updateRiderLocation = (location) => {
  return client.post("/location", location);
};

const getRiderLocation = (bookingKey) => client.get(`/location/${bookingKey}`);

export const getPostcodeFromLocation = async (lat, long) => {
  fetch(`https://api.postcodes.io/postcodes?lon=${long}&lat=${lat}`)
    .then((resp) => resp.json())
    .then((data) => {
      return data.result[0].postcode;
    })
    .catch((err) => console.error(err));
};

export default {
  updateRiderLocation,
  getPostcodeFromLocation,
  isValidLocation,
  getRiderLocation,
};
