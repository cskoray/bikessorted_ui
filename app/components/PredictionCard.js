import React from "react";
import { View, StyleSheet, TouchableWithoutFeedback } from "react-native";

import Text from "./Text";
import colors from "../config/colors";

function PredictionCard({ description, onPress }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.card}>
        <View style={styles.detailsContainer}>
          <Text>{description}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 0,
    backgroundColor: colors.light,
    marginBottom: 5,
    overflow: "hidden",
  },
  detailsContainer: {
    padding: 12,
  },
});

export default PredictionCard;
