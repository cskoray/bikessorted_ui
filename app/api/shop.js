import client from "./client";

const getShops = () => client.get(`/admin/shops`);

export const addNewShop = (
  shopInfo,
  startTime,
  finishTime,
  selectedDays,
  onUploadProgress
) => {
  const data = new FormData();
  data.append("shopName", shopInfo.shopName);
  data.append("ownerName", shopInfo.ownerName);
  data.append("ownerEmail", shopInfo.ownerEmail);
  data.append("ownerPhone", shopInfo.ownerPhone);
  data.append("postcode", shopInfo.postcode);
  data.append("startTime", startTime);
  data.append("finishTime", finishTime);

  selectedDays.forEach((selectedDay) =>
    data.append("selectedDays", selectedDay)
  );

  shopInfo.images.forEach((image, index) =>
    data.append("images", {
      name: "image" + index,
      type: "image/jpeg",
      uri: image,
    })
  );
  return client
    .post("/admin/shop", data, {
      onUploadProgress: (progress) =>
        onUploadProgress(progress.loaded / progress.total),
    })
    .then(console.log);
};

export default {
  addNewShop,
  getShops,
};
