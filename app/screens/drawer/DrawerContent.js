import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, TouchableOpacity, Text } from "react-native";
import { Avatar, Title, Drawer } from "react-native-paper";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";

import UploadScreen from "../UploadScreen";
import { DrawerItem } from "@react-navigation/drawer";
import colors from "../../config/colors";
import profileApi from "../../api/profile";
import routes from "../../navigation/routes";
import useAuth from "../../auth/useAuth";
import ActivityIndicator from "../../components/ActivityIndicator";

function DrawerContent(props) {
  const [progress, setProgress] = useState(0);
  const [uploadVisible, setUploadVisible] = useState(false);
  const getDetailsApi = useApi(profileApi.getDetails);
  const [emergencySelected, setEmergencySelected] = useState(false);
  const [tyreServiceSelected, setTyreServiceSelected] = useState(false);
  const auth = useAuth();

  useEffect(() => {
    async () => {
      await ImagePicker.requestMediaLibraryPermissionsAsync();
    };
    getDetailsApi.request();
  }, []);

  const pickImage = async () => {
    const pickerResult = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3],
      quality: 1,
    });
    if (!pickerResult.cancelled) {
      setProgress(0);
      setUploadVisible(true);
      const result = await profileApi.addProfilePic(
        pickerResult,
        (progress) => {
          setProgress(progress);
        }
      );
      if (result !== undefined) {
        setUploadVisible(false);
      }
      getDetailsApi.request();
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <ActivityIndicator
        visible={getDetailsApi.loading && getDetailsApi.data.profilePicture}
      />
      <UploadScreen
        onDone={() => setUploadVisible(false)}
        progress={progress}
        visible={uploadVisible}
      />
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
          <TouchableOpacity
            onPress={pickImage}
            style={{
              flexDirection: "row",
              alignContent: "center",
              zIndex: 9999,
            }}
          >
            <Avatar.Image
              source={{
                uri: getDetailsApi.data.profilePicture,
              }}
              size={50}
            />
            <View
              style={{
                marginLeft: 15,
                width: "75%",
              }}
            >
              <Title style={styles.title}>{getDetailsApi.data.name}</Title>
              <View
                style={{
                  position: "absolute",
                  bottom: "1%",
                  right: "1%",
                  height: 90,
                  alignSelf: "flex-end",
                  padding: 10,
                  paddingTop: "25%",
                }}
              >
                <Text
                  style={{
                    color: colors.newWhite,
                    fontSize: 15,
                    fontFamily: "popRegular",
                    alignSelf: "center",
                    width: "100%",
                    left: "1%",
                  }}
                >
                  {getDetailsApi.data.badge}
                </Text>
                <Image
                  source={{ uri: getDetailsApi.data.level }}
                  style={{
                    height: 50,
                    width: 50,
                  }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    color: colors.newWhite,
                    fontSize: 15,
                    fontFamily: "popSemiBold",
                    alignSelf: "center",
                    width: "100%",
                    left: "1%",
                  }}
                >
                  Rubie coins: {getDetailsApi.data.point}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <Image
            source={require("../../assets/png/frame.png")}
            style={{
              height: 75,
              width: 75,
              margin: 0,
              position: "absolute",
              top: 48,
              left: 12,
            }}
            resizeMode="contain"
          />
          <DrawerItem
            style={{
              top: 40,
              left: -30,
              width: "105%",
              backgroundColor: colors.white,
            }}
            icon={() => (
              <Image
                source={require("../../assets/png/Repairman.png")}
                style={{
                  height: 24,
                  width: 24,
                  margin: 0,
                  alignSelf: "flex-start",
                }}
                resizeMode="contain"
              />
            )}
            label="Make money riding"
            labelStyle={{
              fontSize: 16,
              fontFamily: "popRegular",
              left: -25,
              color: colors.darkGray,
            }}
            onPress={() => {
              props.navigation.navigate(routes.RiderApply);
            }}
            inactiveTintColor={colors.newWhite}
          />
          <View style={styles.row}></View>
        </View>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Bookings"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.BOOKING);
            }}
            inactiveTintColor={colors.newWhite}
          />
          <DrawerItem
            label="Bikes"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular", top: 10 }}
            onPress={() => {
              const params = {
                refer: "drawer",
                setEmergencySelected: () => setEmergencySelected(false),
                setTyreServiceSelected: () => setTyreServiceSelected(false),
              };
              props.navigation.navigate(routes.BIKES, params);
            }}
            inactiveTintColor={colors.newWhite}
          />
          <DrawerItem
            label="Wallet"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular", top: 10 }}
            onPress={() => {
              const params = { refer: "drawer" };
              props.navigation.navigate(routes.WALLET, params);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
      </View>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={() => (
            <Image
              source={require("../../assets/png/Iconly-Light-outline-Logout.png")}
              style={{ height: 15, width: 15 }}
              resizeMode="contain"
            />
          )}
          label="logout"
          labelStyle={{ fontSize: 13, fontFamily: "popRegular" }}
          onPress={() => auth.logOut()}
          inactiveTintColor={colors.newWhite}
        />
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    backgroundColor: colors.blue,
  },
  userInfoSection: {
    paddingTop: 70,
    paddingLeft: 30,
    backgroundColor: colors.darkGray,
    borderBottomColor: colors.newWhite,
    borderBottomWidth: 0.5,
  },
  title: {
    fontSize: 16,
    marginTop: 15,
    left: 10,
    fontWeight: "bold",
    color: colors.newWhite,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    color: colors.newWhite,
  },
  row: {
    marginTop: 45,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  bottomDrawerSection: {
    backgroundColor: colors.blue,
    height: 80,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

export default DrawerContent;
