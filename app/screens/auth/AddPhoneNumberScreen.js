import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text } from "react-native";
import * as Yup from "yup";
import { createStackNavigator } from "@react-navigation/stack";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";

import authApi from "../../api/auth";
import Screen from "../../components/Screen";
import colors from "../../config/colors";
import useAuth from "../../auth/useAuth";
import authStorage from "../../auth/storage";
import CheckPhoneNumberActivatedScreen from "./CheckPhoneNumberActivatedScreen";

const validationSchema = Yup.object().shape({
  phone: Yup.string()
    .required()
    .min(11, "phone number starts with 0 and 11 characters")
    .max(11, "phone number starts with 0 and 11 characters")
    .label("phone number"),
});

function AddPhoneNumberScreen({ navigation }) {
  const [user, setUser] = useState();
  const Stack = createStackNavigator();
  const [error, setError] = useState();
  const auth = useAuth();

  useEffect(() => {
    const user = authStorage.getUser();
    setUser(user);
  }, []);

  const handleSubmit = async (phoneNumber) => {
    const result = await authApi.addPhoneNumber(phoneNumber);
    if (result && result.ok) {
      auth.logIn(result.data);
      const user = await authStorage.getUser();
      if (user) {
        setUser(user);
      }
    }
  };

  return (
    <>
      {user && user.status === "PHONE_NOT_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="CheckPhoneNumberActivated"
            component={CheckPhoneNumberActivatedScreen}
          />
        </Stack.Navigator>
      ) : (
        <Screen style={styles.container}>
          <View style={{ alignSelf: "center" }}>
            <Form
              initialValues={{ phone: "" }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <Text style={{ alignSelf: "center", color: colors.gray }}>
                Only UK phone numbers
              </Text>
              <ErrorMessage error={error} visible={error} />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon={require("../../assets/png/Iconly-Light-outline-Call.png")}
                keyboardType="phone-pad"
                name="phone"
                placeholder="i.e 07123456789"
              />
              <View style={{ top: 50, backgroundColor: colors.blue }}>
                <SubmitButton
                  title="Save"
                  style={styles.submitBtn}
                  autoCapitalize="none"
                />
              </View>
            </Form>
          </View>
        </Screen>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingTop: 20,
    justifyContent: "center",
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
});

export default AddPhoneNumberScreen;
