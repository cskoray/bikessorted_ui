import React, { useState, useRef, useEffect } from "react";
import {
  FlatList,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  Keyboard,
  View,
} from "react-native";
import MapView from "react-native-maps";
import Constants from "expo-constants";

import Button from "../components/Button";
import useLocation from "../hooks/useLocation";
import routes from "../navigation/routes";
import colors from "../config/colors";
import PredictionCard from "../components/PredictionCard";
import useAuth from "../auth/useAuth";
import locationApi from "../api/location";

function PickLocationScreen2({ navigation }) {
  const { booking, setBooking } = useAuth();
  const location = useLocation();
  const [latestLocation, setLatestLocation] = useState(location);
  const [predictions, setPredictions] = useState([]);
  const [searchBarTxt, setSearchBarTxt] = useState();
  const [showingPredictions, setSShowingPredictions] = useState(false);
  const searchBarTxtRef = useRef();

  useEffect(() => {
    if (location) {
      setLatestLocation(location);
    }
  }, [location]);

  const mapPressed = () => {
    Keyboard.dismiss();
  };

  const renewLocation = async (val) => {
    const geocodingApiKey = "AIzaSyCCqxNE8EJXJBntyOsCrLCtRaOtTkxVVEw";
    val = encodeURIComponent(val);
    fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?key=${geocodingApiKey}&address=${val}`
    )
      .then((resp) => resp.json())
      .then((data) => {
        setLatestLocation({
          latitude: data.results[0].geometry.location.lat,
          longitude: data.results[0].geometry.location.lng,
        });
      })
      .catch((err) => console.error(err));
  };

  const onDestinationChange = (val) => {
    const placesApiKey = "AIzaSyAGDba7eTaStBJsKQ6je2kpD0oxW1n1Bz8";
    setSShowingPredictions(true);
    setSearchBarTxt(val);
    if (latestLocation && val.length >= 4) {
      fetch(
        `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${placesApiKey}&input=${val}&location=${latestLocation.latitude},${latestLocation.longitude}&radius=2000&country=uk`
      )
        .then((resp) => resp.json())
        .then((data) => {
          setPredictions(data.predictions);
        })
        .catch((err) => console.error(err));
    }
    if (val.length == 0) {
      setSShowingPredictions(false);
    }
  };

  const onPress = (description) => {
    renewLocation(description);
    setSShowingPredictions(false);
    setSearchBarTxt(description);
  };

  const done = async () => {
    booking.location = latestLocation;
    const isValid = await locationApi.isValidLocation(
      latestLocation.latitude,
      latestLocation.longitude
    );
    if (isValid.status === 400) {
      alert(
        "Sorry, currently we are not covering your location but we are working hard to expand"
      );
    } else {
      fetch(
        `https://api.postcodes.io/postcodes?lon=${latestLocation.longitude}&lat=${latestLocation.latitude}`
      )
        .then((resp) => resp.json())
        .then((data) => {
          booking.postcode = data.result[0].postcode;
        })
        .catch((err) => console.error(err));
      setBooking(booking);
      if (booking.service.type === "LATER") {
        navigation.navigate(routes.BIKES);
      } else {
        navigation.navigate(routes.WALLET);
      }
    }
  };

  const close = () => {
    if (booking.service.type === "LATER") {
      navigation.navigate(routes.CALENDAR);
    } else {
      navigation.navigate(routes.BIKES);
    }
  };

  return (
    <>
      {latestLocation && (
        <>
          <MapView
            style={{ flex: 1 }}
            initialRegion={{
              latitude: parseFloat(latestLocation.latitude),
              longitude: parseFloat(latestLocation.longitude),
              latitudeDelta: 0.00222,
              longitudeDelta: 0.00421,
            }}
            mapType="mutedStandard"
            showsMyLocationButton
            zoomControlEnabled
            zoomEnabled
            provider={"google"}
            paddingAdjustmentBehavior="automatic"
            onPress={mapPressed}
            region={{
              latitude: parseFloat(latestLocation.latitude),
              longitude: parseFloat(latestLocation.longitude),
              latitudeDelta: 0.00222,
              longitudeDelta: 0.00421,
            }}
          >
            <MapView.Marker
              coordinate={{
                latitude: parseFloat(latestLocation.latitude),
                longitude: parseFloat(latestLocation.longitude),
              }}
            >
              <Image
                source={require("../assets/png/RubMap.png")}
                style={{
                  height: 80,
                }}
                resizeMode="contain"
              />
            </MapView.Marker>
          </MapView>
          <TouchableOpacity
            style={{
              top: Constants.statusBarHeight,
              position: "absolute",
            }}
            onPress={close}
          >
            <Image
              source={require("../assets/png/Iconly-Light-outline-Arrow-Left.png")}
              style={styles.ImageIconStyle}
            />
          </TouchableOpacity>
          <View style={styles.destination}>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
              }}
            >
              <Image
                source={require("../assets/png/search.png")}
                style={{ width: 20, height: 20 }}
              />
            </View>
            <TextInput
              ref={searchBarTxtRef}
              style={{
                fontFamily: "popBold",
                fontSize: 18,
              }}
              value={searchBarTxt}
              placeholder="Search your location"
              placeholderTextColor={colors.test}
              onChangeText={(val) => onDestinationChange(val)}
            />
          </View>
          {showingPredictions && (
            <FlatList
              style={{
                margin: 5,
                position: "absolute",
                alignContent: "center",
                width: "90%",
                margin: 20,
                marginTop: "50%",
              }}
              data={predictions}
              keyExtractor={(prediction) => prediction.id}
              renderItem={({ item }) => (
                <PredictionCard
                  key={item.id}
                  description={item.description}
                  onPress={() => onPress(item.description)}
                />
              )}
            />
          )}
          <Button style={styles.done} title="Confirm" onPress={done} />
        </>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  destination: {
    height: 70,
    width: "95%",
    padding: 10,
    margin: 10,
    marginTop: Constants.statusBarHeight + 25,
    backgroundColor: colors.white,
    // justifyContent: "center",
    // alignSelf: "flex-start",
    position: "absolute",
    // color: colors.darkGray,
    top: Constants.statusBarHeight,
    elevation: 15,
    shadowRadius: 10,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.5,
    flexDirection: "row",
  },
  done: {
    backgroundColor: colors.black,
    borderRadius: 0,
    height: 60,
    padding: 20,
    alignSelf: "center",
  },
  ImageIconStyle: {
    padding: 10,
    width: 40,
    height: 40,
    left: 20,
  },
});

export default PickLocationScreen2;
