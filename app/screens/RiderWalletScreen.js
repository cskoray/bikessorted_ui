import React, { useState, useEffect } from "react";
import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from "react-native";

import WalletCard from "../components/WalletCard";
import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import paymentApi from "../api/payment";
import ActivityIndicator from "../components/ActivityIndicator";

function RiderWalletScreen({ navigation }) {
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [paymentMethods, setPaymentMethods] = useState();

  useEffect(() => {
    const payments = async () => {
      setLoading(true);
      const res = await paymentApi.getPaymentMethods();
      if (res && res.data) {
        setPaymentMethods(res.data);
        setLoading(false);
      }
    };
    payments();
    const unsubscribe = navigation.addListener("focus", async () => {
      setLoading(true);
      const result = await paymentApi.getPaymentMethods();
      if (result.ok) {
        setPaymentMethods(result.data);
        setLoading(false);
      }
    });
  }, []);

  const addNewCard = () => {
    if (
      paymentMethods &&
      paymentMethods.paymentMethods &&
      paymentMethods.paymentMethods.length > 0
    ) {
      alert("Please remove the existing card");
    } else {
      navigation.navigate(routes.RiderAddStripeCard);
    }
  };

  const removePayment = async (paymentMethod) => {
    let params = {
      id: paymentMethod.id,
      brand: paymentMethod.brand,
      last4: paymentMethod.last4,
      refresh: async () => {
        setLoading(true);
        const asd = await paymentApi.getPaymentMethods();
        if (asd.ok) {
          setPaymentMethods(asd.data);
          setLoading(false);
        }
      },
    };
    navigation.navigate(routes.RiderRemovePaymentMethod, params);
  };

  const onPressAction = () => {
    navigation.navigate(routes.RiderHome);
  };

  return (
    <>
      {paymentMethods &&
      paymentMethods.paymentMethods &&
      paymentMethods.paymentMethods.length > 0 ? (
        <Screen style={styles.screen}>
          <View style={{ flex: 2.8 }}>
            <TouchableOpacity
              style={{
                padding: 0,
                margin: 0,
                width: "100%",
                left: 0,
                backgroundColor: colors.darkGray,
                flexDirection: "row",
                alignContent: "center",
              }}
              onPress={onPressAction}
            >
              <>
                <Image
                  source={require("../assets/png/ArrowCloseCircle.png")}
                  style={styles.ImageIconStyle}
                />
                <Text
                  style={{
                    color: colors.newWhite,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                    left: 20,
                  }}
                >
                  Wallet
                </Text>
              </>
            </TouchableOpacity>
            <ActivityIndicator visible={loading} />
            <View>
              {paymentMethods &&
              paymentMethods.paymentMethods &&
              paymentMethods.paymentMethods.length > 0 ? (
                <FlatList
                  data={paymentMethods.paymentMethods}
                  keyExtractor={(key) => key.last4}
                  refreshing={refreshing}
                  onRefresh={() => paymentMethods.request()}
                  renderItem={({ item }) => (
                    <WalletCard
                      navigation={navigation}
                      refresh={async () => {
                        const asd = await paymentApi.getPaymentMethods();
                        if (asd && asd.data) {
                          setPaymentMethods(asd.data);
                        }
                      }}
                      brand={item.brand}
                      number={item.last4}
                      onPress={() => removePayment(item)}
                    />
                  )}
                />
              ) : (
                <View>
                  <Image
                    source={require("../assets/png/wallet.png")}
                    style={styles.addPaymentMethodImgStyle}
                  />
                </View>
              )}
            </View>
            <TouchableOpacity
              onPress={addNewCard}
              style={{ height: 70, padding: 10 }}
            >
              <View>
                <Text
                  style={{
                    color: colors.darkGray,
                    fontFamily: "popSemiBold",
                    fontSize: 16,
                    alignSelf: "center",
                  }}
                >
                  add new payment method
                </Text>
                <Image
                  source={require("../assets/png/Iconly-Light-outline-Plus.png")}
                  style={styles.addPaymentMethodPlusStyle}
                />
              </View>
            </TouchableOpacity>
          </View>
        </Screen>
      ) : (
        <Screen style={styles.screen}>
          <TouchableOpacity
            style={{
              padding: 0,
              margin: 0,
              width: "100%",
              left: 0,
              backgroundColor: colors.darkGray,
              flexDirection: "row",
              alignContent: "center",
            }}
            onPress={onPressAction}
          >
            <>
              <Image
                source={require("../assets/png/ArrowCloseCircle.png")}
                style={styles.ImageIconStyle}
              />
              <Text
                style={{
                  color: colors.newWhite,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                  left: 20,
                }}
              >
                Wallet
              </Text>
            </>
          </TouchableOpacity>
          <Image
            source={require("../assets/png/wallet.png")}
            style={styles.addPaymentMethodImgStyle}
          />
          <TouchableOpacity
            onPress={addNewCard}
            style={{ height: 70, padding: 10 }}
          >
            <View>
              <Text
                style={{
                  color: colors.darkGray,
                  fontFamily: "popSemiBold",
                  fontSize: 16,
                  alignSelf: "center",
                }}
              >
                add new payment method
              </Text>
              <Image
                source={require("../assets/png/Iconly-Light-outline-Plus.png")}
                style={styles.addPaymentMethodPlusStyle}
              />
            </View>
          </TouchableOpacity>
        </Screen>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  addPaymentMethodImgStyle: {
    width: 322,
    height: 226,
    alignSelf: "center",
    top: 20,
  },
  addPaymentMethodPlusStyle: {
    width: 18,
    height: 18,
    alignSelf: "center",
    top: 10,
    alignSelf: "center",
  },
  viewBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  viewGray: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
});

export default RiderWalletScreen;
