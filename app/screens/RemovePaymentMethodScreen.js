import React, { useState, useEffect } from "react";
import {
  Alert,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from "react-native";

import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import paymentApi from "../api/payment";

function RemovePaymentMethodScreen({ route, navigation }) {
  const [id, setId] = useState();
  const [brand, setBrand] = useState();
  const [last4, setLast4] = useState();

  useEffect(() => {
    if (route && route.params) {
      setId(route.params.id);
      setBrand(route.params.brand);
      setLast4(route.params.last4);
    }
  }, []);

  const removeCard = () => {
    Alert.alert(
      "Delete payment method",
      "Are you sure want to delete payment method?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            paymentApi.removePaymentMethod(id);
            navigation.navigate(routes.WALLET);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const back = () => {
    navigation.navigate(routes.WALLET);
  };

  return (
    <>
      {id && (
        <Screen style={styles.screen}>
          <View style={{ flex: 2.8 }}>
            <TouchableOpacity
              style={{
                padding: 0,
                margin: 0,
                width: "100%",
                left: 0,
                backgroundColor: colors.darkGray,
                flexDirection: "row",
                alignContent: "center",
                marginBottom: 20,
              }}
              onPress={back}
            >
              <>
                <Image
                  source={require("../assets/png/ArrowLeftCircle.png")}
                  style={styles.ImageIconStyle}
                />
                <Text
                  style={{
                    color: colors.newWhite,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                    left: 20,
                  }}
                >
                  Remove Payment Method
                </Text>
              </>
            </TouchableOpacity>
            <View>
              <Text
                style={{ fontFamily: "popSemiBold", fontSize: 16, padding: 20 }}
              >
                Payment Method
              </Text>
            </View>
            <View style={styles.card}>
              {brand === "visa" ? (
                <Image
                  style={styles.image}
                  tint="light"
                  source={require("../assets/png/Visa.png")}
                  resizeMode="center"
                />
              ) : (
                <Image
                  style={styles.image}
                  tint="light"
                  source={require("../assets/png/master.png")}
                  resizeMode="center"
                />
              )}
              <View style={styles.detailsContainer}>
                <Text style={styles.title} numberOfLines={1}>
                  Ending {last4}
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  alignSelf: "center",
                  borderWidth: 1,
                  padding: 5,
                  borderColor: "red",
                  left: "100%",
                }}
                onPress={removeCard}
              >
                <Text style={{ color: "red" }}>remove</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Screen>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  addPaymentMethodImgStyle: {
    width: 322,
    height: 226,
    alignSelf: "center",
  },
  addPaymentMethodPlusStyle: {
    width: 14,
    height: 14,
    alignSelf: "center",
    top: 10,
    alignSelf: "center",
  },
  viewBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  viewGray: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  card: {
    backgroundColor: colors.newWhite,
    marginBottom: 20,
    overflow: "hidden",
    flexDirection: "row",
    top: 20,
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: 100,
    height: 40,
    alignSelf: "center",
  },
  title: {
    fontSize: 16,
    fontFamily: "popRegular",
  },
});

export default RemovePaymentMethodScreen;
