import React, { useState } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import * as Yup from "yup";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";
import { createStackNavigator } from "@react-navigation/stack";

import Screen from "../../components/Screen";
import colors from "../../config/colors";
import usersApi from "../../api/users";
import authApi from "../../api/auth";
import useAuth from "../../auth/useAuth";
import ActivityIndicator from "../../components/ActivityIndicator";
import useApi from "../../hooks/useApi";
import authStorage from "../../auth/storage";
import CheckEmailActivatedScreen from "./CheckEmailActivatedScreen";
import CheckPhoneNumberActivatedScreen from "./CheckPhoneNumberActivatedScreen";
import AddPhoneNumberScreen from "./AddPhoneNumberScreen";
import SuspendedUserState from "./SuspendedUserState";
import routes from "../../navigation/routes";

const validationSchema = Yup.object().shape({
  name: Yup.string().required().label("name"),
  email: Yup.string().required().email().label("email"),
  password: Yup.string().required().min(4).label("password"),
});

function RegisterScreen({ navigation }) {
  const [user, setUser] = useState();
  const Stack = createStackNavigator();
  const registerApi = useApi(usersApi.register);
  const loginApi = useApi(authApi.login);
  const auth = useAuth();
  const [error, setError] = useState();
  const [suspendedUser, setSuspendedUser] = useState(false);
  const [emailTried, setEmailTried] = useState("");

  const handleSubmit = async (userInfo) => {
    setEmailTried(userInfo.email);
    const result = await registerApi.request(userInfo);
    if (!result.ok) {
      if (result.data) {
        if (result.data.errors[0].code === "10000") {
          setSuspendedUser(true);
        }
        setError(result.data.error);
      } else {
        setError("An unexpected error occurred.");
      }
      console.log(userInfo);
      return;
    }
    const loginResult = await authApi.login(userInfo.email, userInfo.password);
    auth.logIn(loginResult.data);
    const user = await authStorage.getUser();
    setUser(user);
  };

  const close = () => {
    navigation.navigate(routes.LOGIN);
    setUser(null);
    authStorage.removeToken();
  };

  return (
    <>
      <ActivityIndicator visible={registerApi.loading || loginApi.loading} />
      {user && user.status === "EMAIL_NOT_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="CheckEmailActivated"
            component={CheckEmailActivatedScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "PHONE_NOT_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="CheckPhoneNumberActivated"
            component={CheckPhoneNumberActivatedScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "EMAIL_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="AddPhoneNumber"
            component={AddPhoneNumberScreen}
          />
        </Stack.Navigator>
      ) : (
        <>
          {suspendedUser ? (
            <SuspendedUserState emailTried={emailTried} close={close} />
          ) : (
            <Screen style={styles.container}>
              <View style={{ alignSelf: "center" }}>
                <Form
                  initialValues={{ name: "", email: "", password: "" }}
                  onSubmit={handleSubmit}
                  validationSchema={validationSchema}
                >
                  <ErrorMessage error={error} visible={error} />
                  <FormField
                    autoCorrect={false}
                    icon={require("../../assets/png/Iconly-Light-outline-Profile.png")}
                    name="name"
                    placeholder="Name"
                  />
                  <FormField
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon={require("../../assets/png/Iconly-Light-outline-Message.png")}
                    keyboardType="email-address"
                    name="email"
                    placeholder="Email"
                    textContentType="emailAddress"
                  />
                  <FormField
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon={require("../../assets/png/Iconly-Light-outline-Lock.png")}
                    name="password"
                    placeholder="Password"
                    secureTextEntry
                    textContentType="password"
                  />
                  <TouchableOpacity>
                    <Text style={styles.policy}>
                      By registering you agree to Rubie's{" "}
                      <Text
                        style={{
                          color: colors.blue,
                          textDecorationLine: "underline",
                        }}
                      >
                        terms of service{" "}
                      </Text>
                      and
                      <Text
                        style={{
                          color: colors.blue,
                          textDecorationLine: "underline",
                        }}
                      >
                        {" "}
                        privacy policy{" "}
                      </Text>
                      aggrement.
                    </Text>
                  </TouchableOpacity>
                  <View style={{ top: 50, backgroundColor: colors.blue }}>
                    <SubmitButton
                      title="Sign Up"
                      style={styles.submitBtn}
                      autoCapitalize="none"
                    />
                  </View>
                </Form>
              </View>
              <View
                style={{
                  top: 80,
                  justifyContent: "flex-end",
                  alignSelf: "center",
                  height: 100,
                }}
              >
                <Text
                  style={{
                    color: colors.darkGray,
                    fontFamily: "popRegular",
                    fontSize: 12,
                    alignSelf: "center",
                  }}
                >
                  Do you have an account?
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("Login");
                  }}
                >
                  <Text
                    style={{
                      color: colors.blue,
                      fontFamily: "popBold",
                      fontSize: 12,
                      alignSelf: "center",
                    }}
                  >
                    LOGIN
                  </Text>
                </TouchableOpacity>
              </View>
            </Screen>
          )}
        </>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingTop: 20,
    justifyContent: "center",
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
  policy: {
    color: colors.darkGray,
    justifyContent: "center",
    paddingHorizontal: 20,
    fontSize: 10,
    fontFamily: "popLight",
    alignSelf: "center",
  },
  resendEnabled: {
    alignSelf: "center",
    color: colors.blue,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  resendDisabled: {
    alignSelf: "center",
    color: colors.gray,
    fontFamily: "popRegular",
    fontSize: 19,
  },
});

export default RegisterScreen;
