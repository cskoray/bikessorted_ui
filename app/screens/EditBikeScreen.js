import React, { useEffect, useState } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import * as Yup from "yup";
import {
  Form,
  FormField,
  FormPicker as Picker,
  SubmitButton,
} from "../components/forms";

import BikeTypePickerItem from "../components/BikeTypePickerItem";
import TyreTypePickerItem from "../components/TyreTypePickerItem";
import FormImagePicker from "../components/forms/FormImagePicker";
import UploadScreen from "./UploadScreen";
import Screen from "../components/Screen";
import colors from "../config/colors";
import routes from "../navigation/routes";
import bikesApi from "../api/bikes";

const validationSchema = Yup.object().shape({
  name: Yup.string().required().min(1).label("BikeName"),
  type: Yup.object().required().label("BikeType"),
  tyreType: Yup.object().required().label("TyreType"),
  images: Yup.array().min(1, "Please select at least one image."),
});

function EditBikeScreen({ route, navigation }) {
  const [bike, setBike] = useState();
  const [progress, setProgress] = useState(0);
  const [uploadVisible, setUploadVisible] = useState(false);
  const getBikeTypesApi = useApi(bikesApi.getBikeTypes);
  const getTyreTypesApi = useApi(bikesApi.getTyreTypes);

  const handleSubmit = async (listing, { resetForm }) => {
    setProgress(0);
    setUploadVisible(true);
    const result = await bikesApi.addBike(bike, (progress) =>
      setProgress(progress)
    );
    if (result !== undefined) {
      setUploadVisible(false);
      return alert("Could not save the bike");
    }
    resetForm();
    navigation.navigate(routes.BIKES);
  };

  useEffect(() => {
    getBikeTypesApi.request();
    getTyreTypesApi.request();
    const getBike = async () => {
      const bik = await bikesApi.getBike(route.params.bikeKey);
      console.log(JSON.stringify(bik.data));
      setBike(bik.data);
    };

    getBike();
  }, []);

  return (
    <Screen style={{ backgroundColor: colors.newWhite, flex: 1 }}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          alignContent: "center",
          marginBottom: 20,
        }}
        onPress={() => {
          const params = { refer: "drawer" };
          navigation.navigate(routes.BIKES, params);
        }}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Bikes
          </Text>
        </>
      </TouchableOpacity>
      <View style={{ flex: 0.7 }}>
        {bike ? (
          <Screen style={styles.container}>
            <View style={{ flex: 1 }}>
              <UploadScreen
                onDone={() => setUploadVisible(false)}
                progress={progress}
                visible={uploadVisible}
              />
              <Form
                enableReinitialize
                initialValues={{
                  name: bike.name,
                  type: bike.bikeType,
                  tyreType: bike.tyreSize,
                  // images: bike.bikePictures,
                }}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
              >
                <FormImagePicker name="images" />
                <FormField
                  maxLength={25}
                  name="bikeName"
                  backColor={colors.newWhite}
                  noIcon="true"
                  placeholder="Bike Name"
                  width="100%"
                />
                <Picker
                  items={getBikeTypesApi.data.types}
                  name="bikeType"
                  numberOfColumns={3}
                  PickerItemComponent={BikeTypePickerItem}
                  placeholder="what kind of bike"
                  width="100%"
                />
                <Picker
                  items={getTyreTypesApi.data.types}
                  name="tyreType"
                  numberOfColumns={3}
                  PickerItemComponent={TyreTypePickerItem}
                  placeholder="tyre types"
                  width="100%"
                />
                <View style={{ top: 260, width: "100%" }}>
                  <SubmitButton
                    title="Add Bike"
                    style={{
                      backgroundColor: colors.darkGray,
                      height: 100,
                      top: 20,
                    }}
                  />
                </View>
              </Form>
            </View>
          </Screen>
        ) : null}
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    justifyContent: "center",
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  addBikeImgStyle: {
    width: 322,
    height: 226,
    alignSelf: "center",
    top: 170,
  },
  addBikePlusStyle: {
    width: 14,
    height: 14,
    alignSelf: "center",
    top: 10,
    alignSelf: "center",
  },
});

export default EditBikeScreen;
