import client from "./client";

const getRidersLocations = () => client.get("/admin/rider/locations");

const getUserStatusCounts = () => client.get("/admin/user/status");

const getRiderStats = () => client.get("/admin/rider/status");

const getShopRiderStats = () => client.get("/admin/shop/rider/status");

export const addNewRider = (name, email, phone, images, shopKey) => {
  const data = new FormData();
  data.append("name", name);
  data.append("email", email);
  data.append("phone", phone);
  data.append("shop", shopKey);

  images.forEach((image, index) =>
    data.append("images", {
      name: "image" + index,
      type: "image/jpeg",
      uri: image,
    })
  );
  return client.post("/admin/rider", data);
};

export const setNewPasswordNewRider = (password) => {
  const data = { newPassword: password };
  return client.patch("/users/new/rider/password", data);
};

export default {
  getRidersLocations,
  getUserStatusCounts,
  getRiderStats,
  addNewRider,
  setNewPasswordNewRider,
  getShopRiderStats,
};
