import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";
import NumericInput from "react-native-numeric-input";

import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import stocksApi from "../api/stocks";
import ActivityIndicator from "../components/ActivityIndicator";

function RiderOrderStocksScreen({ navigation }) {
  const [loading, setLoading] = useState(false);
  const [riderStockInfo, setRiderStockInfo] = useState();
  const [riderInventoryInfo, setRiderInventoryInfo] = useState();
  const [total, setTotal] = useState(0);
  const [orderItems, setOrderItems] = useState(new Map());

  const close = () => {
    navigation.navigate(routes.RiderStocks);
  };

  const order = async () => {
    if (total > 0) {
      setLoading(true);
      var arr = [];
      const result = Array.from(orderItems).map((item) => {
        arr.push({
          name: item[0],
          quantity: item[1].quantity,
          stockOrInventory: item[1].stockOrInventory,
        });
      });
      const res = await stocksApi.riderOrderStocks(arr);
      if (res.ok) {
        setLoading(false);
        alert("We got your order! Thanks");
        navigation.navigate(routes.RiderHome);
      }
    } else {
      alert("Please select at least an item");
    }
  };

  useEffect(() => {
    const getRidersStocksInventoryInfo = async () => {
      const result = await stocksApi.getRidersStocksInventoryInfo();
      if (result && result.data) {
        setRiderStockInfo(result.data.stockItemTypes);
        setRiderInventoryInfo(result.data.inventoryItemTypes);
        const orderMap = new Map();
        result.data.stockItemTypes.map((stock) => {
          orderMap.set(stock.type, {
            quantity: 0,
            price: stock.amount,
            stockOrInventory: "stock",
          });
        });
        result.data.inventoryItemTypes.map((inventory) => {
          orderMap.set(inventory.type, {
            quantity: 0,
            price: inventory.amount,
            stockOrInventory: "inventory",
          });
        });
        setOrderItems(orderMap);
      }
    };
    getRidersStocksInventoryInfo();
  }, []);

  return (
    <Screen style={styles.screen}>
      <ActivityIndicator visible={loading} />
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          flexDirection: "row",
          alignContent: "center",
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Order Stocks or Inventory
          </Text>
        </>
      </TouchableOpacity>

      {riderStockInfo && riderStockInfo.length > 0 && (
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "100%",
            left: 0,
          }}
          onPress={order}
        >
          <View style={styles.orderBtn}>
            <Text
              style={{
                color: colors.newWhite,
                fontFamily: "popRegular",
                fontSize: 22,
                alignSelf: "center",
              }}
            >
              Order
            </Text>
          </View>
        </TouchableOpacity>
      )}
      <View style={styles.totalTitle}>
        <Text
          style={{
            color: colors.white,
            fontFamily: "popRegular",
            fontSize: 22,
            alignSelf: "center",
          }}
        >
          Total
        </Text>
        <Text
          style={{
            color: colors.white,
            fontFamily: "popRegular",
            fontSize: 22,
            alignSelf: "center",
            marginLeft: "auto",
          }}
        >
          £ {total}
        </Text>
      </View>
      <ScrollView>
        {riderStockInfo &&
          riderStockInfo.map((stock) => (
            <View style={styles.userStats}>
              <View style={styles.stats}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  {stock.name}
                </Text>

                <View style={{ marginLeft: "auto", alignSelf: "center" }}>
                  <NumericInput
                    onChange={(value) => {
                      if (value >= 0) {
                        orderItems.set(stock.type, {
                          quantity: value,
                          price: stock.amount,
                          stockOrInventory: "stock",
                        });
                        setOrderItems(orderItems);
                        var total = 0;
                        orderItems.forEach(function (value, key) {
                          total += value.quantity * value.price;
                          setTotal(total);
                        });
                      }
                    }}
                    onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                    totalWidth={135}
                    totalHeight={40}
                    iconSize={20}
                    valueType="real"
                    rounded
                    textColor={colors.darkGray}
                    iconStyle={{ color: "white" }}
                    rightButtonBackgroundColor={colors.black}
                    leftButtonBackgroundColor={colors.black}
                    minValue={0}
                    maxValue={10}
                  />
                </View>
              </View>
              <Text
                style={{
                  fontFamily: "popSemiBold",
                  fontSize: 18,
                }}
              >
                £
                {orderItems.has(stock.type) && orderItems.get(stock.type).price}{" "}
                each
              </Text>
            </View>
          ))}
        <View style={{ borderTopWidth: 10, top: 5 }}>
          {riderInventoryInfo &&
            riderInventoryInfo.map((inventory) => (
              <View style={styles.userStats}>
                <View style={styles.stats}>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "center",
                    }}
                  >
                    {inventory.name}
                  </Text>
                  <View style={{ marginLeft: "auto", alignSelf: "center" }}>
                    <NumericInput
                      onChange={(value) => {
                        if (value >= 0) {
                          orderItems.set(inventory.type, {
                            quantity: value,
                            price: inventory.amount,
                            stockOrInventory: "inventory",
                          });
                          setOrderItems(orderItems);
                          var total = 0;
                          orderItems.forEach(function (value, key) {
                            total += value.quantity * value.price;
                            setTotal(total);
                          });
                        }
                      }}
                      onLimitReached={(isMax, msg) => console.log(isMax, msg)}
                      totalWidth={135}
                      totalHeight={40}
                      iconSize={20}
                      valueType="real"
                      rounded
                      textColor={colors.darkGray}
                      iconStyle={{ color: "white" }}
                      rightButtonBackgroundColor={colors.black}
                      leftButtonBackgroundColor={colors.black}
                      minValue={0}
                      maxValue={10}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                  }}
                >
                  £{inventory.amount}
                </Text>
              </View>
            ))}
        </View>
      </ScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  done: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 60,
    padding: 20,
    alignSelf: "center",
    width: "90%",
  },
  title: {
    top: 20,
    flexDirection: "row",
    padding: 15,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 10,
    shadowOffset: {
      height: 5,
    },
    shadowOpacity: 0.8,
  },
  orderBtn: {
    flexDirection: "row",
    padding: 15,
    backgroundColor: colors.black,
    width: "40%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 10,
    shadowOffset: {
      height: 5,
    },
    shadowOpacity: 0.8,
    borderRadius: 10,
    justifyContent: "center",
  },
  totalTitle: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.blue,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    zIndex: 9999,
  },
  stats: {
    flexDirection: "row",
  },
  userStats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
});

export default RiderOrderStocksScreen;
