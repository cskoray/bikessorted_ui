import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import AdminHomeScreen from "../screens/AdminHomeScreen";
import AdminRidersScreen from "../screens/AdminRidersScreen";
import AdminUsersScreen from "../screens/AdminUsersScreen";
import AdminBookingsScreen from "../screens/AdminBookingsScreen";
import AdminPayoutsScreen from "../screens/AdminPayoutsScreen";
import AdminNewRiderScreen from "../screens/AdminNewRiderScreen";
import AdminNewRiderNewPasswordScreen from "../screens/AdminNewRiderNewPasswordScreen";
import AdminNewShopScreen from "../screens/AdminNewShopScreen";

const Stack = createStackNavigator();

const AdminHomeNavigator = () => (
  <Stack.Navigator
    screenOptions={{ headerShown: false, gestureEnabled: false }}
  >
    <Stack.Screen name="AdminHomeScreen" component={AdminHomeScreen} />
    <Stack.Screen name="AdminNewRiderScreen" component={AdminNewRiderScreen} />
    <Stack.Screen name="AdminPayoutsScreen" component={AdminPayoutsScreen} />
    <Stack.Screen name="AdminBookingsScreen" component={AdminBookingsScreen} />
    <Stack.Screen name="AdminUsersScreen" component={AdminUsersScreen} />
    <Stack.Screen name="AdminRidersScreen" component={AdminRidersScreen} />
    <Stack.Screen
      name="AdminNewRiderNewPasswordScreen"
      component={AdminNewRiderNewPasswordScreen}
    />
    <Stack.Screen name="AdminNewShopScreen" component={AdminNewShopScreen} />
  </Stack.Navigator>
);

export default AdminHomeNavigator;
