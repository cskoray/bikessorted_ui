import React, { useState } from "react";
import { BlurView } from "expo-blur";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Modal,
  ImageBackground,
} from "react-native";
import { add, format, subtract } from "mathjs";

import Screen from "../../components/Screen";
import colors from "../../config/colors";

export default function ServicePopup(props) {
  const {
    cancelServiceSelected,
    serviceSelected,
    serviceRefs,
    ifServiceSelected,
    setServiceToOrder,
    data,
    serviceToOrder,
    servicesSelected,
  } = props;

  const [total, setTotal] = useState(0);

  return (
    <>
      {Platform.OS == "ios" ? (
        <BlurView intensity={100} style={StyleSheet.absoluteFill} tint="dark">
          <>
            <Screen style={{ justifyContent: "flex-start" }}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  width: "100%",
                  left: 0,
                  backgroundColor: colors.darkGray,
                  flexDirection: "row",
                  alignContent: "center",
                }}
                onPress={cancelServiceSelected}
              >
                <>
                  <Image
                    source={require("../../assets/png/ArrowCloseCircle.png")}
                    style={styles.ImageIconStyle}
                  />
                  <Text
                    style={{
                      color: colors.newWhite,
                      fontFamily: "popRegular",
                      fontSize: 22,
                      alignSelf: "center",
                    }}
                  >
                    Services
                  </Text>
                </>
              </TouchableOpacity>
              <ScrollView style={{ flex: 1, width: "100%", height: "100%" }}>
                <TouchableOpacity
                  style={{
                    alignItems: "center",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    justifyContent: "flex-start",
                    flex: 1,
                  }}
                  delayPressIn={150}
                >
                  {data &&
                    data.map((service, index) => (
                      <View
                        key={service.serviceKey}
                        style={styles.serviceDet}
                        ref={(r) => (serviceRefs.current[index] = r)}
                      >
                        <TouchableOpacity
                          onPress={() => serviceSelected(service)}
                        >
                          <View>
                            <Image
                              source={{ uri: service.picture }}
                              style={styles.serviceImageIconStyle}
                            />
                          </View>
                          <View
                            style={{
                              flexDirection: "row",
                            }}
                          >
                            <Text
                              style={{
                                left: 10,
                                fontSize: 16,
                                fontFamily: "popSemiBold",
                              }}
                            >
                              {service.name}
                            </Text>
                            <Text
                              style={{
                                right: 5,
                                fontSize: 16,
                                fontFamily: "popSemiBold",
                                marginLeft: "auto",
                              }}
                            >
                              £{service.price}
                            </Text>
                          </View>
                          <Text
                            style={{
                              left: 10,
                              fontSize: 13,
                              fontFamily: "popRegular",
                            }}
                          >
                            {service.description}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => {
                            if (!ifServiceSelected(service.serviceKey)) {
                              setServiceToOrder((prevItems) => [
                                ...prevItems,
                                service,
                              ]);
                              setTotal(
                                format(add(total, service.price), {
                                  precision: 6,
                                })
                              );
                            } else {
                              let c = serviceToOrder.filter(
                                (s) => s.serviceKey !== service.serviceKey
                              );
                              setServiceToOrder(c);
                              setTotal(
                                format(subtract(total, service.price), {
                                  precision: 6,
                                })
                              );
                            }
                          }}
                          delayPressIn={150}
                        >
                          <View
                            style={
                              ifServiceSelected(service.serviceKey)
                                ? styles.serviceAddBlue
                                : styles.serviceAdd
                            }
                          >
                            <Image
                              source={
                                !ifServiceSelected(service.serviceKey)
                                  ? require("../../assets/png/plus.png")
                                  : require("../../assets/png/checkmark.png")
                              }
                              style={styles.addIconStyle}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    ))}
                </TouchableOpacity>
              </ScrollView>
              <TouchableOpacity onPress={servicesSelected}>
                <View
                  style={
                    serviceToOrder.length === 0
                      ? styles.viewGray
                      : styles.viewBlue
                  }
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      color: colors.white,
                      fontFamily: "popBold",
                      fontSize: 20,
                      left: 20,
                    }}
                  >
                    total: £ {total}
                  </Text>
                  <Text
                    style={{
                      alignSelf: "center",
                      color: colors.white,
                      fontFamily: "popBold",
                      fontSize: 20,
                      marginLeft: "auto",
                    }}
                  >
                    next
                  </Text>
                  <Image
                    source={require("../../assets/png/Iconly-Light-outline-Arrow-Right-2.png")}
                    style={{
                      width: 20,
                      height: 20,
                      alignSelf: "center",
                    }}
                  />
                </View>
              </TouchableOpacity>
            </Screen>
          </>
        </BlurView>
      ) : (
        <Modal>
          <ImageBackground
            source={require("../../assets/png/backgroundBlur.jpg")}
            style={{
              flex: 1,
              resizeMode: "cover",
              justifyContent: "center",
              zIndex: 999999,
            }}
            blurRadius={20}
          >
            <TouchableOpacity
              style={{
                padding: 0,
                margin: 0,
                width: "100%",
                left: 0,
                backgroundColor: colors.darkGray,
                flexDirection: "row",
                alignContent: "center",
              }}
              onPress={cancelServiceSelected}
            >
              <>
                <Image
                  source={require("../../assets/png/ArrowCloseCircle.png")}
                  style={styles.ImageIconStyle}
                />
                <Text
                  style={{
                    color: colors.newWhite,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  Services
                </Text>
              </>
            </TouchableOpacity>
            <ScrollView>
              <View
                style={{
                  alignItems: "center",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "flex-start",
                }}
              >
                {data &&
                  data.map((service, index) => (
                    <View
                      key={service.serviceKey}
                      style={styles.serviceDet}
                      ref={(r) => (serviceRefs.current[index] = r)}
                    >
                      <TouchableOpacity
                        onPress={() => serviceSelected(service)}
                      >
                        <View>
                          <Image
                            source={{ uri: service.picture }}
                            style={styles.serviceImageIconStyle}
                          />
                        </View>
                        <View
                          style={{
                            flexDirection: "row",
                          }}
                        >
                          <Text
                            style={{
                              left: 10,
                              fontSize: 16,
                              fontFamily: "popSemiBold",
                            }}
                          >
                            {service.name}
                          </Text>
                          <Text
                            style={{
                              right: 5,
                              fontSize: 16,
                              fontFamily: "popSemiBold",
                              marginLeft: "auto",
                            }}
                          >
                            £{service.price}
                          </Text>
                        </View>
                        <Text
                          style={{
                            left: 10,
                            fontSize: 13,
                            fontFamily: "popRegular",
                          }}
                        >
                          {service.description}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          if (!ifServiceSelected(service.serviceKey)) {
                            setServiceToOrder((prevItems) => [
                              ...prevItems,
                              service,
                            ]);
                            setTotal(
                              format(add(total, service.price), {
                                precision: 6,
                              })
                            );
                          } else {
                            let c = serviceToOrder.filter(
                              (s) => s.serviceKey !== service.serviceKey
                            );
                            setServiceToOrder(c);
                            setTotal(
                              format(subtract(total, service.price), {
                                precision: 6,
                              })
                            );
                          }
                        }}
                        delayPressIn={150}
                      >
                        <View
                          style={
                            ifServiceSelected(service.serviceKey)
                              ? styles.serviceAddBlue
                              : styles.serviceAdd
                          }
                        >
                          <Image
                            source={
                              !ifServiceSelected(service.serviceKey)
                                ? require("../../assets/png/plus.png")
                                : require("../../assets/png/checkmark.png")
                            }
                            style={styles.addIconStyle}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))}
              </View>
            </ScrollView>
            <TouchableOpacity onPress={servicesSelected}>
              <View
                style={
                  serviceToOrder.length === 0
                    ? styles.viewGray
                    : styles.viewBlue
                }
              >
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: "popBold",
                    fontSize: 20,
                    left: 20,
                    alignSelf: "center",
                    width: "80%",
                  }}
                >
                  total: £ {total}
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    color: colors.white,
                    fontFamily: "popBold",
                    fontSize: 20,
                    marginLeft: "auto",
                  }}
                >
                  next
                </Text>
                <Image
                  source={require("../../assets/png/Iconly-Light-outline-Arrow-Right-2.png")}
                  style={{
                    width: 20,
                    height: 20,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                />
              </View>
            </TouchableOpacity>
          </ImageBackground>
        </Modal>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  serviceDet: {
    marginTop: 20,
    margin: 10,
    left: 20,
    borderRadius: 10,
    backgroundColor: "white",
    width: "40%",
    height: 210,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    alignSelf: "flex-start",
    elevation: 15,
  },
  serviceImageIconStyle: {
    margin: 10,
    height: 94,
    borderRadius: 10,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  serviceAdd: {
    width: 70,
    height: 50,
    borderRadius: 5,
    backgroundColor: colors.darkGray,
    alignSelf: "flex-start",
    left: "60%",
    borderBottomLeftRadius: 10,
  },
  serviceAddBlue: {
    width: 70,
    height: 50,
    borderRadius: 5,
    backgroundColor: colors.blue,
    alignSelf: "flex-start",
    left: "60%",
    borderBottomLeftRadius: 10,
  },
  addIconStyle: {
    width: 50,
    height: 50,
    alignSelf: "center",
    borderBottomLeftRadius: 10,
  },
  grayNext: { backgroundColor: colors.darkGray },
  grayBlue: { backgroundColor: colors.blue },
  viewBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
    flexDirection: "row",
  },
  viewGray: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 76,
    margin: 0,
    flexDirection: "row",
  },
});
