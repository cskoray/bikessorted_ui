import client from "./client";

export const updateRiderState = (state) => {
  const params = { riderUpTimeState: state };
  return client.patch("/users/up-time-update", params);
};

export default {
  updateRiderState,
};
