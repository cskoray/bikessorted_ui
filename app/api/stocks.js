import client from "./client";

const getRidersCurrentStocks = () => client.get("/users/stock/current");

const getRidersStocksInventoryBookingHistory = () =>
  client.get("/users/stock/history");

const getRidersStocksInventoryInfo = () => client.get("/users/stock/info");

export const riderOrderStocks = (stocks) => {
  const data = { stocks: stocks };
  return client.post("/users/rider/order", data);
};

export default {
  getRidersStocksInventoryBookingHistory,
  getRidersStocksInventoryInfo,
  riderOrderStocks,
  getRidersCurrentStocks,
};
