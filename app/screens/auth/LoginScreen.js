import React, { useState, useEffect } from "react";
import { StyleSheet, TouchableOpacity, Text, View, Image } from "react-native";
import * as Yup from "yup";
import { createStackNavigator } from "@react-navigation/stack";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";

import Screen from "../../components/Screen";
import authApi from "../../api/auth";
import useAuth from "../../auth/useAuth";
import colors from "../../config/colors";
import authStorage from "../../auth/storage";
import CheckEmailActivatedScreen from "./CheckEmailActivatedScreen";
import CheckPhoneNumberActivatedScreen from "./CheckPhoneNumberActivatedScreen";
import AddPhoneNumberScreen from "./AddPhoneNumberScreen";
import AdminNewRiderNewPasswordScreen from "../AdminNewRiderNewPasswordScreen";
import NewRiderAddStripeCardScreen from "../NewRiderAddStripeCardScreen";

const validationSchema = Yup.object().shape({
  email: Yup.string().required().email().label("email"),
  password: Yup.string().required().min(4).label("password"),
});

function LoginScreen({ navigation }) {
  const [user, setUser] = useState();
  const Stack = createStackNavigator();
  const auth = useAuth();
  const [loginFailed, setLoginFailed] = useState(false);

  useEffect(() => {
    const userState = async () => {
      const user = await authStorage.getUser();
      if (user) {
        setUser(user);
      }
    };
    userState();
  }, [navigation]);

  const handleSubmit = async ({ email, password }) => {
    const result = await authApi.login(email, password);
    if (result && !result.data) return setLoginFailed(true);
    auth.logIn(result.data);
    const user = await authStorage.getUser();
    if (user) {
      setUser(user);
    }
  };

  return (
    <>
      {user && user.status === "EMAIL_NOT_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="CheckEmailActivated"
            component={CheckEmailActivatedScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "PHONE_NOT_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="CheckPhoneNumberActivated"
            component={CheckPhoneNumberActivatedScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "EMAIL_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="AddPhoneNumber"
            component={AddPhoneNumberScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "NEW_RIDER" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="AdminNewRiderNewPasswordScreen"
            component={AdminNewRiderNewPasswordScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "CARD_RIDER" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="NewRiderAddStripeCardScreen"
            component={NewRiderAddStripeCardScreen}
          />
        </Stack.Navigator>
      ) : (
        <Screen style={styles.container}>
          <KeyboardAwareScrollView>
            <View>
              <View style={{ alignSelf: "center", width: 330, height: 280 }}>
                <Image
                  style={styles.mainImage}
                  source={require("../../assets/png/LondonCityDark.png")}
                />
              </View>
              <View style={styles.form}>
                <Form
                  initialValues={{ email: "", password: "" }}
                  onSubmit={handleSubmit}
                  validationSchema={validationSchema}
                >
                  <ErrorMessage
                    error="Invalid email and/or password."
                    visible={loginFailed}
                  />
                  <FormField
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon={require("../../assets/png/Iconly-Light-outline-Profile.png")}
                    keyboardType="email-address"
                    name="email"
                    placeholder="email"
                    textContentType="emailAddress"
                  />
                  <FormField
                    autoCapitalize="none"
                    autoCorrect={false}
                    icon={require("../../assets/png/Iconly-Light-outline-Lock.png")}
                    name="password"
                    placeholder="password"
                    secureTextEntry
                    textContentType="password"
                  />
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate("GetEmailForgotPassword")
                    }
                  >
                    <Text style={styles.forgot}>Forgot Password</Text>
                  </TouchableOpacity>
                  <View style={{ top: 20, backgroundColor: colors.blue }}>
                    <SubmitButton
                      title="Login"
                      style={styles.submitBtn}
                      autoCapitalize="none"
                    />
                  </View>
                </Form>
              </View>
              <View
                style={{
                  justifyContent: "flex-end",
                  alignSelf: "center",
                  height: 80,
                }}
              >
                <Text
                  style={{
                    color: colors.darkGray,
                    fontFamily: "popRegular",
                    fontSize: 12,
                    alignSelf: "center",
                  }}
                >
                  Don't have an account yet?
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("Register");
                  }}
                >
                  <Text
                    style={{
                      color: colors.blue,
                      fontFamily: "popBold",
                      fontSize: 12,
                      alignSelf: "center",
                    }}
                  >
                    SIGN UP
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </Screen>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: colors.white,
  },
  mainImage: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  forgot: {
    color: colors.blue,
    alignSelf: "flex-end",
    right: 10,
    top: 5,
  },
  form: {
    paddingTop: 10,
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
});

export default LoginScreen;
