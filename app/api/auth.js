import client from "./client";

const login = (email, password) =>
  client.post("/users/login", { email, password });

const verifyEmail = (code) => client.post("/users/email", { code: code });

const addPhoneNumber = (phone) => client.patch("/users/phone-number", phone);

const verifyPhoneNumber = (code) => client.post("/users/phone", { code: code });

const verifyPasswordCode = (email, code) =>
  client.post("/users/password", { email: email, code: code });

const generatePasswordCode = (email) =>
  client.post("/users/password/code", { email: email });

const updatePasswordCode = (email, code, password) =>
  client.post("/users/password/update", {
    email: email,
    code: code,
    password: password,
  });

const resend = (type) => client.patch("/users/resend", { type: type });

const resendForgotPassword = (email) =>
  client.patch("/users/resend/forgot-password", { email: email });

const suspendUser = (type) => client.patch("/users/suspend", { type: type });

const verifyUserStatus = (email) => client.get(`/users/verify/${email}/status`);

export default {
  addPhoneNumber,
  login,
  verifyEmail,
  verifyPhoneNumber,
  verifyPasswordCode,
  generatePasswordCode,
  updatePasswordCode,
  resend,
  resendForgotPassword,
  suspendUser,
  verifyUserStatus,
};
