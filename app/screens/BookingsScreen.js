import React, { useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import { round, format } from "mathjs";
import { ScrollView } from "react-native-gesture-handler";

import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import useApi from "../hooks/useApi";
import bookingApi from "../api/booking";
import ActivityIndicator from "../components/ActivityIndicator";

function BookingsScreen({ navigation }) {
  const bookings = useApi(bookingApi.getBookings);

  useEffect(() => {
    bookings.request();
  }, []);

  const detailActive = (booking) => {
    const bookingKey = booking.bookingKey;
    const totalAmount = booking.totalAmount;
    const latitude = booking.latitude;
    const longitude = booking.longitude;
    const type = booking.type;
    const bikeType = booking.bikeType;
    const tyreSize = booking.tyreSize;
    const name = booking.riderName;
    const riderPic = booking.riderPic;
    const createdDate = booking.createdDate;
    let dueDate = booking.dueDateFormatted;
    if (type === "INSTANT") {
      dueDate = booking.dueDateFormatted.slice(-5);
    }
    const services = booking.services;
    const riderRate = booking.riderRate;
    const params = {
      bookingKey,
      totalAmount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      riderPic,
      riderRate,
      createdDate,
      dueDate,
      services,
    };
    if (type === "INSTANT") {
      navigation.navigate(routes.ActiveBookingDetail, params);
    } else if (type === "LATER") {
      navigation.navigate(routes.ActiveLaterBookingDetail, params);
    }
  };

  const close = () => {
    navigation.navigate(routes.HOME);
  };

  return (
    <Screen style={styles.screen}>
      <ActivityIndicator visible={bookings.loading} />
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          alignContent: "center",
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Bookings
          </Text>
        </>
      </TouchableOpacity>
      {bookings.data &&
        (bookings.data.active &&
        bookings.data.active.length === 0 &&
        bookings.data.old &&
        bookings.data.old.length === 0 ? (
          <View style={{ top: 50 }}>
            <Text
              style={{
                alignSelf: "center",
                fontFamily: "popRegular",
                fontSize: 19,
              }}
            >
              No service bookings yet
            </Text>
          </View>
        ) : (
          <ScrollView
            style={{
              backgroundColor: colors.white,
              height: "90%",
            }}
          >
            {bookings.data.active && bookings.data.active.length > 0 && (
              <Text
                style={{ left: 15, fontFamily: "popSemiBold", fontSize: 16 }}
              >
                Active bookings
              </Text>
            )}
            <View style={{ marginBottom: 50 }}>
              {bookings.data.active &&
                bookings.data.active.map((booking) => (
                  <TouchableOpacity
                    onPress={() => detailActive(booking)}
                    key={booking.bookingKey}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        borderBottomWidth: 2,
                        borderColor: colors.light,
                      }}
                    >
                      <Image
                        source={{ uri: booking.pic }}
                        style={styles.bikeImg}
                      />
                      <View
                        style={{
                          alignSelf: "center",
                          width: "50%",
                        }}
                      >
                        <Text>{booking.dueDateFormatted}</Text>
                        {booking.services.map((service) => (
                          <Text
                            style={{ fontSize: 14, fontFamily: "popSemiBold" }}
                          >
                            {service.name}{" "}
                          </Text>
                        ))}
                      </View>
                      <Text
                        style={{
                          alignSelf: "center",
                          fontSize: 14,
                          fontFamily: "popSemiBold",
                        }}
                      >
                        £
                        {round(
                          format(booking.totalAmount, { precision: 6 }),
                          2
                        )}
                      </Text>
                      <Image
                        source={require("../assets/png/Iconly-Light-outline-Arrow-RightCircle.png")}
                        style={{
                          resizeMode: "contain",
                          width: 23,
                          height: 26,
                          alignSelf: "center",
                          left: "40%",
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                ))}
              <View style={{ top: 30 }}>
                {bookings.data.old && bookings.data.old.length > 0 && (
                  <Text
                    style={{
                      left: 15,
                      fontFamily: "popSemiBold",
                      fontSize: 16,
                    }}
                  >
                    Past 5 bookings
                  </Text>
                )}
              </View>
              <View style={{ top: 30 }}>
                {bookings.data.old &&
                  bookings.data.old.map((booking) => (
                    <View
                      style={{
                        flexDirection: "row",
                        borderBottomWidth: 2,
                        borderColor: colors.light,
                      }}
                      key={booking.bookingKey}
                    >
                      <Image
                        source={{ uri: booking.pic }}
                        style={styles.bikeImg}
                      />
                      <View
                        style={{
                          alignSelf: "center",
                          width: "50%",
                        }}
                      >
                        <Text>{booking.dueDateFormatted}</Text>
                        {booking.services.map((service) => (
                          <Text
                            style={{ fontSize: 14, fontFamily: "popSemiBold" }}
                            key={service.name}
                          >
                            {service.name}{" "}
                          </Text>
                        ))}
                      </View>
                      <Text
                        style={{
                          alignSelf: "center",
                          fontSize: 14,
                          fontFamily: "popSemiBold",
                        }}
                      >
                        £
                        {round(
                          format(booking.totalAmount, { precision: 6 }),
                          2
                        )}
                      </Text>
                    </View>
                  ))}
              </View>
            </View>
          </ScrollView>
        ))}
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  bikeImg: {
    margin: 15,
    width: 47,
    height: 40,
    top: 0,
    borderRadius: 10,
    resizeMode: "contain",
  },
});

export default BookingsScreen;
