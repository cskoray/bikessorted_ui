import { useEffect } from "react";
import upTimeApi from "../api/upTime";
import { AppState, Platform } from "react-native";

export default riderUpTime = () => {
  useEffect(() => {
    const stt = AppState.addEventListener("change", handleAppStateChange);
    return () => {
      if (stt) {
        stt.remove(); //AppState.removeNotificationSubscription("change", handleAppStateChange);
      }
    };
  }, []);

  const handleAppStateChange = (nextAppState) => {
    const hours = new Date().getHours();
    if (hours >= 8 && hours <= 20) {
      upTimeApi.updateRiderState(nextAppState);
    }
  };
};
