import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ShopAdminHomeScreen from "../screens/ShopAdminHomeScreen";
import ShopAdminRidersScreen from "../screens/ShopAdminRidersScreen";
import ShopAdminBookingsScreen from "../screens/ShopAdminBookingsScreen";
import ShopAdminPayoutsScreen from "../screens/ShopAdminPayoutsScreen";
import ShopAdminUsersScreen from "../screens/ShopAdminUsersScreen";

const Stack = createStackNavigator();

const ShopAdminHomeNavigator = () => (
  <Stack.Navigator
    screenOptions={{ headerShown: false, gestureEnabled: false }}
  >
    <Stack.Screen name="ShopAdminHomeScreen" component={ShopAdminHomeScreen} />
    <Stack.Screen
      name="ShopAdminPayoutsScreen"
      component={ShopAdminPayoutsScreen}
    />
    <Stack.Screen
      name="ShopAdminBookingsScreen"
      component={ShopAdminBookingsScreen}
    />
    <Stack.Screen
      name="ShopAdminRidersScreen"
      component={ShopAdminRidersScreen}
    />
    <Stack.Screen
      name="ShopAdminUsersScreen"
      component={ShopAdminUsersScreen}
    />
  </Stack.Navigator>
);

export default ShopAdminHomeNavigator;
