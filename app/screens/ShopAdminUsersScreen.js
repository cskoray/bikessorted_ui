import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";
import { Rating } from "react-native-ratings";

import routes from "../navigation/routes";
import colors from "../config/colors";
import Screen from "../components/Screen";
import ridersApi from "../api/riders";

function ShopAdminUsersScreen({ navigation }) {
  const [userStatusCounts, setUserStatusCounts] = useState();
  const [riderStats, setRiderStats] = useState();

  const close = () => {
    navigation.navigate(routes.ShopAdminHome);
  };

  useEffect(() => {
    const getRiderStats = async () => {
      const result = await ridersApi.getShopRiderStats();
      if (result && result.data) {
        setRiderStats(result.data);
      }
    };
    getRiderStats();
  }, []);

  return (
    <Screen style={styles.screen}>
      <ScrollView>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "100%",
            left: 0,
            flexDirection: "row",
            justifyContent: "flex-start",
            height: 60,
          }}
          onPress={close}
        >
          <>
            <Image
              source={require("../assets/png/ArrowCloseCircle.png")}
              style={styles.ImageIconStyle}
            />
          </>
        </TouchableOpacity>
        <View>
          {userStatusCounts &&
            userStatusCounts.counts &&
            userStatusCounts.counts.length > 0 && (
              <View style={styles.title}>
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  Customer counts
                </Text>
              </View>
            )}
          {userStatusCounts &&
            userStatusCounts.counts &&
            userStatusCounts.counts.map((count) => (
              <View style={styles.userStats} key={count.status}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  {count.status}
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  {count.count}
                </Text>
              </View>
            ))}
          {riderStats &&
            riderStats.riderStats &&
            riderStats.riderStats.length > 0 && (
              <View style={styles.title}>
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  Rider Stats
                </Text>
              </View>
            )}
          {riderStats &&
            riderStats.riderStats &&
            riderStats.riderStats.map((rider) => (
              <View style={styles.riderStats}>
                <View style={styles.stats} key={rider.email}>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "center",
                    }}
                  >
                    {rider.name}
                  </Text>
                  <Rating
                    type="custom"
                    readonly
                    startingValue={rider.rating}
                    ratingCount={5}
                    imageSize={60}
                    showRating={false}
                    style={{
                      alignSelf: "center",
                      marginLeft: "auto",
                    }}
                    tintColor={colors.newWhite}
                    imageSize={19}
                    ratingBackgroundColor="#c8c7c8"
                  />
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "center",
                      marginLeft: "auto",
                    }}
                  >
                    Total rides: {rider.finishedBookings}
                  </Text>
                </View>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "flex-start",
                  }}
                >
                  £{rider.total}
                </Text>
              </View>
            ))}
        </View>
      </ScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
  stats: {
    flexDirection: "row",
  },
  riderStats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
  userStats: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
  title: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
  },
});

export default ShopAdminUsersScreen;
