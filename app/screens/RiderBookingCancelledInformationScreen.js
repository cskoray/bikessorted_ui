import React from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import MapView from "react-native-maps";
import { Avatar } from "react-native-paper";
import Constants from "expo-constants";
import { round } from "mathjs";

import colors from "../config/colors";
import routes from "../navigation/routes";

function RiderBookingCancelledInformationScreen({ route, navigation }) {
  const {
    bookingKey,
    amount,
    latitude,
    longitude,
    type,
    bikeType,
    tyreSize,
    name,
    phone,
    postcode,
    createdDate,
    dueDate,
    pic,
    bookingType,
    services,
  } = route.params;

  const close = () => {
    navigation.navigate(routes.RiderHome);
  };

  return (
    <>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          latitudeDelta: 0.00222,
          longitudeDelta: 0.00421,
        }}
        mapType="mutedStandard"
        showsMyLocationButton
        zoomControlEnabled
        zoomEnabled
        provider={"google"}
        paddingAdjustmentBehavior="automatic"
      >
        <MapView.Marker
          draggable
          coordinate={{
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
          }}
        >
          <Image
            source={require("../assets/png/RubMap.png")}
            style={{
              height: 80,
            }}
            resizeMode="contain"
          />
        </MapView.Marker>
      </MapView>
      <TouchableOpacity
        style={{
          top: Constants.statusBarHeight,
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.white,
          flexDirection: "row",
          justifyContent: "flex-start",
          marginBottom: 20,
          height: 60,
          position: "absolute",
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Cancelled booking
          </Text>
        </>
      </TouchableOpacity>
      <View
        style={{
          position: "absolute",
          backgroundColor: colors.darkGray,
          borderRadius: 15,
          top: "52%",
          width: "98%",
          height: "57%",
          alignSelf: "center",
          padding: 10,
        }}
      >
        <View style={{ flexDirection: "row", padding: 5 }}>
          <Avatar.Image
            source={{
              uri: pic,
            }}
            size={35}
          />
          <Text
            style={{
              fontSize: 20,
              fontFamily: "popSemiBold",
              color: colors.white,
              left: 15,
              alignSelf: "center",
            }}
          >
            {name}/{postcode} - {phone}
          </Text>
        </View>
        <View style={{ top: 5 }}>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Earning</Text>
            <Text style={styles.txt}>{round(amount)}</Text>
            <Text style={styles.txt2}>on this ride</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Due to</Text>
            <Text style={styles.txt}>{dueDate}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Bicycle type</Text>
            <Text style={styles.txt}>{bikeType}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Tyre size</Text>
            <Text style={styles.txt}>{tyreSize} inch</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              top: 10,
              backgroundColor: colors.white,
              padding: 2,
              width: "100%",
            }}
          >
            <TouchableOpacity style={styles.btnAccept} delayPressIn={150}>
              <Text style={styles.btntxt}>CANCELLED</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  txt: {
    fontFamily: "popSemiBold",
    fontSize: 18,
    left: 10,
    padding: 5,
    color: colors.white,
  },
  txt2: {
    fontFamily: "popRegular",
    fontSize: 16,
    left: 10,
    padding: 5,
    color: colors.white,
    alignSelf: "center",
  },
  btnAccept: {
    flex: 1,
    backgroundColor: colors.blue,
    padding: 10,
    justifyContent: "center",
    height: 70,
  },
  btntxt: {
    color: colors.white,
    alignSelf: "center",
    fontFamily: "popBold",
    fontSize: 18,
  },
  ImageIconStyle: {
    margin: 10,
    width: 40,
    height: 40,
    top: 0,
    zIndex: 1,
  },
});

export default RiderBookingCancelledInformationScreen;
