import React from "react";
import { Image, View, StyleSheet, TouchableOpacity } from "react-native";

import Text from "./Text";

import electric from "../assets/png/electric.png";
import kids from "../assets/png/kids.png";
import mountain from "../assets/png/mountain.png";
import road from "../assets/png/road.png";
import tricycle from "../assets/png/Tricycle.png";

function Imag(props) {
  const item = props.item;
  switch (item.label) {
    case "ELECTRIC":
      return (
        <Image source={electric} style={styles.img} resizeMode="contain" />
      );
    case "KIDS":
      return <Image source={kids} style={styles.img} resizeMode="contain" />;
    case "MOUNTAIN":
      return (
        <Image source={mountain} style={styles.img} resizeMode="contain" />
      );
    case "ROAD":
      return <Image source={road} style={styles.img} resizeMode="contain" />;
    case "TRICYCLE":
      return (
        <Image source={tricycle} style={styles.img} resizeMode="contain" />
      );
  }
}
function BikeTypePickerItem({ item, onPress }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        {<Imag item={item} />}
        <Text style={styles.label}>{item.label}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: 15,
    alignItems: "center",
    width: "33%",
  },
  label: {
    marginTop: 5,
    textAlign: "center",
    fontSize: 14,
    fontFamily: "popLight",
  },
  img: { width: 80, height: 80 },
});

export default BikeTypePickerItem;
