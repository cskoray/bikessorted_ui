import React, { useState } from "react";
import {
  Button,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
} from "react-native";
import * as Yup from "yup";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import { MaterialIcons } from "@expo/vector-icons";

import Screen from "../components/Screen";
import colors from "../config/colors";
import routes from "../navigation/routes";
import shopApi from "../api/shop";
import FormImagePicker from "../components/forms/FormImagePicker";
import UploadScreen from "./UploadScreen";

const validationSchema = Yup.object().shape({
  shopName: Yup.string().required().label("Shop name"),
  ownerName: Yup.string().required().label("Owner name"),
  ownerEmail: Yup.string().required().label("Owner email"),
  ownerPhone: Yup.string()
    .required()
    .label("phone number")
    .min(11, "phone number starts with 0 and 11 characters")
    .max(11, "phone number starts with 0 and 11 characters"),
  images: Yup.array().min(1, "Please select at least one image."),
  postcode: Yup.string().required().label("postcode"),
});

const days = [
  {
    name: "Off days of shop",
    id: 0,
    children: [
      {
        id: "MONDAY",
        name: "Monday",
      },
      {
        id: "TUESDAY",
        name: "Tuesday",
      },
      {
        id: "WEDNESDAY",
        name: "Wednesday",
      },
      {
        id: "THURSDAY",
        name: "Thursday",
      },
      {
        id: "FRIDAY",
        name: "Friday",
      },
      {
        id: "SATURDAY",
        name: "Saturday",
      },
      {
        id: "SUNDAY",
        name: "Sunday",
      },
    ],
  },
];

function AdminNewShopScreen({ navigation }) {
  const [error, setError] = useState();
  const [progress, setProgress] = useState(0);
  const [uploadVisible, setUploadVisible] = useState(false);
  const [isDatePickerVisibleStart, setDatePickerVisibilityStart] =
    useState(false);
  const [isDatePickerVisibleFinish, setDatePickerVisibilityFinish] =
    useState(false);
  const [startTime, setStartTime] = useState("");
  const [finishTime, setFinishTime] = useState("");
  const [selectedDays, setSelectedDays] = useState([]);

  const onSelectedItemsChange = (selectedDays) => {
    setSelectedDays(selectedDays);
  };
  const confirmOffDays = () => {
    console.log(selectedDays);
  };

  const showDatePickerStart = () => {
    setDatePickerVisibilityStart(true);
  };

  const hideDatePickerStart = () => {
    setDatePickerVisibilityStart(false);
  };

  const handleConfirmStart = (date) => {
    setStartTime(date.getHours() + ":" + date.getMinutes());
    hideDatePickerStart();
  };

  const showDatePickerFinish = () => {
    setDatePickerVisibilityFinish(true);
  };

  const hideDatePickerFinish = () => {
    setDatePickerVisibilityFinish(false);
  };

  const handleConfirmFinish = (date) => {
    setFinishTime(date.getHours() + ":" + date.getMinutes());
    hideDatePickerFinish();
  };

  const handleSubmit = async (shopInfo, { resetForm }) => {
    setProgress(0);
    setUploadVisible(true);
    const result = await shopApi.addNewShop(
      shopInfo,
      startTime,
      finishTime,
      selectedDays,
      (progress) => setProgress(progress)
    );
    if (result && result.ok) {
      resetForm();
      return setUploadVisible(false);
    }
    navigation.navigate(routes.AdminHome);
  };

  const close = () => {
    navigation.navigate(routes.AdminHome);
  };

  return (
    <Screen style={styles.container}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          flexDirection: "row",
          justifyContent: "flex-start",
          height: 60,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Add New Shop
          </Text>
        </>
      </TouchableOpacity>
      <UploadScreen
        onDone={() => setUploadVisible(false)}
        progress={progress}
        visible={uploadVisible}
      />
      <KeyboardAwareScrollView>
        <Form
          enableReinitialize
          initialValues={{
            shopName: "",
            ownerName: "",
            ownerEmail: "",
            phone: "",
            images: [],
            postcode: "",
          }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <ErrorMessage error={error} visible={error} />
          <FormImagePicker name="images" />
          <FormField
            autoCorrect={false}
            icon={require("../assets/png/Iconly-Light-outline-Profile.png")}
            name="shopName"
            placeholder="Shop name"
          />
          <FormField
            autoCorrect={false}
            icon={require("../assets/png/Iconly-Light-outline-Profile.png")}
            name="ownerName"
            placeholder="Owner name"
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon={require("../assets/png/Iconly-Light-outline-Message.png")}
            keyboardType="email-address"
            name="ownerEmail"
            placeholder="Owner email"
            textContentType="emailAddress"
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon={require("../assets/png/Iconly-Light-outline-Call.png")}
            keyboardType="phone-pad"
            name="ownerPhone"
            placeholder="i.e 07123456789"
            textContentType="telephoneNumber"
          />
          <FormField
            autoCorrect={false}
            icon={require("../assets/png/Iconly-Light-outline-Profile.png")}
            name="postcode"
            placeholder="Postcode"
          />
          <View>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "popSemiBold",
                alignSelf: "center",
              }}
            >
              Working hours
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Button title="Start time" onPress={showDatePickerStart} />
              <Text
                style={{
                  fontSize: 18,
                  fontFamily: "popSemiBold",
                  alignSelf: "center",
                }}
              >
                {startTime}
              </Text>
            </View>
            <DateTimePickerModal
              isVisible={isDatePickerVisibleStart}
              mode="time"
              locale="en_GB"
              onConfirm={handleConfirmStart}
              onCancel={hideDatePickerStart}
            />
            <View style={{ flexDirection: "row" }}>
              <Button title="Finish time" onPress={showDatePickerFinish} />
              <Text
                style={{
                  fontSize: 18,
                  fontFamily: "popSemiBold",
                  alignSelf: "center",
                }}
              >
                {finishTime}
              </Text>
            </View>
            <DateTimePickerModal
              isVisible={isDatePickerVisibleFinish}
              mode="time"
              locale="en_GB"
              onConfirm={handleConfirmFinish}
              onCancel={hideDatePickerFinish}
            />
          </View>
          <View>
            <SectionedMultiSelect
              items={days}
              IconRenderer={MaterialIcons}
              uniqueKey="id"
              subKey="children"
              selectText="Pick off days of the shop"
              showDropDowns={true}
              readOnlyHeadings={true}
              onSelectedItemsChange={(day) => onSelectedItemsChange(day)}
              selectedItems={selectedDays}
              onConfirm={confirmOffDays}
            />
          </View>
          <View style={{ top: 5, backgroundColor: colors.blue }}>
            <SubmitButton
              title="Add Shop"
              style={styles.submitBtn}
              autoCapitalize="none"
            />
          </View>
        </Form>
      </KeyboardAwareScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
  policy: {
    color: colors.darkGray,
    justifyContent: "center",
    paddingHorizontal: 20,
    fontSize: 10,
    fontFamily: "popLight",
    alignSelf: "center",
  },
  resendEnabled: {
    alignSelf: "center",
    color: colors.blue,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  resendDisabled: {
    alignSelf: "center",
    color: colors.gray,
    fontFamily: "popRegular",
    fontSize: 19,
  },
});

export default AdminNewShopScreen;
