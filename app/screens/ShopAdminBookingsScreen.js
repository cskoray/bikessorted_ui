import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";
import routes from "../navigation/routes";

import colors from "../config/colors";
import Screen from "../components/Screen";
import bookingsApi from "../api/booking";

function ShopAdminBookingsScreen({ navigation }) {
  const [bookingStats, setBookingStats] = useState();

  const close = () => {
    navigation.navigate(routes.AdminHome);
  };

  useEffect(() => {
    const getBookingStats = async () => {
      const result = await bookingsApi.getBookingStats();
      if (result && result.data) {
        setBookingStats(result.data);
      }
    };
    getBookingStats();
  }, []);

  return (
    <Screen style={styles.screen}>
      <ScrollView>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "100%",
            left: 0,
            flexDirection: "row",
            justifyContent: "flex-start",
            height: 60,
          }}
          onPress={close}
        >
          <>
            <Image
              source={require("../assets/png/ArrowCloseCircle.png")}
              style={styles.ImageIconStyle}
            />
            <View></View>
          </>
        </TouchableOpacity>
        <View>
          <View style={styles.title}>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popRegular",
                fontSize: 22,
                alignSelf: "center",
              }}
            >
              Booking Stats
            </Text>
            {bookingStats &&
              bookingStats.finishedBookings &&
              bookingStats.finishedBookings.length > 0 && (
                <Text
                  style={{
                    color: colors.white,
                    fontFamily: "popBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  Today's total: £{bookingStats.todaysTotal}
                </Text>
              )}
          </View>
          {bookingStats &&
            bookingStats.finishedBookings &&
            bookingStats.finishedBookings.map((booking) => (
              <View style={styles.bookingStats} key={booking.riderEmail}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "flex-start",
                    color: colors.primary,
                  }}
                >
                  Rubie's earning £{booking.rubieEarning}
                </Text>
                <View style={styles.stats}>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "center",
                    }}
                  >
                    {booking.riderName}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      marginLeft: "auto",
                    }}
                  >
                    {booking.postCode}
                  </Text>
                </View>
                <View style={styles.stats}>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "flex-start",
                    }}
                  >
                    {booking.riderEmail}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      marginLeft: "auto",
                    }}
                  >
                    {booking.type}
                  </Text>
                </View>
                <View style={styles.stats}>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "flex-start",
                    }}
                  >
                    Booking cost £{booking.amount}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      marginLeft: "auto",
                    }}
                  >
                    tip £{booking.tip}
                  </Text>
                </View>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                  }}
                >
                  Rider's earning £{booking.riderEarning}
                </Text>
                <View style={{ top: 10 }}>
                  <Text
                    style={{
                      fontFamily: "popRegular",
                      fontSize: 22,
                      alignSelf: "flex-start",
                    }}
                  >
                    Finished at {booking.finishedDate}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "popRegular",
                      fontSize: 22,
                      alignSelf: "flex-start",
                    }}
                  >
                    Accepted at {booking.acceptedDate}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "popRegular",
                      fontSize: 22,
                      alignSelf: "flex-start",
                    }}
                  >
                    Created at {booking.createdDate}
                  </Text>
                </View>
              </View>
            ))}
        </View>
      </ScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
  title: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
  },
  stats: {
    flexDirection: "row",
  },
  bookingStats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
});

export default ShopAdminBookingsScreen;
