import React, { useEffect, useState } from "react";
import { BlurView } from "expo-blur";
import Moment from "moment";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Modal,
  Platform,
} from "react-native";
import Screen from "../../components/Screen";
import colors from "../../config/colors";
import Button from "../../components/Button";

export default function EmergencyHelpPopup(props) {
  const { cancelEmergencySelected, orderEmergencyNow, arrival, data } = props;

  const [price, setPrice] = useState();

  useEffect(() => {
    if (data && data[0]) {
      setPrice(data[0].price);
    }
  }, [data]);

  return (
    <>
      {Platform.OS == "ios" ? (
        <BlurView intensity={100} style={StyleSheet.absoluteFill} tint="dark">
          <>
            <Screen style={{ justifyContent: "flex-start" }}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  width: "100%",
                  left: 0,
                  backgroundColor: colors.darkGray,
                  flexDirection: "row",
                  alignContent: "center",
                }}
                onPress={cancelEmergencySelected}
              >
                <Image
                  source={require("../../assets/png/ArrowCloseCircle.png")}
                  style={styles.ImageIconStyle}
                />
                <Text
                  style={{
                    color: colors.newWhite,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  Puncture service
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                delayPressIn={150}
                onPress={cancelEmergencySelected}
                style={{ alignItems: "center", flex: 1 }}
              >
                <Text
                  style={{
                    color: colors.newWhite,
                    fontSize: 16,
                    fontFamily: "popRegular",
                    paddingTop: 90,
                  }}
                >
                  Reaching you max 30 minutes
                </Text>
                <Text
                  style={{
                    color: colors.newWhite,
                    fontSize: 16,
                    fontFamily: "popRegular",
                  }}
                >
                  wherever you are!..
                </Text>
                <TouchableOpacity
                  style={styles.emergencyDet}
                  onPress={null}
                  delayPressIn={150}
                >
                  <View>
                    <Image
                      source={require("../../assets/png/EmergencyKitTyre.png")}
                      style={styles.emergencyImageIconStyle}
                    />
                    <View
                      style={{ padding: 10, paddingLeft: 15, paddingTop: 25 }}
                    >
                      <Text
                        style={{
                          fontFamily: "popRegular",
                          fontSize: 12,
                        }}
                      >
                        meet you by
                      </Text>
                      <View style={{ flexDirection: "row" }}>
                        <Image
                          source={require("../../assets/png/Time.png")}
                          style={styles.TimeIconStyle}
                        />
                        <Text
                          style={{
                            fontFamily: "popRegular",
                            fontSize: 20,
                            paddingLeft: 10,
                            alignSelf: "flex-end",
                          }}
                        >
                          {Moment(arrival).format("HH:mm")}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View
                    style={{
                      justifyContent: "flex-start",
                      padding: 20,
                      paddingLeft: 10,
                    }}
                  >
                    <Text style={styles.emergencyDescTitle}>Flat tyre?</Text>
                    <Text style={styles.emergencyDesc}>
                      No worries! All we need is
                    </Text>
                    <Text style={styles.emergencyDesc}>your tyre size. </Text>
                    <Text
                      style={{
                        alignSelf: "flex-end",
                        top: "20%",
                        fontFamily: "popRegular",
                        fontSize: 20,
                      }}
                    >
                      £ {price}
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Screen>
            <View style={styles.tyreSelectBtnBlue}>
              <Button
                style={styles.orderNowBlue}
                title="Book"
                onPress={orderEmergencyNow}
              />
            </View>
          </>
        </BlurView>
      ) : (
        <Modal>
          <ImageBackground
            source={require("../../assets/png/backgroundBlur.jpg")}
            style={{
              flex: 1,
              resizeMode: "cover",
              justifyContent: "center",
              zIndex: 999999,
            }}
            blurRadius={20}
          >
            <>
              <Screen style={{ justifyContent: "flex-start" }}>
                <TouchableOpacity
                  style={{
                    padding: 0,
                    margin: 0,
                    width: "100%",
                    left: 0,
                    backgroundColor: colors.darkGray,
                    flexDirection: "row",
                    alignContent: "center",
                  }}
                  onPress={cancelEmergencySelected}
                >
                  <Image
                    source={require("../../assets/png/ArrowCloseCircle.png")}
                    style={styles.ImageIconStyle}
                  />
                  <Text
                    style={{
                      color: colors.newWhite,
                      fontFamily: "popRegular",
                      fontSize: 22,
                      alignSelf: "center",
                    }}
                  >
                    Puncture service
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ alignItems: "center", flex: 1 }}
                  delayPressIn={150}
                  onPress={cancelEmergencySelected}
                >
                  <Text
                    style={{
                      color: colors.darkGray,
                      fontSize: 16,
                      fontFamily: "popRegular",
                      paddingTop: 90,
                    }}
                  >
                    Reaching you max 30 minutes
                  </Text>
                  <Text
                    style={{
                      color: colors.darkGray,
                      fontSize: 16,
                      fontFamily: "popRegular",
                    }}
                  >
                    wherever you are!..
                  </Text>
                  <TouchableOpacity
                    style={styles.emergencyDet}
                    onPress={null}
                    delayPressIn={150}
                  >
                    <View>
                      <Image
                        source={require("../../assets/png/EmergencyKitTyre.png")}
                        style={styles.emergencyImageIconStyle}
                      />
                      <View
                        style={{ padding: 10, paddingLeft: 15, paddingTop: 25 }}
                      >
                        <Text
                          style={{
                            fontFamily: "popRegular",
                            fontSize: 12,
                          }}
                        >
                          meet you by
                        </Text>
                        <View style={{ flexDirection: "row" }}>
                          <Image
                            source={require("../../assets/png/Time.png")}
                            style={styles.TimeIconStyle}
                          />
                          <Text
                            style={{
                              fontFamily: "popRegular",
                              fontSize: 20,
                              paddingLeft: 10,
                              alignSelf: "flex-end",
                            }}
                          >
                            {Moment(arrival).format("HH:mm")}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <View
                      style={{
                        justifyContent: "flex-start",
                        padding: 20,
                        paddingLeft: 10,
                      }}
                    >
                      <Text style={styles.emergencyDescTitle}>Flat tyre?</Text>
                      <Text style={styles.emergencyDesc}>
                        No worries! All we need is
                      </Text>
                      <Text style={styles.emergencyDesc}>your tyre size. </Text>
                      <Text
                        style={{
                          alignSelf: "flex-end",
                          top: "20%",
                          fontFamily: "popRegular",
                          fontSize: 20,
                        }}
                      >
                        £ {price}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </TouchableOpacity>
              </Screen>
              <View style={styles.tyreSelectBtnBlue}>
                <Button
                  style={styles.orderNowBlue}
                  title="Next"
                  onPress={orderEmergencyNow}
                />
              </View>
            </>
          </ImageBackground>
        </Modal>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  emergencyDet: {
    marginTop: 70,
    borderRadius: 10,
    backgroundColor: "white",
    width: "90%",
    height: 180,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    flexDirection: "row",
  },
  add: {
    width: 50,
    height: 50,
    backgroundColor: colors.darkGray,
    alignSelf: "flex-start",
    left: 290,
    borderBottomLeftRadius: 10,
    zIndex: 9999,
  },
  addBlue: {
    width: 50,
    height: 50,
    backgroundColor: colors.blue,
    alignSelf: "flex-start",
    left: 290,
    borderBottomLeftRadius: 10,
    zIndex: 9999,
  },
  addIconStyle: {
    width: 50,
    height: 50,
    alignSelf: "center",
    borderBottomLeftRadius: 10,
  },
  emergencyImageIconStyle: {
    top: 20,
    left: 10,
    width: 130,
    height: 84,
    borderRadius: 10,
  },
  TimeIconStyle: { marginTop: 10, width: 24, height: 24 },
  emergencyDescTitle: {
    fontFamily: "popBold",
    fontSize: 20,
    paddingBottom: 10,
    left: 10,
  },
  emergencyDesc: {
    fontFamily: "popRegular",
    fontSize: 12,
    paddingBottom: 10,
    left: 10,
  },
  orderNow: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    margin: 0,
  },
  orderNowBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    margin: 0,
    fontFamily: "popBold",
    fontSize: 24,
  },
  tyreSelectBtn: {
    alignContent: "flex-end",
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 76,
    margin: 0,
  },
  tyreSelectBtnBlue: {
    alignContent: "flex-end",
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
  },
});
