import React from "react";
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
} from "react-native";

import Text from "./Text";
import colors from "../config/colors";

function TimeCard({ booked, label, selectd, onPress }) {
  return (
    <TouchableWithoutFeedback disabled={booked} onPress={onPress}>
      <View style={label === selectd ? styles.cardSelected : styles.card}>
        {booked ? (
          <View style={styles.timeViewInactive}>
            <Text style={styles.inactive} numberOfLines={1}>
              {label}
            </Text>
          </View>
        ) : label === selectd ? (
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <View style={styles.timeViewSelected}>
              <Text style={styles.titleSelected} numberOfLines={1}>
                {label}
              </Text>
            </View>
            <Image
              source={require("../assets/png/Icon-ionic-ios-checkmark.png")}
              style={styles.ImageIconStyle}
              resizeMode="contain"
            />
          </View>
        ) : (
          <View style={styles.timeView}>
            <Text style={styles.title} numberOfLines={1}>
              {label}
            </Text>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: colors.newWhite,
    overflow: "hidden",
    marginHorizontal: 15,
    marginVertical: 5,
    padding: 15,
  },
  cardSelected: {
    borderRadius: 15,
    backgroundColor: colors.blue,
    overflow: "hidden",
    marginHorizontal: 15,
    marginVertical: 5,
    padding: 15,
  },
  title: {
    padding: 10,
    alignSelf: "flex-start",
    color: colors.white,
  },
  inactive: {
    backgroundColor: colors.newWhite,
    padding: 10,
    alignSelf: "flex-start",
    color: colors.white,
  },
  timeView: {
    borderRadius: 35,
    backgroundColor: colors.blue,
    alignSelf: "flex-start",
  },
  timeViewInactive: {
    borderRadius: 35,
    backgroundColor: colors.gray,
    alignSelf: "flex-start",
  },
  titleSelected: {
    padding: 10,
    alignSelf: "flex-start",
    color: colors.blue,
  },
  timeViewSelected: {
    borderRadius: 35,
    backgroundColor: colors.white,
    alignSelf: "flex-start",
  },
  ImageIconStyle: {
    width: 24,
    height: 24,
    top: 0,
    alignSelf: "center",
    left: 180,
  },
});

export default TimeCard;
