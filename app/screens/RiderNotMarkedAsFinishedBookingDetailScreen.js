import React, { useEffect, useState } from "react";
import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  Modal,
  Button,
  Linking,
  ScrollView,
} from "react-native";
import MapView from "react-native-maps";
import { Avatar } from "react-native-paper";
import Constants from "expo-constants";
import { round } from "mathjs";

import RiderBookingDetailInfoIndicator from "../components/RiderBookingDetailInfoIndicator";
import colors from "../config/colors";
import bookingApi from "../api/booking";
import routes from "../navigation/routes";
import useLocation from "../hooks/useLocation";
import useApi from "../hooks/useApi";
import servicesApi from "../api/services";

function RiderNotMarkedAsFinishedBookingDetailScreen({ route, navigation }) {
  const [actionTaken, setActionTaken] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedPic, setSelectedPic] = useState();
  const [bikePicModalVisible, setBikePicModalVisible] = useState(false);
  const [notSureModalVisible, setNotSureModalVisible] = useState(false);
  const [reported, setReported] = useState(false);
  const [latestLocation, setLatestLocation] = useState();
  const location = useLocation();
  const getServicesApi = useApi(servicesApi.getServices);
  const [serviceToOrder, setServiceToOrder] = useState([]);
  const [orderDisabled, setOrderDisabled] = useState(false);
  const [approved, setApproved] = useState(false);
  const [total, setTotal] = useState(0);

  const {
    bookingKey,
    amount,
    latitude,
    longitude,
    type,
    name,
    phone,
    pic,
    bikeType,
    tyreSize,
    dueDate,
    services,
    postcode,
    option,
    bikePics,
  } = route.params;

  useEffect(() => {
    setLatestLocation(location);
    const isReported = async () => {
      const result = await bookingApi.isReported(bookingKey);
      if (result && result.data) {
        setReported(result.data.status);
      }
    };
    ifApproved();
    isReported();
  }, [location]);

  const ifApproved = async () => {
    const result = await bookingApi.getNotSureOrderedApproved(bookingKey);
    if (result.ok) {
      setApproved(false);
    } else {
      if (result.data.errors[0].code === "10034") {
        setApproved(true);
      }
    }
  };

  const order = async () => {
    if (serviceToOrder.length > 0) {
      setOrderDisabled(true);
      const result = await bookingApi.notSureServiceRiderOrder(
        bookingKey,
        serviceToOrder
      );
      setNotSureModalVisible(false);
      if (result.ok) {
        alert(`Thanks! Please wait ${name} to approve, before fixing`);
      } else {
        if (result.data.errors[0].code === "10031") {
          alert("You already ordered! We are on it");
        } else {
          alert("Please try in a few mins");
        }
      }
    }
  };

  const checkIfAlreadyOrderedOrCanOrder = async () => {
    const result = await bookingApi.getNotSureOrderedAlready(bookingKey);
    if (result && !result.data) {
      getServicesApi.request();
      setNotSureModalVisible(true);
      setOrderDisabled(false);
    } else {
      alert(`You already ordered! Please wait ${name} to approve`);
    }
  };

  const openServicesModal = async () => {
    const dateObj = new Date();
    const mins = dateObj.getMinutes();
    const hour = dateObj.getHours();
    const month = dateObj.getMonth() + 1;
    const day = String(dateObj.getDate()).padStart(2, "0");
    const year = dateObj.getFullYear();
    const today = day + "-" + month + "-" + year;
    if (today === dueDate.substring(6)) {
      if (
        hour >= parseInt(dueDate.substring(0, 2)) &&
        mins >= parseInt(dueDate.substring(3, 5))
      ) {
        ifApproved();
        checkIfAlreadyOrderedOrCanOrder();
      } else if (hour == parseInt(dueDate.substring(0, 2))) {
        if (mins >= parseInt(dueDate.substring(3, 5))) {
          ifApproved();
          checkIfAlreadyOrderedOrCanOrder();
        } else {
          alert(`Please wait for ${dueDate} to order`);
        }
      } else {
        alert(`Please wait for ${dueDate} to order`);
      }
    } else {
      alert(`Please wait for ${dueDate} to order`);
    }
  };

  const ifServiceSelected = (serviceKey) => {
    if (serviceToOrder.length === 0) return false;
    return serviceToOrder.find((service) => service === serviceKey);
  };

  const addService = (service) => {
    if (!ifServiceSelected(service.serviceKey)) {
      setServiceToOrder((prevItems) => [...prevItems, service.serviceKey]);
      setTotal(total + service.price);
    } else {
      let c = serviceToOrder.filter((s) => s !== service.serviceKey);
      setServiceToOrder(c);
      setTotal(total - service.price);
    }
  };

  const cancelServices = () => {
    setNotSureModalVisible(false);
  };

  const reportIt = () => {
    setModalVisible(true);
  };

  const finish = async () => {
    setActionTaken(true);
    const result = await bookingApi.finishBooking(bookingKey);
    if (result.status === 400) {
      alert("you can not finish before " + dueDate);
      setActionTaken(false);
    } else {
      navigation.navigate(routes.RiderHome);
    }
  };

  const close = () => {
    navigation.navigate(routes.RiderHome);
  };

  const cancel = () => {
    setModalVisible(false);
  };

  const riderBikeBroke = async () => {
    Alert.alert(
      "Cancel Booking",
      "Are you sure want to report?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.riderBikeBroke(bookingKey);
            alert("Thanks! We have received the report");
            setModalVisible(false);
            setActionTaken(true);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const requestNewRider = async () => {
    Alert.alert(
      "Reassign Booking",
      "Are you sure you can't make it?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.requestNewRider(bookingKey);
            alert("Thanks! We received your request");
            setModalVisible(false);
            setActionTaken(true);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const danger = async () => {
    Alert.alert(
      "Cancel Booking",
      "Are you sure want to report?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.danger(bookingKey);
            alert("Thanks! We have received the report");
            setModalVisible(false);
            setActionTaken(true);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const userBikeProblem = async () => {
    Alert.alert(
      "Cancel Booking",
      "Are you sure want to report?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.userBikeProblem(bookingKey);
            alert("Thanks! We have received the report");
            setModalVisible(false);
            setActionTaken(true);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const cancelZoom = (uri) => {
    setBikePicModalVisible(false);
  };

  const zoom = (uri) => {
    setSelectedPic(uri);
    setBikePicModalVisible(true);
  };

  const redirect = () => {
    Linking.openURL(
      Platform.OS === "ios"
        ? `googleMaps://app?saddr=${latestLocation.latitude}+${latestLocation.longitude}&daddr=${latitude}+${longitude}`
        : `google.navigation:q=${latitude}+${longitude}`
    );
  };

  return (
    <>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          latitudeDelta: 0.00222,
          longitudeDelta: 0.00421,
        }}
        mapType="mutedStandard"
        showsMyLocationButton
        zoomControlEnabled
        zoomEnabled
        provider={"google"}
        paddingAdjustmentBehavior="automatic"
      >
        <MapView.Marker
          draggable
          coordinate={{
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
          }}
        >
          <Image
            source={require("../assets/png/RubMap.png")}
            style={{
              height: 80,
            }}
            resizeMode="contain"
          />
        </MapView.Marker>
      </MapView>
      <TouchableOpacity
        style={{
          top: Constants.statusBarHeight,
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.white,
          flexDirection: "row",
          justifyContent: "flex-start",
          // marginBottom: 10,
          height: 50,
          position: "absolute",
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            fixing this bicycle
          </Text>
        </>
      </TouchableOpacity>
      {option && (
        <View
          style={{
            position: "absolute",
            top: "45%",
            width: "40%",
            height: "11%",
            backgroundColor: approved ? colors.blue : colors.darkGray,
            padding: 2,
            borderRadius: 15,
            left: "1%",
          }}
        >
          <TouchableOpacity
            style={approved ? styles.btnServicesBlue : styles.btnServices}
            onPress={openServicesModal}
            disabled={approved}
          >
            {approved ? (
              <>
                <RiderBookingDetailInfoIndicator visible={true} />
                <Text style={styles.btnServicesApprovedtxt}>Ready To Fix</Text>
              </>
            ) : (
              <Text style={styles.btnServicestxt}>Order Services</Text>
            )}
          </TouchableOpacity>
        </View>
      )}
      <View
        style={{
          position: "absolute",
          top: "60%",
          width: "40%",
          height: "11%",
          backgroundColor: colors.blue,
          padding: 2,
          borderRadius: 15,
          right: "1%",
          alignSelf: "flex-end",
        }}
      >
        <TouchableOpacity onPress={redirect}>
          <Text
            style={{
              color: colors.white,
              alignSelf: "center",
              top: 5,
              fontFamily: "popRegular",
            }}
          >
            switch to
          </Text>
          <Text
            style={{
              color: colors.white,
              alignSelf: "center",
              fontFamily: "popSemiBold",
              fontSize: 16,
            }}
          >
            Google Maps
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          position: "absolute",
          backgroundColor: colors.darkGray,
          borderRadius: 15,
          top: "66%",
          width: "98%",
          height: "52%",
          alignSelf: "center",
          padding: 10,
        }}
      >
        <View style={{ flexDirection: "row", padding: 5 }}>
          <Avatar.Image
            source={{
              uri: pic,
            }}
            size={35}
          />
          <Text
            style={{
              fontSize: 20,
              fontFamily: "popSemiBold",
              color: colors.white,
              left: 15,
              alignSelf: "center",
            }}
          >
            {name}/{postcode} - {phone}
          </Text>
        </View>
        <Modal animationType="fade" visible={bikePicModalVisible}>
          <TouchableOpacity style={{ top: 25 }} onPress={cancelZoom}>
            <Text
              style={{
                color: colors.blue,
                alignSelf: "center",
                fontFamily: "popRegular",
                fontSize: 20,
              }}
            >
              close
            </Text>
          </TouchableOpacity>
          <View
            style={{
              height: "92%",
              marginTop: "auto",
              backgroundColor: colors.white,
            }}
          >
            <Image
              source={{ uri: selectedPic }}
              style={{ width: "100%", height: "100%", borderRadius: 5 }}
            />
          </View>
        </Modal>
        <View style={{ top: 5 }}>
          <View
            style={{
              position: "absolute",
              alignSelf: "flex-end",
              top: -10,
              zIndex: 9999,
            }}
          >
            <ScrollView horizontal>
              {bikePics.map((uri) => (
                <TouchableOpacity
                  style={{
                    flexDirection: "row",
                  }}
                  onPress={() => zoom(uri)}
                  key={uri}
                >
                  <Image
                    source={{ uri: uri }}
                    style={{ width: 100, height: 100, borderRadius: 5 }}
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Earning</Text>
            <Text style={styles.txt}>£{round(amount)}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Due</Text>
            <Text style={styles.txt}>{dueDate}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt}>
              {bikeType} bicycle / {tyreSize} inch
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            {services.map((service) => (
              <Text key={service} style={styles.txt}>
                {service}{" "}
              </Text>
            ))}
          </View>
          {!actionTaken && (
            <View
              style={{
                flexDirection: "row",
                top: 20,
                padding: 2,
                width: "100%",
              }}
            >
              <TouchableOpacity style={styles.btnAccept} onPress={finish}>
                <Text style={styles.btntxt}>finish</Text>
              </TouchableOpacity>
              {!reported && (
                <TouchableOpacity style={styles.btnReject} onPress={reportIt}>
                  <Text style={styles.btntxt}>report problem</Text>
                </TouchableOpacity>
              )}
            </View>
          )}
        </View>
      </View>
      <Modal animationType="slide" visible={modalVisible} transparent={true}>
        <View
          style={{
            height: "55%",
            marginTop: "auto",
            backgroundColor: colors.test,
            padding: 10,
            marginHorizontal: "1%",
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          }}
        >
          <Button style={styles.cancel} title="close" onPress={cancel} />

          {!reported && (
            <View style={{ top: 25 }}>
              <TouchableOpacity
                onPress={riderBikeBroke}
                style={{
                  flexDirection: "row",
                  padding: 10,
                  margin: 5,
                  backgroundColor: colors.white,
                  borderRadius: 5,
                }}
              >
                <Image
                  source={require("../assets/png/Iconly-Light-outline-CloseSquare.png")}
                  style={{ width: 30, height: 30 }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    fontFamily: "popRegular",
                    fontSize: 16,
                    marginLeft: "auto",
                    width: "80%",
                  }}
                >
                  My bike is broken
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  padding: 10,
                  backgroundColor: colors.white,
                  margin: 5,
                  borderRadius: 5,
                }}
                onPress={danger}
              >
                <Image
                  source={require("../assets/png/Iconly-Light-outline-Danger.png")}
                  style={{ width: 30, height: 30 }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    top: 5,
                    fontFamily: "popRegular",
                    fontSize: 16,
                    marginLeft: "auto",
                    width: "80%",
                  }}
                >
                  I'm in dangerous situation
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  padding: 10,
                  flexDirection: "row",
                  backgroundColor: colors.white,
                  margin: 5,
                  borderRadius: 5,
                }}
                onPress={userBikeProblem}
              >
                <Image
                  source={require("../assets/png/Iconly-Light-outline-InfoCircle.png")}
                  style={{ width: 30, height: 30 }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    top: 5,
                    fontFamily: "popRegular",
                    fontSize: 16,
                    marginLeft: "auto",
                    width: "80%",
                  }}
                >
                  Bicycle has some other problem
                </Text>
              </TouchableOpacity>
            </View>
          )}
          {!reported && type === "LATER" && (
            <View style={{ top: "20%" }}>
              <TouchableOpacity
                onPress={() => requestNewRider()}
                style={{
                  backgroundColor: colors.blue,
                  padding: 5,
                  borderRadius: 10,
                }}
              >
                <View
                  style={{ flexDirection: "row", justifyContent: "flex-start" }}
                >
                  <Image
                    source={require("../assets/png/Iconly-Light-outline-Notification.png")}
                    style={{ width: 40, height: 40 }}
                    resizeMode="contain"
                  />
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 14,
                      alignSelf: "center",
                      color: colors.white,
                      marginLeft: "auto",
                      width: "85%",
                    }}
                  >
                    Find new technician for this booking
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
      <Modal
        animationType="slide"
        visible={notSureModalVisible}
        transparent={true}
      >
        <View
          style={{
            height: "100%",
            marginTop: "auto",
            backgroundColor: colors.white,
          }}
        >
          <ScrollView style={{ top: 20 }}>
            <Button title="close" onPress={cancelServices} />
            <View style={styles.title}>
              <Text
                style={{
                  color: colors.white,
                  fontFamily: "popSemiBold",
                  fontSize: 22,
                  alignSelf: "center",
                }}
              >
                Total
              </Text>
              <Text
                style={{
                  color: colors.white,
                  fontFamily: "popSemiBold",
                  fontSize: 22,
                  alignSelf: "center",
                  marginLeft: "auto",
                }}
              >
                £{total}
              </Text>
            </View>
            <View style={{ top: 30 }}>
              {getServicesApi.data &&
                getServicesApi.data.map(
                  (service, index) =>
                    service.name !== "Not Sure" && (
                      <TouchableOpacity onPress={() => addService(service)}>
                        <View
                          style={{
                            alignSelf: "center",
                            backgroundColor: colors.newWhite,
                            padding: 10,
                            marginTop: 20,
                            elevation: 15,
                            shadowRadius: 5,
                            shadowOffset: {
                              width: 5,
                              height: 10,
                            },
                            shadowOpacity: 0.2,
                            width: "95%",
                            borderRadius: 10,
                            flexDirection: "row",
                          }}
                        >
                          <Text
                            style={{
                              fontFamily: "popSemiBold",
                              fontSize: 18,
                            }}
                          >
                            Add {service.name} Service
                          </Text>
                          <Text
                            style={{
                              fontFamily: "popSemiBold",
                              fontSize: 18,
                              marginLeft: "auto",
                            }}
                          >
                            £{service.price}
                          </Text>
                          {ifServiceSelected(service.serviceKey) ? (
                            <Image
                              source={require("../assets/png/checked.png")}
                              style={{
                                width: 30,
                                height: 30,
                                marginLeft: "auto",
                              }}
                              resizeMode="contain"
                            />
                          ) : (
                            <Image
                              source={require("../assets/png/unchecked.png")}
                              style={{
                                width: 30,
                                height: 30,
                                marginLeft: "auto",
                              }}
                              resizeMode="contain"
                            />
                          )}
                        </View>
                      </TouchableOpacity>
                    )
                )}
            </View>
            <TouchableOpacity
              style={
                serviceToOrder.length > 0 && !orderDisabled
                  ? styles.orderBlue
                  : styles.order
              }
              onPress={order}
              delayPressIn={serviceToOrder.length > 0 ? 0 : 150}
              disabled={orderDisabled}
            >
              <Text
                style={{
                  color:
                    serviceToOrder.length > 0 && !orderDisabled
                      ? colors.newWhite
                      : colors.gray,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                }}
              >
                Order services for {name}
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  txt: {
    fontFamily: "popSemiBold",
    fontSize: 18,
    left: 10,
    padding: 5,
    color: colors.white,
  },
  txt2: {
    fontFamily: "popRegular",
    fontSize: 16,
    left: 10,
    padding: 5,
    color: colors.white,
    alignSelf: "center",
  },
  order: {
    padding: 20,
    margin: 20,
    width: "90%",
    backgroundColor: colors.light,
    justifyContent: "center",
    borderRadius: 10,
    top: 50,
  },
  orderBlue: {
    padding: 20,
    margin: 20,
    width: "90%",
    backgroundColor: colors.blue,
    justifyContent: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
    },
    shadowOpacity: 0.8,
    borderRadius: 10,
    top: 50,
  },
  btnServices: {
    flex: 0.5,
    backgroundColor: colors.white,
    padding: 10,
    justifyContent: "center",
    height: 50,
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
  },
  btnServicesBlue: {
    flex: 0.5,
    backgroundColor: colors.blue,
    padding: 10,
    justifyContent: "center",
    height: 50,
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
  },
  btnAccept: {
    flex: 0.5,
    backgroundColor: colors.blue,
    padding: 10,
    justifyContent: "center",
    height: 50,
    borderRadius: 10,
  },
  btnReject: {
    flex: 0.5,
    backgroundColor: colors.black,
    padding: 10,
    justifyContent: "center",
    height: 50,
    borderRadius: 10,
  },
  btntxt: {
    color: colors.white,
    alignSelf: "center",
    fontFamily: "popBold",
    fontSize: 18,
  },
  btnServicestxt: {
    color: colors.black,
    alignSelf: "center",
    fontFamily: "popBold",
    fontSize: 18,
  },
  btnServicesApprovedtxt: {
    color: colors.white,
    alignSelf: "center",
    fontFamily: "popBold",
    fontSize: 18,
  },
  ImageIconStyle: {
    margin: 5,
    width: 40,
    height: 40,
    top: 0,
    zIndex: 1,
  },
  cancel: {
    backgroundColor: "black",
    borderRadius: 0,
    height: 60,
    padding: 20,
    alignSelf: "center",
    top: 100,
  },
  title: {
    top: 10,
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
  },
});

export default RiderNotMarkedAsFinishedBookingDetailScreen;
