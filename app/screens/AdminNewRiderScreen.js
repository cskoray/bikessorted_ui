import React, { useEffect, useState, useRef } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import * as Yup from "yup";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import { Picker } from "@react-native-picker/picker";
import FormImagePicker from "../components/forms/FormImagePicker";
import UploadScreen from "./UploadScreen";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ActivityIndicator from "../components/ActivityIndicator";

import Screen from "../components/Screen";
import colors from "../config/colors";
import routes from "../navigation/routes";
import ridersApi from "../api/riders";
import shopApi from "../api/shop";

const validationSchema = Yup.object().shape({
  name: Yup.string().required().label("name"),
  email: Yup.string().required().email().label("email"),
  phone: Yup.string()
    .required()
    .min(11, "phone number starts with 0 and 11 characters")
    .max(11, "phone number starts with 0 and 11 characters")
    .label("phone number"),
  images: Yup.array().min(1, "Please select at least one image."),
});

function AdminNewRiderScreen({ navigation }) {
  const [error, setError] = useState();
  const [selectedShop, setSelectedShop] = useState();
  const [saving, setSaving] = useState(false);
  const [shops, setShops] = useState();
  const pickerRef = useRef();
  const [progress, setProgress] = useState(0);
  const [uploadVisible, setUploadVisible] = useState(false);

  useEffect(() => {
    const getShops = async () => {
      const result = await shopApi.getShops();
      if (result) {
        setShops(result.data);
      }
    };
    getShops();
  }, []);

  const handleSubmit = async (userInfo, { resetForm }) => {
    if (selectedShop !== "-") {
      setSaving(true);
      const result = await ridersApi.addNewRider(
        userInfo.name,
        userInfo.email,
        userInfo.phone,
        userInfo.images,
        selectedShop
      );
      if (result.ok) {
        setSaving(false);
        alert("Rider created");
        resetForm();
      } else {
        setSaving(false);
        alert("Something went wrong");
      }
      setUploadVisible(false);
      navigation.navigate(routes.AdminHome);
    } else {
      alert("Please pick shop info");
    }
  };

  const close = () => {
    navigation.navigate(routes.AdminHome);
  };

  return (
    <Screen style={styles.container}>
      <ActivityIndicator visible={saving} />
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          flexDirection: "row",
          justifyContent: "flex-start",
          height: 60,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Add New Rider
          </Text>
        </>
      </TouchableOpacity>
      <View style={{ alignSelf: "center", flex: 1 }}>
        <UploadScreen
          onDone={() => setUploadVisible(false)}
          progress={progress}
          visible={uploadVisible}
        />
        <KeyboardAwareScrollView>
          <Form
            initialValues={{ name: "", email: "", password: "", images: [] }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <ErrorMessage error={error} visible={error} />
            <FormImagePicker name="images" />
            <FormField
              autoCorrect={false}
              icon={require("../assets/png/Iconly-Light-outline-Profile.png")}
              name="name"
              placeholder="Name"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon={require("../assets/png/Iconly-Light-outline-Message.png")}
              keyboardType="email-address"
              name="email"
              placeholder="Email"
              textContentType="emailAddress"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon={require("../assets/png/Iconly-Light-outline-Call.png")}
              keyboardType="phone-pad"
              name="phone"
              placeholder="i.e 07123456789"
              textContentType="telephoneNumber"
            />
            <Text
              style={{
                fontSize: 18,
                fontFamily: "popSemiBold",
                alignSelf: "center",
                top: 10,
              }}
            >
              Employed one of below shops?
            </Text>
            <Picker
              ref={pickerRef}
              selectedValue={selectedShop}
              onValueChange={(value) => setSelectedShop(value)}
              mode="dropdown"
            >
              <Picker.Item label="Pick below" value="-" />
              <Picker.Item
                label="Self Employed"
                value="99999999-9999-9999-9999-999999999999"
              />
              {shops &&
                shops.map((shop) => {
                  return (
                    <Picker.Item
                      label={shop.name}
                      value={shop.shopKey}
                      key={shop.shopKey}
                    />
                  );
                })}
            </Picker>
            <View style={{ backgroundColor: colors.blue }}>
              <SubmitButton
                title="Add New Rider"
                style={styles.submitBtn}
                autoCapitalize="none"
              />
            </View>
          </Form>
        </KeyboardAwareScrollView>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
  policy: {
    color: colors.darkGray,
    justifyContent: "center",
    paddingHorizontal: 20,
    fontSize: 10,
    fontFamily: "popLight",
    alignSelf: "center",
  },
  resendEnabled: {
    alignSelf: "center",
    color: colors.blue,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  resendDisabled: {
    alignSelf: "center",
    color: colors.gray,
    fontFamily: "popRegular",
    fontSize: 19,
  },
});

export default AdminNewRiderScreen;
