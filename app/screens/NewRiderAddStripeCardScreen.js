import React, { useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import { CardField, useConfirmSetupIntent } from "@stripe/stripe-react-native";

import Screen from "../components/Screen";
import paymentApi from "../api/payment";
import colors from "../config/colors";
import useAuth from "../auth/useAuth";
import authStorage from "../auth/storage";

function NewRiderAddStripeCardScreen({ navigation }) {
  const { confirmSetupIntent, loading } = useConfirmSetupIntent();
  const [saveLoading, setSaveLoading] = useState(false);
  const [user, setUser] = useState();
  const auth = useAuth();

  const savePaymentMethod = async () => {
    setSaveLoading(true);
    const result = await paymentApi.setupIntentRider();
    setSaveLoading(false);
    const clientSecret = result.data;
    const { setupIntent, error } = await confirmSetupIntent(clientSecret, {
      type: "Card",
    });
    if (error) {
      console.log(error);
    }
    if (setupIntent && setupIntent.status === "Succeeded") {
      const result = await paymentApi.chargeRiderSetupCosts();
      if (result && result.data) {
        auth.logIn(result.data);
        const user = await authStorage.getUser();
        if (user) {
          setUser(user);
        }
      }
    }
  };

  return (
    <>
      {user && user.status === "ACTIVE" ? (
        <UserNavigator />
      ) : (
        <Screen>
          <View style={{ top: 150 }}>
            <Text
              style={{
                color: colors.darkGray,
                fontFamily: "popSemiBold",
                fontSize: 18,
                alignSelf: "center",
              }}
            >
              Please add payment method
            </Text>
            <CardField
              postalCodeEnabled={false}
              placeholder={{
                number: "4242 4242 4242 4242",
              }}
              cardStyle={{
                backgroundColor: "#FFFFFF",
                textColor: "#000000",
              }}
              style={{
                width: "100%",
                height: 50,
                marginVertical: 30,
              }}
            />
            <Button
              onPress={savePaymentMethod}
              title="Save payment method"
              loading={loading}
              disabled={loading}
            />
          </View>
        </Screen>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
});

export default NewRiderAddStripeCardScreen;
