import React, { useEffect, useState } from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import colors from "../../config/colors";

export default function RiderHomeScreenHeader(props) {
  const { data } = props;

  const [profilePicture, setProfilePicture] = useState(
    "https://solidbike.s3.eu-west-2.amazonaws.com/users/profiles/default/default.png"
  );
  const [name, setName] = useState("");

  useEffect(() => {
    if (data) {
      setProfilePicture(data.profilePicture);
      setName(data.name);
    }
  }, [data]);

  return (
    <View style={styles.top}>
      <View style={styles.prof}>
        <Image
          style={styles.profImage}
          source={{
            uri: profilePicture,
          }}
        />
        <Text style={styles.txtName}>Hi, {name}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  top: {
    // flex: 0.65,
    // width: "100%",
    backgroundColor: colors.darkGray,
  },
  prof: {
    flexDirection: "row",
    paddingHorizontal: 20,
    // top: 5,
    backgroundColor: colors.white,
  },
  profImage: {
    alignSelf:"center",
    width: 45,
    height: 45,
    borderRadius: 24,
    overflow: "hidden",
  },
  txtName: {
    color: colors.darkGray,
    padding: 20,
    fontSize: 16,
    fontFamily: "popRegular",
  },
  announce: {
    color: colors.newWhite,
  },
});
