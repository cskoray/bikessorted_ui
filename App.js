import "expo-dev-client";
import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import AppLoading from "expo-app-loading";
import { StatusBar } from "expo-status-bar";
import { StripeProvider } from "@stripe/stripe-react-native";
import { useFonts } from "expo-font";
import * as Sentry from "sentry-expo";

import navigationTheme from "./app/navigation/navigationTheme";
import OfflineNotice from "./app/components/OfflineNotice";
import AuthNavigator from "./app/navigation/AuthNavigator";
import AuthContext from "./app/auth/context";
import BookingContext from "./app/auth/bookingContext";
import authStorage from "./app/auth/storage";
import { navigationRef } from "./app/navigation/rootNavigation";
import UserNavigator from "./app/navigation/UserNavigator";
import RiderNavigator from "./app/navigation/RiderNavigator";
import AdminNavigator from "./app/navigation/AdminNavigator";
import ShopAdminNavigator from "./app/navigation/ShopAdminNavigator";
import usersApi from "./app/api/users";

Sentry.init({
  dsn: "https://e0576d2fe29f41abb8f7691efb6dae9f@o976506.ingest.sentry.io/5932810",
  enableInExpoDevelopment: true,
  debug: true, // Sentry will try to print out useful debugging information if something goes wrong with sending an event. Set this to `false` in production.
  // enableOutOfMemoryTracking = true // false in production??
});

export default function App() {
  const [user, setUser] = useState();
  const [booking, setBooking] = useState({
    booking: {
      bookingKey: "-",
    },
    bike: {
      bikeKey: "-",
    },
    service: {
      services: [],
      serviceKeys: [],
      type: "-",
    },
    payment: {
      cardKey: "-",
    },
    location: {
      latitude: "-",
      longitude: "-",
    },
    calendar: {
      dateTime: "-",
    },
    tip: {
      type: "Z",
    },
    points: {
      use: "n",
      amount: 0,
    },
  });
  const [isReady, setIsReady] = useState(false);
  const [stripeKey, setStripeKey] = useState("");
  const [stripeMerchantId, setStripeMerchantId] = useState("");

  const [loaded] = useFonts({
    popLight: require("./app/assets/fonts/poppins/Light.ttf"),
    popRegular: require("./app/assets/fonts/poppins/Regular.ttf"),
    popMedium: require("./app/assets/fonts/poppins/Medium.ttf"),
    popSemiBold: require("./app/assets/fonts/poppins/SemiBold.ttf"),
    popBold: require("./app/assets/fonts/poppins/Bold.ttf"),
  });

  const restoreUser = async () => {
    const result = await usersApi.getStripeKey();
    if (result && result.data) {
      setStripeKey(result.data.publisableKey);
      setStripeMerchantId(result.data.merchantId);
    }
    const user = await authStorage.getUser();
    if (user) {
      setUser(user);
    }
  };

  if (!isReady || !loaded) {
    return (
      <AppLoading
        startAsync={restoreUser}
        onFinish={() => setIsReady(true)}
        onError={console.warn}
      />
    );
  }

  return (
    <>
      <StatusBar barStyle="dark" />
      <StripeProvider
        publishableKey={stripeKey}
        merchantIdentifier={stripeMerchantId}
      >
        <AuthContext.Provider value={{ user, setUser }}>
          <BookingContext.Provider value={{ booking, setBooking }}>
            <OfflineNotice />
            <NavigationContainer ref={navigationRef} theme={navigationTheme}>
              {user && user.status === "ACTIVE" ? (
                user.scope === "USER" ? (
                  <UserNavigator />
                ) : user.scope === "ADMIN" ? (
                  <AdminNavigator />
                ) : user.scope === "SHOP_ADMIN" ? (
                  <ShopAdminNavigator />
                ) : (
                  <RiderNavigator />
                )
              ) : (
                <AuthNavigator />
              )}
            </NavigationContainer>
          </BookingContext.Provider>
        </AuthContext.Provider>
      </StripeProvider>
    </>
  );
}
