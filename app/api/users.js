import client from "./client";

const register = (userInfo) => client.post("/users", userInfo);

const applyAsRider = () => client.post("/users/apply");

const isApplied = () => client.get("/users/applied");

const getStripeKey = () => client.get("/payment/publishable-key");

export default { register, applyAsRider, isApplied, getStripeKey };
