import React from "react";
import { StyleSheet, Image } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import colors from "../config/colors";
import RiderHomeNavigator from "./RiderHomeNavigator";
import RiderDrawerContent from "../screens/drawer/RiderDrawerContent";

const Drawer = createDrawerNavigator();

const UserNavigator = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <RiderDrawerContent {...props} />}
      drawerStyle={{
        backgroundColor: colors.blue,
        width: "80%",
        shadowRadius: 5,
        shadowOffset: {
          width: 5,
          height: 10,
        },
        shadowOpacity: 0.2,
        flex: 1,
      }}
      hideStatusBar="true"
    >
      <Drawer.Screen
        name="Booking"
        component={RiderHomeNavigator}
        options={{
          title: "Home",
          headerTintColor: colors.newWhite,
          drawerIcon: ({ focused, size }) => (
            <Image
              source={require("../assets/mosh.jpg")}
              style={[focused ? styles.drawerActive : styles.drawerInActive]}
            />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  container: { alignItems: "center" },
  drawerActive: {
    width: 45,
    height: 45,
    borderRadius: 24,
    overflow: "hidden",
  },
});

export default UserNavigator;
