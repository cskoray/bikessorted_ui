import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "../screens/HomeScreen";
import PickLocationScreen2 from "../screens/PickLocationScreen2";
import AddStripeCardScreen from "../screens/AddStripeCardScreen";
import AddBikeScreen from "../screens/AddBikeScreen";
import BikesScreen from "../screens/BikesScreen";
import WalletScreen from "../screens/WalletScreen";
import BookingsScreen from "../screens/BookingsScreen";
import CalendarScreen from "../screens/CalendarScreen";
import CheckoutScreen from "../screens/CheckoutScreen";
import BookingConfirmationScreen from "../screens/BookingConfirmationScreen";
import ActiveBookingDetailScreen from "../screens/ActiveBookingDetailScreen";
import ActiveLaterBookingDetailScreen from "../screens/ActiveLaterBookingDetailScreen";
import RemovePaymentMethodScreen from "../screens/RemovePaymentMethodScreen";
import RiderApplyScreen from "../screens/RiderApplyScreen";
import EditBikeScreen from "../screens/EditBikeScreen";
import BookingCancelledScreen from "../screens/BookingCancelledScreen";
import ApproveRiderOrderedScreen from "../screens/ApproveRiderOrderedScreen";

const Stack = createStackNavigator();

const HomeNavigator = () => (
  <Stack.Navigator
    screenOptions={{ headerShown: false, gestureEnabled: false }}
  >
    <Stack.Screen name="Home" component={HomeScreen} />
    <Stack.Screen name="PickLocation2" component={PickLocationScreen2} />
    <Stack.Screen name="AddStripeCard" component={AddStripeCardScreen} />
    <Stack.Screen name="Bikes" component={BikesScreen} />
    <Stack.Screen name="AddBike" component={AddBikeScreen} />
    <Stack.Screen name="Wallet" component={WalletScreen} />
    <Stack.Screen name="Bookings" component={BookingsScreen} />
    <Stack.Screen name="Calendar" component={CalendarScreen} />
    <Stack.Screen name="Checkout" component={CheckoutScreen} />
    <Stack.Screen
      name="BookingConfirmationScreen"
      component={BookingConfirmationScreen}
    />
    <Stack.Screen
      name="ActiveBookingDetailScreen"
      component={ActiveBookingDetailScreen}
    />
    <Stack.Screen
      name="ActiveLaterBookingDetailScreen"
      component={ActiveLaterBookingDetailScreen}
    />
    <Stack.Screen
      name="RemovePaymentMethodScreen"
      component={RemovePaymentMethodScreen}
    />
    <Stack.Screen name="RiderApplyScreen" component={RiderApplyScreen} />
    <Stack.Screen name="EditBikeScreen" component={EditBikeScreen} />
    <Stack.Screen
      name="BookingCancelledScreen"
      component={BookingCancelledScreen}
    />
    <Stack.Screen
      name="ApproveRiderOrderedScreen"
      component={ApproveRiderOrderedScreen}
    />
  </Stack.Navigator>
);

export default HomeNavigator;
