import React from "react";
import { Image, Text, TouchableOpacity, View, StyleSheet } from "react-native";
import colors from "../../config/colors";

export default function AllTypeServicesComponent(props) {
  const { setArrival, setEmergencySelected, setServicePopUpSelected } = props;

  const handleOnPress = () => {
    let d = new Date();
    let minutesToAdd = 30;
    setArrival(new Date(d.getTime() + minutesToAdd * 60000));
    setEmergencySelected(true);
  };

  return (
    <View style={styles.mid}>
      <TouchableOpacity onPress={handleOnPress}>
        <View style={styles.row1}>
          <View style={styles.emergency}>
            <Text style={styles.txt1}>Emergency </Text>
            <Text style={styles.txt2}>Puncture Help</Text>
            <Image
              style={styles.tyreImage}
              source={require("../../assets/png/EmergencyHelpTyre.png")}
            />
            <TouchableOpacity onPress={handleOnPress}>
              <Text style={styles.det}>select</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.emergencyTxt}>
            <Text style={styles.txt5}>Flat tyre?</Text>
            <Text style={styles.txt5}>We'll be there in 30 mins</Text>
          </View>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setServicePopUpSelected(true)}>
        <View style={styles.row2}>
          <View style={styles.servicesTxt}>
            <Text style={styles.txt5}>Let's make it ready to ride...</Text>
          </View>
          <View style={styles.services}>
            <Text style={styles.txt4}>Bike Services</Text>
            <Text style={styles.txt3}>anytime</Text>
            <Text style={styles.txt6}>anywhere</Text>
            <Image
              style={styles.tyreImage}
              source={require("../../assets/png/ToolBox.png")}
            />
            <TouchableOpacity onPress={() => setServicePopUpSelected(true)}>
              <Text style={styles.det}>select</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  mid: {
    flex: 4,
    height: 230,
    width: "100%",
    top: 5,
  },
  row1: {
    flexDirection: "row",
    top: 10,
  },
  row2: {
    flexDirection: "row",
    top: 5,
    display: "flex",
  },
  emergency: {
    padding: 20,
    bottom: 10,
    borderRadius: 10,
    backgroundColor: "white",
    width: 160,
    height: 220,
    marginTop: 10,
    marginLeft: 20,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.3,
  },
  txt1: { fontSize: 16, fontFamily: "popSemiBold" },
  txt2: { fontSize: 14, paddingTop: 10, fontFamily: "popSemiBold", left: 10 },
  det: {
    fontSize: 13,
    fontFamily: "popSemiBold",
    color: colors.blue,
    marginLeft: 90,
    marginTop: 20,
    width: "100%",
  },
  tyreImage: {
    marginTop: 25,
    width: 50,
    height: 50,
    alignSelf: "center",
  },
  txt3: { fontSize: 14, paddingTop: 5, fontFamily: "popSemiBold", left: 5 },
  txt4: { fontSize: 16, fontFamily: "popSemiBold" },
  txt5: { fontSize: 16, fontFamily: "popSemiBold" },
  txt6: { fontSize: 14, paddingTop: 5, fontFamily: "popSemiBold", left: 15 },
  emergencyTxt: {
    justifyContent: "center",
    left: 15,
    fontFamily: "popSemiBold",
    fontSize: 16,
  },
  servicesTxt: {
    position: "absolute",
    justifyContent: "center",
    height: 200,
    zIndex: 1,
    padding: 20,
    width: "50%",
  },
  services: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: "white",
    width: 160,
    height: 220,
    marginTop: 0,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.3,
    top: 5,
    marginBottom: 15,
    marginLeft: "auto",
    right: 10,
  },
});
