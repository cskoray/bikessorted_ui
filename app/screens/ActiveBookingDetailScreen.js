import React, { useEffect, useState } from "react";
import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  Modal,
  Button,
} from "react-native";
import { Animated, MarkerAnimated } from "react-native-maps";
import { Avatar } from "react-native-paper";
import Constants from "expo-constants";
import { Rating } from "react-native-ratings";

import colors from "../config/colors";
import routes from "../navigation/routes";
import bookingApi from "../api/booking";
import locationApi from "../api/location";

function ActiveBookingDetailScreen({ route, navigation }) {
  const [modalVisible, setModalVisible] = useState(false);
  const [reported, setReported] = useState(false);
  const [lat, setLatitude] = useState(route.params.latitude);
  const [long, setLongitude] = useState(route.params.longitude);
  var interval;

  const {
    bookingKey,
    totalAmount,
    type,
    name,
    phone,
    riderPic,
    riderRate,
    dueDate,
    services,
  } = route.params;

  const checkForRiderLocation = async () => {
    const result = await locationApi.getRiderLocation(bookingKey);
    if (
      result &&
      result.data &&
      result.data.latitude &&
      result.data.longitude
    ) {
      setLatitude(result.data.latitude);
      setLongitude(result.data.longitude);
    }
  };

  useEffect(() => {
    navigation.addListener("focus", () => {
      interval = setInterval(() => {
        checkForRiderLocation();
        const isReported = async () => {
          const result = await bookingApi.isReported(bookingKey);
          if (result && result.data) {
            setReported(result.data.status);
          }
        };
        isReported();
      }, 20000);
      return () => {
        if (!name) {
          clearInterval(interval);
          clearTimeout(interval);
        }
      };
    });
  }, [lat, long]);

  const reportIt = () => {
    setModalVisible(true);
  };

  const cancel = () => {
    setModalVisible(false);
  };

  const close = () => {
    clearInterval(interval);
    navigation.navigate(routes.HOME);
  };

  const cancelBooking = async () => {
    Alert.alert(
      "Cancel Booking",
      "Are you sure want to cancel?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.userCancelBooking(bookingKey);
            if (result && result.ok) {
              navigation.navigate(routes.HOME);
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  const complain = async () => {
    Alert.alert(
      "Report Booking",
      "Are you sure want to report?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.complain(bookingKey);
            if (result && result.ok) {
              alert("Thanks! We will get back to you as soon as possible");
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <>
      {Platform.OS === "ios" ? (
        <Animated
          style={{ flex: 1 }}
          mapType="mutedStandard"
          showsMyLocationButton
          zoomControlEnabled
          zoomEnabled
          provider={"google"}
          paddingAdjustmentBehavior="automatic"
          initialRegion={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.01222,
            longitudeDelta: 0.01421,
          }}
          mapType="mutedStandard"
          region={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.01222,
            longitudeDelta: 0.01421,
          }}
          pitchEnabled={false}
          rotateEnabled={false}
          scrollEnabled={false}
        >
          <MarkerAnimated
            coordinate={{
              latitude: parseFloat(lat),
              longitude: parseFloat(long),
            }}
          >
            <Image
              source={require("../assets/png/Repairman.png")}
              style={{
                height: 25,
              }}
              resizeMode="contain"
            />
          </MarkerAnimated>
        </Animated>
      ) : (
        <Animated
          style={{ flex: 1 }}
          mapType="mutedStandard"
          showsMyLocationButton
          zoomControlEnabled
          zoomEnabled
          provider={"google"}
          paddingAdjustmentBehavior="automatic"
          initialRegion={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.01222,
            longitudeDelta: 0.01421,
          }}
          mapType="mutedStandard"
          region={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.01222,
            longitudeDelta: 0.01421,
          }}
          pitchEnabled={false}
          rotateEnabled={false}
          scrollEnabled={false}
        >
          <MarkerAnimated
            coordinate={{
              latitude: parseFloat(lat),
              longitude: parseFloat(long),
            }}
          >
            <Image
              source={require("../assets/png/Repairman.png")}
              style={{
                height: 25,
              }}
              resizeMode="contain"
            />
          </MarkerAnimated>
        </Animated>
      )}
      <TouchableOpacity
        style={{
          top: Constants.statusBarHeight,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          justifyContent: "flex-start",
          marginBottom: 20,
          height: 60,
          position: "absolute",
          zIndex: 9999,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            service is on it's way
          </Text>
        </>
      </TouchableOpacity>
      <View
        style={{
          position: "absolute",
          backgroundColor: colors.darkGray,
          borderRadius: 15,
          top: "74%",
          left: "5%",
          width: "90%",
          height: "20%",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            top: "50%",
            padding: 5,
            position: "absolute",
          }}
        >
          <Avatar.Image
            source={{
              uri: riderPic,
            }}
            size={55}
          />
          <Text
            style={{
              fontSize: 20,
              fontFamily: "popSemiBold",
              color: colors.white,
              left: "25%",
            }}
          >
            {name}
          </Text>
        </View>
        <Rating
          type="custom"
          readonly
          startingValue={riderRate}
          ratingCount={5}
          imageSize={60}
          showRating={false}
          style={{
            top: "70%",
            alignSelf: "flex-start",
            left: "20%",
            padding: 5,
            position: "absolute",
          }}
          tintColor="#2D3640"
          imageSize={18}
          ratingBackgroundColor="#c8c7c8"
        />
        <View
          style={{
            position: "absolute",
            backgroundColor: colors.white,
            borderRadius: 15,
            // top: "60%",
            // left: "5%",
            width: "100%",
            height: "45%",
            elevation: 15,
            shadowRadius: 5,
            shadowOffset: {
              height: -5,
            },
            shadowOpacity: 0.5,
          }}
        >
          <TouchableOpacity style={styles.report} onPress={reportIt}>
            <Text style={styles.btntxt}>report problem</Text>
          </TouchableOpacity>
          <View style={{ padding: 5 }}>
            <Image
              source={require("../assets/png/Time.png")}
              style={{ position: "absolute", width: 50, height: 50, margin: 5 }}
            />
            <View style={{ left: 70 }}>
              <Text style={styles.txt}>Arrives before</Text>
              <Text style={styles.txt}>{dueDate}</Text>
            </View>
          </View>
        </View>
      </View>
      <Modal animationType="slide" visible={modalVisible} transparent={true}>
        <View
          style={{
            height: "45%",
            marginTop: "auto",
            backgroundColor: colors.white,
            marginHorizontal: "2%",
            borderRadius: 10,
          }}
        >
          <Button style={styles.done} title="close" onPress={cancel} />
          <View style={{ top: 50, padding: 5 }}>
            <TouchableOpacity
              onPress={cancelBooking}
              style={{
                backgroundColor: colors.newWhite,
                height: 55,
                justifyContent: "center",
                width: "100%",
                borderRadius: 5,
              }}
            >
              <Image
                source={require("../assets/png/Iconly-Light-outline-CloseSquare.png")}
                style={{ position: "absolute", width: 30, height: 30 }}
                resizeMode="contain"
              />
              <Text
                style={{
                  left: 40,
                  fontFamily: "popRegular",
                  fontSize: 18,
                }}
              >
                cancel booking
              </Text>
            </TouchableOpacity>
            {!reported && (
              <TouchableOpacity
                style={{
                  backgroundColor: colors.newWhite,
                  height: 55,
                  justifyContent: "center",
                  width: "100%",
                  borderRadius: 5,
                  top: 20,
                }}
                onPress={complain}
              >
                <Image
                  source={require("../assets/png/Iconly-Light-outline-Work.png")}
                  style={{ position: "absolute", width: 30, height: 30 }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    left: 40,
                    fontFamily: "popRegular",
                    fontSize: 18,
                  }}
                >
                  unhappy with technician
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  txt: {
    fontFamily: "popSemiBold",
    fontSize: 18,
  },
  report: {
    right: 20,
    justifyContent: "center",
    position: "absolute",
    width: 100,
    height: 50,
    zIndex: 1,
  },
  btntxt: {
    color: colors.blue,
    alignSelf: "flex-end",
    fontFamily: "popSemiBold",
    fontSize: 10,
  },
  ImageIconStyle: {
    margin: 10,
    width: 40,
    height: 40,
    top: 0,
  },
  image2: {
    width: 60,
    height: 30,
    alignSelf: "center",
    left: 90,
  },
  done: {
    backgroundColor: "black",
    borderRadius: 0,
    height: 60,
    padding: 20,
    alignSelf: "center",
    top: 100,
  },
});

export default ActiveBookingDetailScreen;
