import client from "./client";

const getEmergencyService = () => client.get("/services/flat");

const getServices = () => client.get("/services");

export const getOptionalServices = (serviceToOrder) => {
  const data = { serviceKeys: serviceToOrder };
  return client.post("/services/optional", data);
};

export default {
  getEmergencyService,
  getServices,
  getOptionalServices,
};
