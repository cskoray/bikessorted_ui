import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import * as Yup from "yup";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";

import Screen from "../../components/Screen";
import colors from "../../config/colors";

const validationSchema = Yup.object().shape({
  email: Yup.string().required().email().label("email"),
});

function GetEmailForgotPasswordScreen({ navigation }) {
  const [error, setError] = useState();

  const handleSubmit = async (userInfo) => {
    const params = { userInfo: userInfo.email };
    navigation.navigate("ForgotPassword", params);
  };

  return (
    <Screen style={styles.container}>
      <View>
        <Form
          initialValues={{ email: "" }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <ErrorMessage error={error} visible={error} />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon={require("../../assets/png/Iconly-Light-outline-Message.png")}
            keyboardType="email-address"
            name="email"
            placeholder="Email"
            textContentType="emailAddress"
          />
          <View style={{ top: 50, backgroundColor: colors.blue }}>
            <SubmitButton
              title="send verification code"
              style={styles.submitBtn}
              autoCapitalize="none"
            />
          </View>
        </Form>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    justifyContent: "center",
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
  policy: {
    color: colors.darkGray,
    justifyContent: "center",
    paddingHorizontal: 20,
    fontSize: 10,
    fontFamily: "popLight",
    alignSelf: "center",
  },
});

export default GetEmailForgotPasswordScreen;
