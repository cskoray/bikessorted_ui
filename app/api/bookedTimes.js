import client from "./client";

const getBookedTimes = () => client.get("/calendar/times");

export default {
  getBookedTimes,
};
