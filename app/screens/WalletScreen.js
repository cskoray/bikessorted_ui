import React, { useState, useEffect } from "react";
import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from "react-native";

import WalletCard from "../components/WalletCard";
import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import paymentApi from "../api/payment";
import ActivityIndicator from "../components/ActivityIndicator";
import CheckoutScreen from "./CheckoutScreen";

function WalletScreen({ route, navigation }) {
  const { booking, setBooking } = useAuth();
  const paymentMethods = useApi(paymentApi.getPaymentMethods);

  useEffect(() => {
    paymentMethods.request();
    const unsubscribe = navigation.addListener("focus", () => {
      paymentMethods.request();
    });
  }, [navigation]);

  const removePayment = (paymentMethod) => {
    let params = {
      id: paymentMethod.id,
      brand: paymentMethod.brand,
      last4: paymentMethod.last4,
    };
    navigation.navigate(routes.RemovePaymentMethod, params);
  };

  const onPressAction = () => {
    if (route.params && route.params.refer === "drawer") {
      navigation.navigate(routes.HOME);
    } else {
      if (booking.service.type === "INSTANT") {
        navigation.navigate(routes.PICK_LOCATION);
      }
      if (booking.service.type === "LATER") {
        navigation.navigate(routes.BIKES);
      }
    }
  };

  return (
    <>
      {!paymentMethods.data ||
      !paymentMethods.data.paymentMethods ||
      (route.params && route.params.refer === "drawer") ? (
        <Screen style={styles.screen}>
          <View style={{ flex: 2.8 }}>
            <TouchableOpacity
              style={{
                padding: 0,
                margin: 0,
                width: "100%",
                left: 0,
                backgroundColor: colors.darkGray,
                flexDirection: "row",
                alignContent: "center",
                marginBottom: 20,
              }}
              onPress={onPressAction}
            >
              <>
                {route.params && route.params.refer === "drawer" ? (
                  <Image
                    source={require("../assets/png/ArrowCloseCircle.png")}
                    style={styles.ImageIconStyle}
                  />
                ) : (
                  <Image
                    source={require("../assets/png/ArrowLeftCircle.png")}
                    style={styles.ImageIconStyle}
                  />
                )}
                <Text
                  style={{
                    color: colors.newWhite,
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "center",
                    left: 20,
                  }}
                >
                  Wallet
                </Text>
              </>
            </TouchableOpacity>
            <ActivityIndicator visible={paymentMethods.loading} />
            {paymentMethods.error && (
              <>
                <AppText>Couldn't retrieve the listings.</AppText>
                <Button
                  title="Retry"
                  onPress={() => paymentMethods.request()}
                />
              </>
            )}
            <View>
              {paymentMethods.data &&
              paymentMethods.data.paymentMethods &&
              paymentMethods.data.paymentMethods.length > 0 ? (
                <FlatList
                  data={paymentMethods.data.paymentMethods}
                  keyExtractor={(key) => key.last4}
                  renderItem={({ item }) => (
                    <WalletCard
                      brand={item.brand}
                      number={item.last4}
                      onPress={() => removePayment(item)}
                    />
                  )}
                />
              ) : (
                <View>
                  <Image
                    source={require("../assets/png/wallet.png")}
                    style={styles.addPaymentMethodImgStyle}
                  />
                </View>
              )}
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate(routes.ADD_STRIPE_CARD)}
              style={{ height: 70, padding: 10 }}
            >
              <View>
                <Text
                  style={{
                    color: colors.darkGray,
                    fontFamily: "popSemiBold",
                    fontSize: 16,
                    alignSelf: "center",
                  }}
                >
                  add new payment method
                </Text>
                <Image
                  source={require("../assets/png/Iconly-Light-outline-Plus.png")}
                  style={styles.addPaymentMethodPlusStyle}
                />
              </View>
            </TouchableOpacity>
          </View>
        </Screen>
      ) : (
        <CheckoutScreen />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  addPaymentMethodImgStyle: {
    width: 322,
    height: 226,
    alignSelf: "center",
  },
  addPaymentMethodPlusStyle: {
    width: 18,
    height: 18,
    alignSelf: "center",
    top: 10,
  },
  viewBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  viewGray: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
});

export default WalletScreen;
