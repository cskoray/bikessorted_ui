import React from "react";
import LottieView from "lottie-react-native";
import { View, StyleSheet } from "react-native";

function ActivityIndicatorBooking({ visible = false }) {
  if (!visible) return null;

  return (
    <View style={styles.overlay}>
      <LottieView
        autoPlay
        loop
        source={require("../assets/animations/bookingLoading.json")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  overlay: {
    top:70,
    alignSelf:"center",
    backgroundColor: "white",
    height: 100,
    opacity: 1,
    width: 100,
    zIndex: 1,
  },
});

export default ActivityIndicatorBooking;
