import React, { useState, useEffect } from "react";
import {
  Animated,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { createStackNavigator } from "@react-navigation/stack";
import { Platform } from "react-native";
import CountDown from "react-native-countdown-component";

import authApi from "../../api/auth";
import Screen from "../../components/Screen";
import colors from "../../config/colors";
import useAuth from "../../auth/useAuth";
import AddPhoneNumberScreen from "./AddPhoneNumberScreen";
import authStorage from "../../auth/storage";
import routes from "../../navigation/routes";
import CheckPhoneNumberActivatedScreen from "./CheckPhoneNumberActivatedScreen";

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";

const { Value, Text: AnimatedText } = Animated;
const CELL_COUNT = 6;
const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({ hasValue, index, isFocused }) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

function CheckEmailActivatedScreen({ navigation }) {
  const [value, setValue] = useState("");
  const [userTriedCodeStatus, setUserTriedCodeStatus] = useState("");
  const [resendDisabled, setResendDisabled] = useState(true);
  const [timez, setTimez] = useState("0");
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  const [user, setUser] = useState();
  const Stack = createStackNavigator();
  const [verificationFailed, setVerificationFailed] = useState(false);
  const auth = useAuth();

  useEffect(() => {
    const userState = async () => {
      const user = await authStorage.getUser();
      if (user) {
        setUser(user);
      }
    };
    userState();
  }, [user]);

  const handleSubmit = async (code) => {
    if (code) {
      const result = await authApi.verifyEmail(code);
      if (result) {
        if (result.ok) {
          auth.logIn(result.data);
          const user = await authStorage.getUser();
          if (user) {
            setUser(user);
          }
        } else {
          if (result.data.errors[0].code === "10001") {
            setUserTriedCodeStatus("INVALID");
          }
        }
      }
    }
  };

  const handleCodeAndroid = async () => {
    if (value) {
      const result = await authApi.verifyEmail(value);
      if (result) {
        if (result.ok) {
          auth.logIn(result.data);
          const user = await authStorage.getUser();
          if (user) {
            setUser(user);
          }
        } else {
          if (result.data.errors[0].code === "10001") {
            setUserTriedCodeStatus("INVALID");
          }
        }
      }
    }
  };

  const close = () => {
    setUser(null);
    authStorage.removeToken();
    navigation.navigate(routes.LOGIN);
  };

  const timeUpCancel = async () => {
    setResendDisabled(false);
  };

  const resend = async () => {
    let n = Number(timez);
    n = n + 1;
    setTimez(String(n));
    setResendDisabled(true);
    if (n >= 3) {
      const result = await authApi.suspendUser("EMAIL");
      if (result && result.ok) {
        auth.logIn(result.data);
        const user = await authStorage.getUser();
        setUser(user);
      }
    } else {
      const result = await authApi.resend("EMAIL");
      if (result && result.ok) {
        auth.logIn(result.data);
        const user = await authStorage.getUser();
        setUser(user);
      }
    }
  };

  const renderCell = ({ index, symbol, isFocused }) => {
    const hasValue = Boolean(symbol);
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: ["#3557b7", "#f7fafe"],
          })
        : animationsColor[index].interpolate({
            inputRange: [0, 1],
            outputRange: ["#fff", "#f7fafe"],
          }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [55, 8],
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.2, 1],
          }),
        },
      ],
    };

    // Run animation on next event loop tik
    // Because we need first return new style prop and then animate this value
    setTimeout(() => {
      animateCell({ hasValue, index, isFocused });
    }, 0);

    return (
      <AnimatedText
        key={index}
        style={[styles.cell, animatedCellStyle]}
        onLayout={getCellOnLayoutHandler(index)}
      >
        {symbol || (isFocused ? <Cursor /> : null)}
      </AnimatedText>
    );
  };
  return (
    <>
      {user && user.status === "EMAIL_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="AddPhoneNumber"
            component={AddPhoneNumberScreen}
          />
        </Stack.Navigator>
      ) : user && user.status === "PHONE_NOT_VERIFIED" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="CheckPhoneNumberActivated"
            component={CheckPhoneNumberActivatedScreen}
          />
        </Stack.Navigator>
      ) : (
        <>
          {user && user.status !== "SUSPENDED" ? (
            <Screen style={styles.container}>
              <CountDown
                id={timez}
                size={19}
                until={59}
                onFinish={timeUpCancel}
                digitStyle={{
                  backgroundColor: colors.light,
                  bottom: 9,
                }}
                timeLabelStyle={{ fontWeight: "normal" }}
                separatorStyle={{ bottom: 10, color: colors.gray }}
                timeToShow={["M", "S"]}
                timeLabels={{ m: null, s: null }}
                digitTxtStyle={{ color: colors.gray }}
                showSeparator
              />
              {userTriedCodeStatus === "INVALID" && (
                <Text style={{ alignSelf: "center", color: "red" }}>
                  invalid code
                </Text>
              )}
              {Platform.OS === "ios" ? (
                <>
                  <OTPInputView
                    style={{ width: "100%", height: 100 }}
                    pinCount={6}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    onCodeFilled={(code) => handleSubmit(code)}
                  />
                  <Text style={{ alignSelf: "center" }}>
                    We have sent a verification code to your email
                  </Text>
                  <TouchableOpacity onPress={resend} disabled={resendDisabled}>
                    <Text
                      style={
                        resendDisabled
                          ? styles.resendDisabled
                          : styles.resendEnabled
                      }
                    >
                      resend code
                    </Text>
                  </TouchableOpacity>
                </>
              ) : (
                <>
                  <CodeField
                    ref={ref}
                    {...props}
                    value={value}
                    onChangeText={setValue}
                    cellCount={CELL_COUNT}
                    rootStyle={styles.codeFiledRoot}
                    keyboardType="number-pad"
                    textContentType="oneTimeCode"
                    renderCell={renderCell}
                  />
                  <TouchableOpacity onPress={handleCodeAndroid}>
                    <View style={styles.nextButton}>
                      <Text style={styles.nextButtonText}>Verify</Text>
                    </View>
                  </TouchableOpacity>
                </>
              )}
            </Screen>
          ) : (
            <Screen style={styles.limitContainer}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  left: 0,
                  width: "50%",
                  flexDirection: "row",
                  justifyContent: "center",
                  top: 20,
                }}
                onPress={close}
              >
                <>
                  <Image
                    source={require("../../assets/png/Iconly-Light-outline-Home.png")}
                    style={styles.ImageIconStyle}
                    resizeMode="contain"
                  />
                </>
              </TouchableOpacity>
              <View style={{ top: 50 }}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  You have reached your maximum try for resend code
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 10,
                    fontFamily: "popRegular",
                    fontSize: 16,
                  }}
                >
                  Please contact
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    top: 20,
                    fontFamily: "popBold",
                    fontSize: 24,
                  }}
                >
                  info@rubiebikes.com
                </Text>
              </View>
            </Screen>
          )}
        </>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
    justifyContent: "center",
  },
  limitContainer: {
    padding: 10,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
  },
  resendEnabled: {
    alignSelf: "center",
    color: colors.blue,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  resendDisabled: {
    alignSelf: "center",
    color: colors.gray,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
    backgroundColor: colors.newWhite,
    borderRadius: 20,
    right: "100%",
  },
  underlineStyleBase: {
    width: 50,
    height: 50,
    backgroundColor: colors.blue,
    borderRadius: 5,
    alignContent: "flex-start",
    borderWidth: 0,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.white,
  },
  underlineStyleHighLighted: {
    backgroundColor: colors.darkGray,
    borderWidth: 0,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
  },
  codeFiledRoot: {
    height: 55,
    marginTop: 30,
    paddingHorizontal: 40,
    justifyContent: "center",
  },
  cell: {
    marginHorizontal: 4,
    height: 55,
    width: 55,
    lineHeight: 55 - 5,
    fontSize: 30,
    textAlign: "center",
    borderRadius: 8,
    color: "#3759b8",
    backgroundColor: "#fff",

    // IOS
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    // Android
    elevation: 3,
  },

  // =======================

  root: {
    minHeight: 800,
    padding: 20,
  },
  title: {
    paddingTop: 50,
    color: "#000",
    fontSize: 25,
    fontWeight: "700",
    textAlign: "center",
    paddingBottom: 40,
  },
  icon: {
    width: 217 / 2.4,
    height: 158 / 2.4,
    marginLeft: "auto",
    marginRight: "auto",
  },
  subTitle: {
    paddingTop: 30,
    color: "#000",
    textAlign: "center",
  },
  nextButton: {
    marginTop: 30,
    height: 60,
    backgroundColor: "#3557b7",
    justifyContent: "center",
    minWidth: 300,
    marginBottom: 100,
  },
  nextButtonText: {
    textAlign: "center",
    fontSize: 20,
    color: "#fff",
    fontWeight: "700",
  },
});

export default CheckEmailActivatedScreen;
