import React, { useState, useEffect } from "react";
import { StyleSheet, Image, View } from "react-native";
import { Calendar } from "react-native-calendars";
import Button from "../components/Button";
import { today } from "xdate";
import _ from "lodash";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AnimatedFlatList, AnimationType } from "flatlist-intro-animations";

import TimeCard from "../components/TimeCard";
import Screen from "../components/Screen";
import colors from "../config/colors";
import bookedTimesApi from "../api/bookedTimes";
import useApi from "../hooks/useApi";
import routes from "../navigation/routes";
import useAuth from "../auth/useAuth";

function CalendarScreen({ route, navigation }) {
  var defaultTime = [
    {
      timeDate: new Date(selectedDay + " 09:00:00"),
      booked: false,
      startTime: "09:00:00",
      label: "09:00 - 10:00",
      key: "1",
    },
    {
      timeDate: new Date(selectedDay + " 10:00:00"),
      booked: false,
      startTime: "10:00:00",
      label: "10:00 - 11:00",
      key: "2",
    },
    {
      timeDate: new Date(selectedDay + " 11:00:00"),
      booked: false,
      startTime: "11:00:00",
      label: "11:00 - 12:00",
      key: "3",
    },
    {
      timeDate: new Date(selectedDay + " 12:00:00"),
      booked: false,
      startTime: "12:00:00",
      label: "12:00 - 13:00",
      key: "4",
    },
    {
      timeDate: new Date(selectedDay + " 13:00:00"),
      booked: false,
      startTime: "13:00:00",
      label: "13:00 - 14:00",
      key: "5",
    },
    {
      timeDate: new Date(selectedDay + " 14:00:00"),
      booked: false,
      startTime: "14:00:00",
      label: "14:00 - 15:00",
      key: "6",
    },
    {
      timeDate: new Date(selectedDay + " 15:00:00"),
      booked: false,
      startTime: "15:00:00",
      label: "15:00 - 16:00",
      key: "7",
    },
    {
      timeDate: new Date(selectedDay + " 16:00:00"),
      booked: false,
      startTime: "16:00:00",
      label: "16:00 - 17:00",
      key: "8",
    },
    {
      timeDate: new Date(selectedDay + " 17:00:00"),
      booked: false,
      startTime: "17:00:00",
      label: "17:00 - 18:00",
      key: "9",
    },
    {
      timeDate: new Date(selectedDay + " 18:00:00"),
      booked: false,
      startTime: "18:00:00",
      label: "18:00 - 19:00",
      key: "10",
    },
    {
      timeDate: new Date(selectedDay + " 19:00:00"),
      booked: false,
      startTime: "19:00:00",
      label: "19:00 - 20:00",
      key: "11",
    },
    {
      timeDate: new Date(selectedDay + " 20:00:00"),
      booked: false,
      startTime: "20:00:00",
      label: "20:00 - 21:00",
      key: "12",
    },
  ];
  const time = [
    {
      timeDate: 9,
      booked: false,
      startTime: "09:00:00",
      label: "09:00 - 10:00",
      key: "1",
    },
    {
      timeDate: 10,
      booked: false,
      startTime: "10:00:00",
      label: "10:00 - 11:00",
      key: "2",
    },
    {
      timeDate: 11,
      booked: false,
      startTime: "11:00:00",
      label: "11:00 - 12:00",
      key: "3",
    },
    {
      timeDate: 12,
      booked: false,
      startTime: "12:00:00",
      label: "12:00 - 13:00",
      key: "4",
    },
    {
      timeDate: 13,
      booked: false,
      startTime: "13:00:00",
      label: "13:00 - 14:00",
      key: "5",
    },
    {
      timeDate: 14,
      booked: false,
      startTime: "14:00:00",
      label: "14:00 - 15:00",
      key: "6",
    },
    {
      timeDate: 15,
      booked: false,
      startTime: "15:00:00",
      label: "15:00 - 16:00",
      key: "7",
    },
    {
      timeDate: 16,
      booked: false,
      startTime: "16:00:00",
      label: "16:00 - 17:00",
      key: "8",
    },
    {
      timeDate: 17,
      booked: false,
      startTime: "17:00:00",
      label: "17:00 - 18:00",
      key: "9",
    },
    {
      timeDate: 18,
      booked: false,
      startTime: "18:00:00",
      label: "18:00 - 19:00",
      key: "10",
    },
    {
      timeDate: 19,
      booked: false,
      startTime: "19:00:00",
      label: "19:00 - 20:00",
      key: "11",
    },
    {
      timeDate: 20,
      booked: false,
      startTime: "20:00:00",
      label: "20:00 - 21:00",
      key: "12",
    },
  ];

  const { booking, setBooking } = useAuth();
  const [timez, setTimez] = useState(); //time);
  const [days, setDays] = useState();
  const [nowHour, setNowHour] = useState(new Date().getHours());
  const [selectedDay, setSelectedDay] = useState(
    new Date().toISOString().slice(0, 10)
  );
  const [selectedTime, setSelectedTime] = useState();
  const getBookedTimesApi = useApi(bookedTimesApi.getBookedTimes);

  useEffect(() => {
    getBookedTimesApi.request();
    if (getBookedTimesApi.data && getBookedTimesApi.data.bookedDays) {
      setDays(getBookedTimesApi.data.bookedDays);
    }
    filterEarlierTime(time, selectedDay);
  }, []);

  const back = () => {
    navigation.navigate(routes.HOME);
  };

  const filterEarlierTime = async (tmz, day) => {
    if (day === new Date().toISOString().slice(0, 10)) {
      let currentTime = new Date();
      let nextAvailableSlot = roundToNextHour(currentTime);
      setTimez(tmz.filter((t) => t.timeDate >= nextAvailableSlot.getHours()));
    }
  };

  function roundToNextHour(date) {
    const p = 60 * 60 * 1000;
    return new Date(Math.round(date.getTime() / p) * p).addHours(2);
  }

  Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + h * 60 * 60 * 1000);
    return this;
  };

  return (
    <Screen style={{ backgroundColor: colors.white }}>
      <View style={{ flex: 0.92 }}>
        <TouchableOpacity onPress={back}>
          <Image
            source={require("../assets/png/ArrowLeftCircle.png")}
            style={styles.ImageIconStyle}
          />
        </TouchableOpacity>
        <Calendar
          minDate={today}
          markedDates={{
            [selectedDay]: {
              selected: true,
              disableTouchEvent: true,
              selectedColor: colors.blue,
            },
          }}
          disableAllTouchEventsForDisabledDays={true}
          theme={{
            calendarBackground: colors.white,
            selectedDayBackgroundColor: colors.blue,
            todayTextColor: colors.blue,
            arrowColor: colors.darkGray,
            textMonthFontFamily: "popMedium",
            textDayFontFamily: "popMedium",
            textDayFontSize: 13,
            textDayHeaderFontSize: 10,
            dayTextColor: colors.darkGray,
          }}
          onDayPress={(day) => {
            setSelectedDay(day.dateString);
            setTimez(defaultTime);
            if (days && days.length > 0) {
              for (let d = 0; d < days.length; d++) {
                if (days[d].bookedDay === day.dateString) {
                  for (let i = 0; i < days[d].bookedHours.length; i++) {
                    for (let j = 0; j < time.length; j++) {
                      if (days[d].bookedHours[i] === time[j].startTime) {
                        time[j].booked = true;
                        setTimez(time);
                      }
                    }
                  }
                }
              }
            }
            filterEarlierTime(
              time,
              new Date(day.dateString).toISOString().slice(0, 10)
            );
          }}
        />
        {nowHour >= 8 && nowHour <= 21 ? (
          <AnimatedFlatList
            data={timez}
            keyExtractor={(time) => time.key}
            animationType={AnimationType.SlideFromBottom}
            animationDuration={1000}
            renderItem={({ item }) => (
              <TimeCard
                label={item.label}
                booked={item.booked}
                selectd={selectedTime}
                onPress={() => {
                  setSelectedTime(item.label);
                  if (booking) {
                    booking.calendar.dateTime = selectedDay + " " + item.label;
                    setBooking(booking);
                  }
                }}
              />
            )}
            extraData={selectedTime}
          />
        ) : (
          <View style={{ flex: 0.8 }}>
            <TimeCard label="No free slots left for today" />
          </View>
        )}
        <View style={{ flex: 0.1 }}>
          <Button
            style={selectedTime === undefined ? styles.notDone : styles.done}
            title="book"
            onPress={() =>
              selectedTime === undefined
                ? alert("please select a time slot")
                : navigation.navigate(routes.PICK_LOCATION)
            }
          />
        </View>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  tagline: {
    fontSize: 25,
    fontWeight: "600",
    paddingVertical: 10,
  },
  done: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 100,
    padding: 20,
    alignSelf: "center",
    fontFamily: "popBold",
    fontSize: 22,
  },
  notDone: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 100,
    padding: 20,
    alignSelf: "center",
    fontFamily: "popBold",
    fontSize: 22,
  },
  ImageIconStyle: {
    left: 15,
    width: 40,
    height: 40,
    top: 0,
  },
});

export default CalendarScreen;
