import React, { useState, useEffect, useRef } from "react";
import {
  AppState,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
  RefreshControl,
} from "react-native";
import * as Notifications from "expo-notifications";
import RiderDrawerToggle from "./drawer/RiderDrawerToggle";
import routes from "../navigation/routes";
import { StatusBar } from "expo-status-bar";

import colors from "../config/colors";
import Screen from "../components/Screen";
import bookingApi from "../api/booking";
import useApi from "../hooks/useApi";
import riderUpTime from "../hooks/riderUpTime";
import upTimeApi from "../api/upTime";
import useNotification from "../hooks/useNotification";
import userRiderLocation from "../hooks/riderLocation";
import RiderDesiredBookings from "./RiderDesiredBookings";
import RiderHomeScreenHeader from "./home/RiderHomeScreenHeader";
import profileApi from "../api/profile";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

function RiderHomeScreen({ navigation }) {
  const [refreshing, setRefreshing] = useState(false);
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const getAcceptedBookings = useApi(bookingApi.getAcceptedBookings);
  const getFinishedBookings = useApi(bookingApi.getFinishedBookings);
  const getNewBookingRequests = useApi(bookingApi.getNewBookingRequests);
  riderUpTime();
  const expoToken = useNotification();
  const riderLocation = userRiderLocation();
  const getDetailsApi = useApi(profileApi.getDetails);

  const bookingDetail = (booking) => {
    const bookingKey = booking.bookingKey;
    const amount = booking.totalAmount;
    const latitude = booking.latitude;
    const longitude = booking.longitude;
    const type = booking.type;
    const bikeType = booking.bikeType;
    const tyreSize = booking.tyreSize;
    const name = booking.name;
    const phone = booking.phone;
    const postcode = booking.postcode;
    const pic = booking.pic;
    const createdDate = booking.createdDate;
    const dueDate = booking.dueDate;
    const services = booking.services;
    const option = booking.option;
    const bikePics = booking.bikePics;
    const params = {
      bookingKey,
      amount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      phone,
      postcode,
      pic,
      createdDate,
      dueDate,
      services,
      option,
      bikePics,
    };
    navigation.navigate(routes.RiderBookingDetail, params);
  };

  const finishedBookingDetail = (booking) => {
    const bookingKey = booking.bookingKey;
    const amount = booking.totalAmount;
    const latitude = booking.latitude;
    const longitude = booking.longitude;
    const type = booking.type;
    const bikeType = booking.bikeType;
    const tyreSize = booking.tyreSize;
    const name = booking.name;
    const pic = booking.pic;
    const createdDate = booking.createdDate;
    const finishedDate = booking.finishedDate;
    const services = booking.services;
    const params = {
      bookingKey,
      amount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      pic,
      createdDate,
      finishedDate,
      services,
    };
    navigation.navigate(routes.RiderFinishedBookingDetail, params);
  };

  const newBookingDetail = (booking) => {
    const bookingKey = booking.bookingKey;
    const amount = booking.totalAmount;
    const latitude = booking.latitude;
    const longitude = booking.longitude;
    const type = booking.type;
    const bikeType = booking.bikeType;
    const tyreSize = booking.tyreSize;
    const name = booking.name;
    const phone = booking.phone;
    const postcode = booking.postcode;
    const pic = booking.pic;
    const createdDate = booking.createdDate;
    const dueDate = booking.dueDate;
    const services = booking.services;
    const params = {
      bookingKey,
      amount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      phone,
      pic,
      createdDate,
      dueDate,
      postcode,
      services,
    };
    navigation.navigate(routes.RiderBookingConfirmation, params);
  };

  const close = () => {
    // navigation.navigate(routes.HOME);
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getAcceptedBookings.request();
    getNewBookingRequests.request();
    getFinishedBookings.request();
    setRefreshing(false);
  }, []);

  useEffect(() => {
    getDetailsApi.request();
    navigation.addListener("focus", () => {
      getAcceptedBookings.request();
      getNewBookingRequests.request();
    });
    const stt = AppState.addEventListener("change", handleAppStateChange);
    upTimeApi.updateRiderState("active");
    getAcceptedBookings.request();
    getNewBookingRequests.request();
    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        if (response.notification.request.content.data.type === "RIDER") {
          const bookingKey =
            response.notification.request.content.data.bookingKey;
          const amount = response.notification.request.content.data.amount;
          const latitude = response.notification.request.content.data.latitude;
          const longitude =
            response.notification.request.content.data.longitude;
          const type = response.notification.request.content.data.type;
          const bikeType = response.notification.request.content.data.bikeType;
          const tyreSize = response.notification.request.content.data.tyreSize;
          const name = response.notification.request.content.data.name;
          const phone = response.notification.request.content.data.phone;
          const postcode = response.notification.request.content.data.postcode;
          const createdDate =
            response.notification.request.content.data.createdDate;
          const dueDate = response.notification.request.content.data.dueDate;
          const pic = response.notification.request.content.data.pic;
          const services = response.notification.request.content.data.services;
          const bookingType =
            response.notification.request.content.data.bookingType;
          const option = response.notification.request.content.data.option;
          const params = {
            bookingKey,
            amount,
            latitude,
            longitude,
            type,
            bikeType,
            tyreSize,
            name,
            phone,
            postcode,
            createdDate,
            dueDate,
            pic,
            bookingType,
            services,
            option,
          };
          if (
            response.notification.request.content.data.notificationType ===
            "book"
          ) {
            navigation.navigate(routes.RiderBookingConfirmation, params);
          } else if (
            response.notification.request.content.data.notificationType ===
            "reassign"
          ) {
            navigation.navigate(routes.RiderReassignConfirmation, params);
          } else if (
            response.notification.request.content.data.notificationType ===
            "cancel"
          ) {
            navigation.navigate(
              routes.RiderBookingCancelledInformation,
              params
            );
          } else if (
            response.notification.request.content.data.notificationType ===
            "notSureOrderApproved"
          ) {
            navigation.navigate(routes.RiderBookingDetail, params);
          }
        }
      });
    return () => {
      if (stt) {
        stt.remove();
      }
    };
  }, []);

  const handleAppStateChange = async (nextAppState) => {
    if (nextAppState === "active" || nextAppState === "inactive") {
      getAcceptedBookings.request();
      getNewBookingRequests.request();
    }
  };

  return (
    <Screen style={styles.screen}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.white,
          flexDirection: "row",
          justifyContent: "flex-start",
          marginBottom: 20,
          height: 60,
        }}
        onPress={close}
        delayPressIn={150}
      >
        <>
          <RiderDrawerToggle
            navigation={navigation}
            style={{ alignSelf: "center" }}
          />
          <RiderHomeScreenHeader data={getDetailsApi.data} />
        </>
      </TouchableOpacity>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={{ height: 60 }}>
          <Text
            style={{
              fontFamily: "popRegular",
              fontSize: 15,
              alignSelf: "center",
            }}
          >
            pull down to refresh{" "}
          </Text>
          <Image
            source={require("../assets/png/Iconly-Light-outline-Location.png")}
            style={{
              resizeMode: "contain",
              width: 23,
              height: 26,
              alignSelf: "center",
              top: 10,
            }}
          />
          <Text
            style={{
              fontFamily: "popSemiBold",
              fontSize: 14,
              alignSelf: "center",
              top: 10,
            }}
          >
            rider home page
          </Text>
        </View>
        <RiderDesiredBookings
          getDesiredBookings={getNewBookingRequests}
          title="New Booking Requests"
          desiredBookingDetail={newBookingDetail}
        />
        <RiderDesiredBookings
          getDesiredBookings={getAcceptedBookings}
          title="Accepted Bookings"
          desiredBookingDetail={bookingDetail}
        />
        <RiderDesiredBookings
          getDesiredBookings={getFinishedBookings}
          title="Finished Bookings"
          desiredBookingDetail={finishedBookingDetail}
        />
      </ScrollView>
      <StatusBar style="dark" />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
});

export default RiderHomeScreen;
