import React from "react";
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { Image } from "react-native-expo-image-cache";

import Text from "./Text";
import colors from "../config/colors";
import bikesApi from "../api/bikes";

function BikeCard({
  refer,
  bikeKey,
  name,
  type,
  tyreSize,
  imageUrl,
  onPress,
  thumbnailUrl,
  refresh,
}) {
  const del = async () => {
    Alert.alert(
      "Delete bike",
      "Are you sure want to delete this bike?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const bikeListReturn = await bikesApi.deleteBike(bikeKey);
            if (bikeListReturn) {
              refresh(); //reload bike list.
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <View
      style={{
        elevation: 15,
        shadowRadius: 5,
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 0.3,
      }}
    >
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.card}>
          <View style={styles.innerCard}>
            <Image
              style={styles.image}
              tint="light"
              preview={{ uri: thumbnailUrl }}
              uri={imageUrl}
              resizeMode="contain"
            />
            {refer && (
              <View style={{ flexDirection: "row", zIndex: 99999 }}>
                <TouchableOpacity
                  onPress={del}
                  style={{
                    left: 200,
                    top: 30,
                    position: "absolute",
                    elevation: 15,
                  }}
                >
                  <ImageBackground
                    tint="light"
                    source={require("../assets/png/Iconly-Light-outline-Delete.png")}
                    resizeMode="contain"
                    style={{
                      width: 30,
                      height: 30,
                      left: 100,
                      backgroundColor: colors.white,
                    }}
                  />
                </TouchableOpacity>
              </View>
            )}

            <View style={styles.detailsContainer}>
              <Text style={styles.title} numberOfLines={1}>
                Name: {name}
              </Text>
              <Text style={styles.title} numberOfLines={1}>
                Tyre Size: {tyreSize} inches
              </Text>
              <Text style={styles.subTitle} numberOfLines={2}>
                Type: {type}
              </Text>
            </View>
          </View>
          <View>
            {!refer && (
              <ImageBackground
                tint="light"
                source={require("../assets/rightarrow.png")}
                resizeMode="contain"
                style={{ width: 60, height: 30 }}
              />
            )}
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    backgroundColor: colors.white,
    marginTop: 15,
    overflow: "hidden",
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
    margin: 10,
  },
  innerCard: {
    flex: 1,
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: "100%",
    height: 200,
    margin: 10,
    alignSelf: "center",
  },
  subTitle: {
    color: colors.secondary,
    fontWeight: "bold",
  },
  title: {
    marginBottom: 7,
    color: colors.darkGray,
  },
});

export default BikeCard;
