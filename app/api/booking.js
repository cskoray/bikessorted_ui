import settings from "../config/settings";
import authStorage from "../auth/storage";
import axios from "axios";
import client from "./client";

const getBookings = () => client.get("/bookings");

const getRiderBookings = () => client.get("/bookings/rider-bookings");

const getAcceptedBookings = () => client.get("/bookings/accepted");

const getFinishedBookings = () => client.get("/bookings/finished");

const getActiveBookings = () => client.get("/bookings/active");

const getNewBookingRequests = () => client.get("/bookings/rider-requested");

const getRequestedBookings = () => client.get("/bookings/requested");

const getLaterActiveBookings = () => client.get("/bookings/later/active");

const getLastNotRatedBooking = () => client.get("/bookings/not-rated");

const getBookingStats = () => client.get("/admin/booking/status");

const isReported = (bookingKey) =>
  client.get(`/bookings/${bookingKey}/if-reported`);

const checkRider = (bookingKey) =>
  client.get(`/bookings/${bookingKey}/booking`);

export const dontAskRatingAgain = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch(`/bookings/${bookingKey}/no-ask`, data);
};

export const rateBooking = (bookingKey, rate, yesNoSelection) => {
  const data = {
    bookingKey: bookingKey,
    rate: rate,
    yesNoSelection: yesNoSelection,
  };
  return client.patch(`/bookings/rate`, data);
};

export const systemCancelBooking = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/remove", data);
};

export const userCancelBooking = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/cancel", data);
};

export const complain = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/complain", data);
};

export const riderBikeBroke = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/rider-bike-broke", data);
};

export const danger = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/rider-danger", data);
};

export const requestNewRider = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/rider/reassign", data);
};

export const userBikeProblem = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/user-bike-problem", data);
};

export const finishBooking = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/finish", data);
};

export const acceptBooking = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/accept", data);
};

export const acceptBookingReassigned = (bookingKey) => {
  const data = { bookingKey: bookingKey };
  return client.patch("/bookings/accept/reassigned", data);
};

export const book = async (booking) => {
  const authToken = await authStorage.getToken();
  if (!authToken) return;
  const res = await axios({
    method: "post",
    url: settings.apiUrl + "/bookings/book",
    headers: { authorisation: authToken },
    data: booking,
  })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error.response.data;
    });
  return res;
};

export const notSureServiceRiderOrder = (bookingKey, serviceToOrder) => {
  const data = { bookingKey: bookingKey, serviceKeys: serviceToOrder };
  return client.post("/bookings/rider/order", data);
};

export const approveNotSureServiceRiderOrder = (paymentMethodId, tipType) => {
  const data = { paymentMethodId: paymentMethodId, tipType: tipType };
  return client.post("/bookings/rider/order/approve", data);
};

export const rejectNotSureServiceRiderOrder = () => {
  return client.post("/bookings/rider/order/reject");
};

const getNotSureOrderedAlready = (bookingKey) =>
  client.get(`/bookings/rider/${bookingKey}/ordered`);

const getNotSureOrderedApproved = (bookingKey) =>
  client.get(`/bookings/rider/${bookingKey}/approved`);

const getRiderOrdered = () => client.get("/bookings/rider/ordered/check");

export default {
  book,
  acceptBooking,
  acceptBookingReassigned,
  getBookings,
  getRiderBookings,
  getAcceptedBookings,
  getFinishedBookings,
  finishBooking,
  getActiveBookings,
  getNewBookingRequests,
  getRequestedBookings,
  getLaterActiveBookings,
  checkRider,
  systemCancelBooking,
  userCancelBooking,
  complain,
  riderBikeBroke,
  danger,
  userBikeProblem,
  isReported,
  getLastNotRatedBooking,
  dontAskRatingAgain,
  rateBooking,
  getBookingStats,
  notSureServiceRiderOrder,
  approveNotSureServiceRiderOrder,
  getNotSureOrderedAlready,
  getRiderOrdered,
  getNotSureOrderedApproved,
  rejectNotSureServiceRiderOrder,
  requestNewRider,
};
