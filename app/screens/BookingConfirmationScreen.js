import React, { useState, useEffect, useRef } from "react";
import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
} from "react-native";
import ActivityIndicatorBooking from "../components/ActivityIndicatorBooking";
import { Avatar } from "react-native-paper";
import routes from "../navigation/routes";
import colors from "../config/colors";
import Screen from "../components/Screen";
import { Rating } from "react-native-ratings";
import CountDown from "react-native-countdown-component";
import bookingApi from "../api/booking";
import useAuth from "../auth/useAuth";

function BookingConfirmationScreen({ route, navigation }) {
  const { booking, setBooking } = useAuth();
  const [name, setName] = useState();
  const [pic, setPic] = useState();
  const [riderRate, setRiderRate] = useState();
  const [dueDate, setDueDate] = useState();
  const [error, setError] = useState("");
  const [bookingKey, setBookingKey] = useState(route.params.bookingKey);
  let interval = useRef();

  const checkForRider = async () => {
    console.log("Rider checking");
    const result = await bookingApi.checkRider(bookingKey);
    if (result.ok && result.data) {
      setName(result.data.riderName);
      setPic(result.data.riderPic);
      setRiderRate(result.data.riderRate);
      setDueDate(result.data.dueDateFormatted);
      clearInterval(interval);
    } else {
      if (
        result.data &&
        result.data.errors &&
        result.data.errors[0].code === "10036"
      ) {
        setError("declined");
      }
      clearInterval(interval);
    }
  };

  useEffect(() => {
    if (name === undefined && error === "") {
      interval = setInterval(() => {
        checkForRider();
      }, 10000);
    }
  }, [interval]);

  const timeUpCancel = async () => {
    const result = await bookingApi.systemCancelBooking(bookingKey);
    alert(
      "Could not found an available rider! Please try again in a few minutes"
    );
    navigation.navigate(routes.HOME);
  };

  const close = () => {
    navigation.navigate(routes.HOME);
  };

  const cancelBooking = async () => {
    Alert.alert(
      "Cancel Booking",
      "Are you sure want to cancel?",
      [
        {
          text: "No",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            const result = await bookingApi.userCancelBooking(bookingKey);
            navigation.navigate(routes.HOME);
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <Screen style={styles.screen}>
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "50%",
            left: 0,
            flexDirection: "row",
            justifyContent: "center",
            marginBottom: 20,
          }}
          onPress={close}
        >
          <>
            <Image
              source={require("../assets/png/Iconly-Light-outline-Home.png")}
              style={styles.ImageIconStyle}
              resizeMode="contain"
            />
          </>
        </TouchableOpacity>
        {!error && (
          <TouchableOpacity
            style={{
              padding: 0,
              margin: 0,
              width: "50%",
              right: "10%",
              flexDirection: "row",
              justifyContent: "center",
              marginBottom: 20,
            }}
            onPress={cancelBooking}
          >
            <Text
              style={{
                color: colors.darkGray,
                fontFamily: "popRegular",
                fontSize: 14,
                alignSelf: "center",
                justifyContent: "flex-end",
                left: "100%",
              }}
            >
              cancel booking
            </Text>
          </TouchableOpacity>
        )}
      </View>
      <View>
        <Image
          source={require("../assets/png/LondonCityOrder.png")}
          style={styles.img}
        />
        {!error ? (
          <Image
            source={require("../assets/png/OrderComplete.png")}
            style={{
              resizeMode: "contain",
              width: 23,
              height: 26,
              alignSelf: "center",
              top: 40,
            }}
          />
        ) : (
          <Image
            source={require("../assets/png/Iconly-Light-outline-Shield Fail.png")}
            style={{
              resizeMode: "contain",
              width: 23,
              height: 26,
              alignSelf: "center",
              top: 40,
              backgroundColor: colors.white,
            }}
          />
        )}
        {!error && name === undefined && (
          <Text
            style={{
              fontFamily: "popSemiBold",
              fontSize: 14,
              alignSelf: "center",
              top: 50,
            }}
          >
            we have received your order
          </Text>
        )}
        {!error && name !== undefined && (
          <Text
            style={{
              fontFamily: "popSemiBold",
              fontSize: 14,
              alignSelf: "center",
              top: 50,
            }}
          >
            {name} is on the way
          </Text>
        )}
        {error ? (
          <>
            <Text
              style={{
                fontFamily: "popSemiBold",
                fontSize: 14,
                alignSelf: "center",
                top: 60,
              }}
            >
              Your payment declined :(
            </Text>
            <Text
              style={{
                fontFamily: "popSemiBold",
                fontSize: 14,
                alignSelf: "center",
                top: 60,
              }}
            >
              Please try another card
            </Text>
          </>
        ) : name === undefined ? (
          <>
            <Text
              style={{
                fontFamily: "popRegular",
                fontSize: 14,
                alignSelf: "center",
                top: 60,
              }}
            >
              please hold tight
            </Text>
            <Text
              style={{
                fontFamily: "popRegular",
                fontSize: 14,
                alignSelf: "center",
                top: 65,
              }}
            >
              finding the nearest technician for you
            </Text>
            <CountDown
              size={15}
              until={480}
              onFinish={timeUpCancel}
              digitStyle={{
                backgroundColor: "#FFF",
                top: 70,
              }}
              timeLabelStyle={{ fontWeight: "bold" }}
              separatorStyle={{ top: 70 }}
              timeToShow={["M", "S"]}
              timeLabels={{ m: null, s: null }}
              digitTxtStyle={{ color: "#fff" }}
            />
            <ActivityIndicatorBooking visible={true} />
          </>
        ) : (
          <>
            <View
              style={{
                flexDirection: "row",
                top: "30%",
                backgroundColor: colors.newWhite,
                borderRadius: 15,
                padding: 10,
                alignSelf: "center",
                width: "100%",
              }}
            >
              <Avatar.Image
                source={{
                  uri: pic,
                }}
                size={75}
              />
              <Text
                style={{
                  left: "25%",
                  fontFamily: "popBold",
                  fontSize: 22,
                }}
              >
                {name}
              </Text>
              <Image
                source={require("../assets/png/Time.png")}
                style={styles.timeStyle}
                resizeMode="center"
              />
              <Rating
                type="custom"
                readonly
                startingValue={riderRate}
                ratingCount={5}
                imageSize={60}
                showRating={false}
                style={{
                  top: "7%",
                  padding: 5,
                  left: "-15%",
                }}
                tintColor="#F8F9FD"
                imageSize={18}
                ratingBackgroundColor="#c8c7c8"
              />
            </View>
            <Text
              style={{
                top: "22%",
                left: "28%",
                fontFamily: "popSemiBold",
                fontSize: 14,
              }}
            >
              arrival {dueDate}
            </Text>
          </>
        )}
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
    backgroundColor: colors.newWhite,
    borderRadius: 20,
    right: "100%",
  },
  timeStyle: {
    margin: 15,
    width: 35,
    height: 35,
    backgroundColor: colors.light,
    borderRadius: 20,
    position: "absolute",
    left: "85%",
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
});

export default BookingConfirmationScreen;
