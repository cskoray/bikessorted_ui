import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import DrawerToggle from "./drawer/DrawerToggle";
import routes from "../navigation/routes";
import Button from "../components/Button";

import colors from "../config/colors";
import Screen from "../components/Screen";

function ShopAdminHomeScreen({ navigation }) {
  const bookingsBtn = () => {
    navigation.navigate(routes.ShopAdmin_Users);
  };

  return (
    <Screen style={styles.screen}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          justifyContent: "flex-start",
          marginBottom: 20,
          height: 60,
        }}
      >
        <>
          <DrawerToggle
            navigation={navigation}
            style={{ alignSelf: "center" }}
          />
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Shop Admin home
          </Text>
        </>
      </TouchableOpacity>
      <View>
        <Button style={styles.done} title="Bookings" onPress={bookingsBtn} />
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
  done: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 60,
    padding: "5%",
    alignSelf: "center",
    width: "70%",
    borderRadius: 5,
  },
});

export default ShopAdminHomeScreen;
