import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";

import Button from "../components/Button";
import routes from "../navigation/routes";
import colors from "../config/colors";
import Screen from "../components/Screen";
import paymentApi from "../api/payment";

function ShopAdminPayoutsScreen({ navigation }) {
  const [payouts, setPayouts] = useState();
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());

  const done = async () => {
    const result = await paymentApi.getPayouts(fromDate, toDate);
    if (result && result.data) {
      setPayouts(result.data);
    }
  };

  const close = () => {
    navigation.navigate(routes.ShopAdminHome);
  };

  const onChangeFrom = (event, selectedDate) => {
    const currentDate = selectedDate || fromDate;
    setFromDate(currentDate);
  };

  const onChangeTo = (event, selectedDate) => {
    const currentDate = selectedDate || fromDate;
    setToDate(currentDate);
  };

  return (
    <Screen style={styles.screen}>
      <ScrollView>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "100%",
            left: 0,
            flexDirection: "row",
            justifyContent: "flex-start",
            height: 60,
          }}
          onPress={close}
        >
          <>
            <Image
              source={require("../assets/png/ArrowCloseCircle.png")}
              style={styles.ImageIconStyle}
            />
            <View></View>
          </>
        </TouchableOpacity>
        <View style={styles.title}>
          <Text
            style={{
              color: colors.white,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
            }}
          >
            Rider Payouts
          </Text>
        </View>
        <Text
          style={{
            fontFamily: "popRegular",
            fontSize: 22,
            alignSelf: "center",
          }}
        >
          From
        </Text>
        <DateTimePicker
          testID="from"
          value={fromDate}
          mode={"date"}
          display="default"
          onChange={onChangeFrom}
          // style={{ left: "35%" }}
        />
        <Text
          style={{
            fontFamily: "popRegular",
            fontSize: 22,
            alignSelf: "center",
          }}
        >
          To
        </Text>
        <DateTimePicker
          testID="to"
          value={toDate}
          mode={"date"}
          display="default"
          onChange={onChangeTo}
          // style={{ left: "35%" }}
          style={{ width: 320, backgroundColor: "white" }}
          textColor={colors.blue}
        />
        <Button style={styles.done} title="Calculate Payouts" onPress={done} />
        {payouts &&
          payouts.payouts.map((payout) => (
            <View style={styles.stats} key={payout.email}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                  }}
                >
                  {payout.name}
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    marginLeft: "auto",
                  }}
                >
                  due: £{payout.amount}
                </Text>
              </View>
              <Text
                style={{
                  fontFamily: "popRegular",
                  fontSize: 22,
                }}
              >
                {payout.email}
              </Text>
              <Text
                style={{
                  fontFamily: "popRegular",
                  fontSize: 22,
                }}
              >
                {payout.phone}
              </Text>
            </View>
          ))}
      </ScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
  done: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 60,
    padding: 20,
    alignSelf: "center",
  },
  title: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
  },
  stats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
});

export default ShopAdminPayoutsScreen;
