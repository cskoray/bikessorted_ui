import React from "react";
import { Text, TouchableOpacity, Image, View, StyleSheet } from "react-native";
import { Avatar } from "react-native-paper";
import colors from "../config/colors";

export default function RiderDesiredBookings(props) {
  return (
    <View style={styles.bookingView}>
      {props.getDesiredBookings.data &&
        props.getDesiredBookings.data.length > 0 && (
          <Text
            style={{
              fontFamily: "popBold",
              fontSize: 19,
              padding: 5,
            }}
          >
            {props.title}
          </Text>
        )}
      {props.getDesiredBookings.data &&
        props.getDesiredBookings.data.length > 0 &&
        props.getDesiredBookings.data.map((booking) => (
          <TouchableOpacity
            style={{
              flexDirection: "row",
              backgroundColor: colors.blue,
              margin: 5,
              padding: 10,
              borderRadius: 5,
            }}
            onPress={() => props.desiredBookingDetail(booking)}
            key={booking.bookingKey}
          >
            <Avatar.Image
              source={{
                uri: booking.pic,
              }}
              size={55}
            />
            <View
              style={{
                alignSelf: "center",
                left: 10,
                width: "100%",
              }}
            >
              <Text
                style={{
                  fontFamily: "popBold",
                  color: colors.white,
                }}
              >
                {booking.dueDate}
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                }}
              >
                <Text
                  style={{
                    fontFamily: "popBold",
                    color: colors.white,
                  }}
                >
                  {booking.name}
                </Text>
                <Image
                  source={require("../assets/png/Iconly-Light-outline-Arrow-Right-2.png")}
                  style={{
                    resizeMode: "contain",
                    width: 23,
                    height: 26,
                    left: 130,
                    alignSelf: "center",
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: "row",
                }}
              >
                {booking.services.map((service) => (
                  <Text
                    key={service}
                    style={{
                      fontFamily: "popRegular",
                      color: colors.white,
                      left: 2,
                    }}
                  >
                    {service}{" "}
                  </Text>
                ))}
              </View>
              <Text
                style={{
                  fontFamily: "popSemiBold",
                  color: colors.white,
                  top: 5,
                }}
              >
                {booking.postcode}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
    </View>
  );
}

const styles = StyleSheet.create({
  bookingView: {
    margin: 10,
    elevation: 15,
    shadowRadius: 10,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.6,
    backgroundColor: colors.white,
    borderRadius: 5,
  },
});
