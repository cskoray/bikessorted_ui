import React from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import routes from "../navigation/routes";
import colors from "../config/colors";
import Screen from "../components/Screen";

function BookingCancelledScreen({ route, navigation }) {
  const close = () => {
    navigation.navigate(routes.HOME);
  };

  return (
    <Screen style={styles.screen}>
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "50%",
            left: 0,
            flexDirection: "row",
            justifyContent: "center",
            marginBottom: 20,
          }}
          onPress={close}
        >
          <>
            <Image
              source={require("../assets/png/Iconly-Light-outline-Home.png")}
              style={styles.ImageIconStyle}
              resizeMode="contain"
            />
          </>
        </TouchableOpacity>
      </View>
      <View>
        <Image
          source={require("../assets/png/LondonCityOrder.png")}
          style={styles.img}
        />
        <Text
          style={{
            fontFamily: "popRegular",
            fontSize: 17,
            alignSelf: "center",
            top: 50,
          }}
        >
          Booking cancelled due to
        </Text>
        <Text
          style={{
            fontFamily: "popSemiBold",
            fontSize: 17,
            alignSelf: "center",
            top: 50,
          }}
        >
          {route.params.cancelReason}
        </Text>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
    backgroundColor: colors.newWhite,
    borderRadius: 20,
    right: "100%",
  },
  timeStyle: {
    margin: 15,
    width: 35,
    height: 35,
    backgroundColor: colors.light,
    borderRadius: 20,
    position: "absolute",
    left: "85%",
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
});

export default BookingCancelledScreen;
