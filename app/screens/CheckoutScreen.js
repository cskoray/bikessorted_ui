import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
  Switch,
} from "react-native";
import { useConfirmSetupIntent } from "@stripe/stripe-react-native";
import { add, format, divide, subtract } from "mathjs";
import Modal from "react-native-modal";
import LottieView from "lottie-react-native";

import colors from "../config/colors";
import Screen from "../components/Screen";
import useAuth from "../auth/useAuth";
import bookingApi from "../api/booking";
import paymentApi from "../api/payment";
import servicesApi from "../api/services";
import routes from "../navigation/routes";
import { useNavigation } from "@react-navigation/native";
import ActivityIndicator from "../components/ActivityIndicator";
import authStorage from "../auth/storage";
import SwitchPaymentMethod from "./SwitchPaymentMethod";
import { fontPixel } from "./NormaliseRatios";

function CheckoutScreen() {
  const [confirmButton, setConfirmButton] = useState(false);
  const { booking, setBooking } = useAuth();
  const [total, setTotal] = useState(0);
  const [originalTotal, setOriginalTotal] = useState(0);
  const [tip, setTip] = useState(0);
  const [tipType, setTipType] = useState("Z");
  const [paymentMethodId, setPaymentMethodId] = useState("-");
  const [methods, setMethods] = useState();
  const [last4, setLast4] = useState();
  const [brand, setBrand] = useState();
  const [optionalServices, setOptionalServices] = useState([]);
  const [optionalServicesModal, setOptionalServicesModal] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const { confirmSetupIntent, loading } = useConfirmSetupIntent();
  const navigation = useNavigation();
  const [saveLoading, setSaveLoading] = useState(false);
  const [pointsUsed, setPointsUsed] = useState(false);
  const [pointAmount, setPointAmount] = useState(false);
  const [serviceFee, setServiceFee] = useState(0);
  const [optionalServicesTotal, setOptionalServicesTotal] = useState(0);
  const [serviceFeeApplied, setServiceFeeApplied] = useState("n");
  const [toggledOptionalServices, setToggledOptionalServices] = useState([]);
  const [toggledOptionalServiceKeys, setToggledOptionalServiceKeys] = useState(
    []
  );

  const getOptionalServicesTotal = () => {
    let t = 0;
    if (toggledOptionalServices && toggledOptionalServices.length > 0) {
      toggledOptionalServices.map((s) => {
        t = format(add(t, s.price), {
          precision: 6,
        });
      });
    }
    setOptionalServicesTotal(t);
    return t;
  };

  const confirmOptionalServices = () => {
    setOptionalServicesModal(false);
    const tot = getOptionalServicesTotal();
    setTotal(
      format(add(add(originalTotal, tot), tip), {
        precision: 6,
      })
    );
  };

  const see = async () => {
    getOptionals();
    setOptionalServicesModal(true);
  };

  const ifOptionalServiceSelected = (optionalServiceKey) => {
    let found = false;
    if (toggledOptionalServices && toggledOptionalServices.length > 0) {
      toggledOptionalServices.map((s) => {
        if (s.optionalServiceKey === optionalServiceKey) {
          found = true;
        }
      });
    }
    return found;
  };

  const toggleSwitch = (optionalService) => {
    if (!ifOptionalServiceSelected(optionalService.optionalServiceKey)) {
      setToggledOptionalServices((prevItems) => [
        ...prevItems,
        optionalService,
      ]);
      setToggledOptionalServiceKeys((prevItems) => [
        ...prevItems,
        optionalService.optionalServiceKey,
      ]);
    } else {
      let c = toggledOptionalServices.filter(
        (s) => s.optionalServiceKey !== optionalService.optionalServiceKey
      );
      if (c) {
        setToggledOptionalServices(c);
        setToggledOptionalServiceKeys(c.optionalServiceKey);
      }
    }
  };

  // const modalViewTimeout = () => {
  //   setTimeout(function () {
  //     if (optionalServices) {
  //       setOptionalServicesModal(true);
  //     }
  //   }, 2000);
  // };

  const formatDate = (ds) => {
    var first = ds.split(" ")[0],
      y = first.split("-")[0],
      mo = first.split("-")[1] - 1,
      d = first.split("-")[2],
      second = ds.split(" ")[1],
      h = second.split(":")[0],
      m = second.split(":")[1];

    return new Date(y, mo, d, h, m);
  };

  const validateServiceFee = () => {
    let fee = 0;
    if (booking.service.type === "INSTANT") {
      let serviceFeeTime = new Date();
      serviceFeeTime.setHours(17, 30);
      const now = new Date();
      if (now > serviceFeeTime) {
        fee = 1.99;
        setServiceFee(fee);
        setServiceFeeApplied("y");
      }
    } else {
      if (booking.calendar.dateTime.length > 1) {
        let bookingTimeParsed = booking.calendar.dateTime.substring(
          0,
          booking.calendar.dateTime.length - 8
        );
        let formatted = formatDate(bookingTimeParsed);
        const bookingTime = new Date(formatted);
        let serviceFeeTime = new Date(formatted);
        serviceFeeTime.setHours(17, 0);
        if (bookingTime >= serviceFeeTime) {
          fee = 1.99;
          setServiceFee(fee);
          setServiceFeeApplied("y");
        }
      }
    }
    return fee;
  };

  const closeOptionalServicesModal = () => {
    setOptionalServicesModal(false);
  };

  const getOptionals = async () => {
    if (booking.service.type === "INSTANT") {
      const optionalServicesResult = await servicesApi.getOptionalServices([
        booking.service.serviceKeys,
      ]);
      if (optionalServicesResult && optionalServicesResult.data) {
        setOptionalServices(optionalServicesResult.data);
        setToggledOptionalServiceKeys([]);
      }
    } else {
      const optionalServicesResult = await servicesApi.getOptionalServices(
        booking.service.serviceKeys
      );
      if (optionalServicesResult && optionalServicesResult.data) {
        setOptionalServices(optionalServicesResult.data);
        setToggledOptionalServiceKeys([]);
      }
    }
  };

  useEffect(() => {
    const getMethods = async () => {
      setSaveLoading(true);
      getOptionals();
      const result = await paymentApi.getPaymentMethods();
      setSaveLoading(false);
      if (result.data && result.data.paymentMethods.length > 0) {
        setMethods(result.data);
        setLast4(result.data.paymentMethods[0].last4);
        setBrand(result.data.paymentMethods[0].brand);
        setPaymentMethodId(result.data.paymentMethods[0].id);
      }
    };
    const setToken = async () => {
      const authToken = await authStorage.getToken();
      if (!authToken) return;
      booking.token = authToken;
      setBooking(booking);
    };

    let t = validateServiceFee();
    if (booking.service.type === "LATER") {
      booking.service.services.map(
        (service) =>
          (t = format(add(t, service.price), {
            precision: 6,
          }))
      );
    } else {
      t = booking.service.services.price;
    }
    getMethods();
    setTotal(t);
    setOriginalTotal(t);
    setToken();
  }, []);

  const change = () => {
    setModalVisible(true);
  };

  const addTip = (val, tipType) => {
    if (tip === val) {
      setTip(0);
      setTotal(
        format(add(originalTotal, optionalServicesTotal), {
          precision: 6,
        })
      );
      setTipType("Z");
    } else {
      setTip(val);
      setTotal(
        format(add(add(originalTotal, val), optionalServicesTotal), {
          precision: 6,
        })
      );
      setTipType(tipType);
    }
  };

  const confirm = async () => {
    setConfirmButton(true);
    let bookin = {
      type: booking.service.type,
      bikeKey: booking.bike.bikeKey,
      cardKey: paymentMethodId,
      latitude: booking.location.latitude,
      longitude: booking.location.longitude,
      postcode: booking.postcode,
      dateTime: booking.calendar.dateTime,
      tipType: tipType,
      serviceFeeApplied: serviceFeeApplied,
      pointsUsed: booking.points.use,
    };
    if (paymentMethodId === "-") {
      bookin.cardKey = methods.paymentMethods[0].id;
    } else {
      bookin.cardKey = paymentMethodId;
    }
    if (booking.service.type === "LATER") {
      bookin.serviceKeys = booking.service.serviceKeys;
    } else {
      let keys = [booking.service.serviceKeys];
      bookin.serviceKeys = keys;
    }
    bookin.optionalServiceKeys = toggledOptionalServiceKeys;
    const result = await bookingApi.book(bookin);
    if (result) {
      if (result.status === 200) {
        let params = {
          name: "-",
          pic: "-",
          bookingKey: result.data.bookingKey,
        };
        navigation.navigate(routes.BookingConfirmation, params);
      } else {
        if (result.errors[0].code === "10020") {
          alert(
            "Sorry, we are currently not covering your location but we are working hard to do so"
          );
        } else if (result.errors[0].code === "10009") {
          alert("Sorry! Something went wrong. Please try again in a moment");
          setConfirmButton(false);
        }
      }
    } else {
      alert("Sorry, something went wrong. Please try again in a moment");
      setConfirmButton(false);
    }
  };

  const close = () => {
    if (booking.service.type === "INSTANT") {
      navigation.navigate(routes.PICK_LOCATION);
    }
    if (booking.service.type === "LATER") {
      navigation.navigate(routes.BIKES);
    }
  };

  const closeModel = () => {
    setModalVisible(false);
  };

  const pickCard = (method) => {
    setPaymentMethodId(method.id);
    setLast4(method.last4);
    setModalVisible(false);
  };

  const savePaymentMethod = async () => {
    setSaveLoading(true);
    const result = await paymentApi.setupIntent();
    setSaveLoading(false);
    const clientSecret = result.data;
    const { setupIntent, error } = await confirmSetupIntent(clientSecret, {
      type: "Card",
    });
    if (error) {
      console.log(error);
    }
    if (setupIntent && setupIntent.status === "Succeeded") {
      setSaveLoading(true);
      const result = await paymentApi.getPaymentMethods();
      setSaveLoading(false);
      if (result.data) {
        setMethods(result.data);
        const newMethod = result.data.paymentMethods.filter(
          (m) => m.id !== result.data.paymentMethodId
        );
        setLast4(newMethod.last4);
        setBrand(newMethod.brand);
        setPaymentMethodId(newMethod.id);
        setModalVisible(false);
      }
    } else {
      alert("something went wrong");
      setModalVisible(false);
    }
  };

  const usePoints = () => {
    let p = divide(booking.points.amount, 50);
    if (!pointsUsed) {
      booking.points.use = "y";
      setBooking(booking);
      setTotal(
        format(subtract(total, p), {
          precision: 6,
        })
      );
    } else {
      setTotal(
        format(add(total, p), {
          precision: 6,
        })
      );
    }
    setPointsUsed(!pointsUsed);
    setPointAmount(p);
  };

  return (
    <>
      <ActivityIndicator visible={saveLoading} />
      <Screen style={styles.screen}>
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={{
              padding: 0,
              margin: 0,
              width: "50%",
              left: 0,
              flexDirection: "row",
              alignContent: "center",
            }}
            onPress={close}
          >
            <>
              <Image
                source={require("../assets/png/ArrowLeftCircle.png")}
                style={styles.ImageIconStyle}
              />
              <Text
                style={{
                  color: colors.darkGray,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                  left: 20,
                }}
              >
                Checkout
              </Text>
            </>
          </TouchableOpacity>
          {booking.points.amount > 0 && (
            <TouchableOpacity
              style={pointsUsed ? styles.used : styles.notUsed}
              onPress={usePoints}
            >
              <Text
                style={{
                  color: colors.white,
                  alignSelf: "center",
                  paddingHorizontal: 5,
                  fontFamily: "popRegular",
                  fontSize: 16,
                }}
              >
                USE {booking.points.amount} POINTS
              </Text>
            </TouchableOpacity>
          )}
        </View>
        <Modal
          animationIn="zoomInUp"
          isVisible={optionalServicesModal}
          hasBackdrop={true}
        >
          <View
            style={{
              flex: 0.5,
              backgroundColor: colors.newWhite,
              elevation: 15,
              shadowRadius: 10,
              shadowOffset: {
                width: 2,
                height: 2,
              },
              shadowOpacity: 0.2,
              borderRadius: 10,
            }}
          >
            <View
              style={{
                flex: 0.15,
                backgroundColor: colors.test,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
              }}
            >
              <TouchableOpacity
                style={{ marginVertical: 5, flexDirection: "row" }}
                onPress={closeOptionalServicesModal}
              >
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    left: "50%",
                    color: colors.darkGray,
                  }}
                >
                  Special offers for you
                </Text>
                <Image
                  source={require("../assets/png/ArrowCloseCircle.png")}
                  style={{
                    width: 30,
                    height: 30,
                    alignSelf: "center",
                    marginLeft: "auto",
                    right: 5,
                  }}
                />
              </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 0.75 }}>
              {optionalServices &&
                optionalServices.length > 0 &&
                optionalServices.map((optionalService) => (
                  <View
                    style={{
                      flexDirection: "row",
                      backgroundColor: colors.white,
                      marginTop: 15,
                    }}
                    key={optionalService.optionalServiceKey}
                  >
                    <View style={{ alignSelf: "center" }}>
                      <Text
                        style={{
                          fontFamily: "popSemiBold",
                          fontSize: 22,
                          left: 5,
                          color: colors.darkGray,
                        }}
                      >
                        {optionalService.name}
                      </Text>
                      <Text
                        style={{
                          fontFamily: "popRegular",
                          fontSize: 20,
                          left: 5,
                          color: colors.lightTxt,
                        }}
                      >
                        {optionalService.description}
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: "popRegular",
                            fontSize: 22,
                            left: 5,
                            color: colors.lightTxt,
                            textDecorationLine: "line-through",
                          }}
                        >
                          £{optionalService.oldPrice}
                        </Text>
                        <Text
                          style={{
                            fontFamily: "popSemiBold",
                            fontSize: 20,
                            left: 15,
                            color: colors.blue,
                          }}
                        >
                          £{optionalService.price}
                        </Text>
                      </View>
                    </View>
                    <Switch
                      trackColor={{ false: colors.white, true: colors.blue }}
                      onChange={() => toggleSwitch(optionalService)}
                      value={ifOptionalServiceSelected(
                        optionalService.optionalServiceKey
                      )}
                      style={{
                        marginLeft: "auto",
                        alignSelf: "center",
                        right: 5,
                      }}
                    />
                  </View>
                ))}
            </ScrollView>
            <TouchableOpacity
              style={{
                backgroundColor: colors.blue,
                justifyContent: "center",
                flex: 0.2,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
              }}
              onPress={confirmOptionalServices}
            >
              <Text
                style={{
                  color: colors.newWhite,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                }}
              >
                Confirm
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <View
          style={{
            // flex: 2.5,
            backgroundColor: colors.white,
            margin: 10,
            elevation: 15,
            shadowRadius: 5,
            shadowOffset: {
              width: 5,
              height: 10,
            },
            shadowOpacity: 0.2,
            borderRadius: 10,
            height: "auto",
          }}
        >
          <View
            style={{
              backgroundColor: colors.test,
              width: "100%",
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              justifyContent: "center",
              height: "auto",
            }}
          >
            {booking.service.type === "LATER" ? (
              <Text
                style={{
                  fontFamily: "popRegular",
                  fontSize: fontPixel(20),
                  alignSelf: "center",
                  color: colors.darkGray,
                }}
              >
                Due to {booking.calendar.dateTime}
              </Text>
            ) : (
              <Text
                style={{
                  fontFamily: "popRegular",
                  fontSize: 18,
                  alignSelf: "center",
                  color: colors.darkGray,
                }}
              >
                Due in 30 min
              </Text>
            )}
          </View>
          <View
            style={{
              backgroundColor: colors.white,
              borderRadius: 10,
              height: "auto",
              paddingLeft: 10,
              paddingBottom: 15,
              paddingTop: 0,
              minHeight: "40%",
            }}
          >
            <Text style={{ fontFamily: "popSemiBold", fontSize: 20 }}>
              Booked Services
            </Text>
            <View style={{ alignItems: "flex-start" }}>
              {booking.service.type === "LATER" ? (
                booking.service.services.map((service) => (
                  <View
                    style={{
                      flexDirection: "row",
                      width: "100%",
                      paddingLeft: 5,
                    }}
                    key={service.name}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        fontFamily: "popSemiBold",
                        width: "80%",
                        color: colors.lightTxt,
                      }}
                    >
                      {service.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: 20,
                        fontFamily: "popSemiBold",
                        marginLeft: "auto",
                        width: "20%",
                        color: colors.lightTxt,
                      }}
                    >
                      £{service.price}
                    </Text>
                  </View>
                ))
              ) : (
                <View
                  style={{
                    flexDirection: "row",
                    width: "100%",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: "popSemiBold",
                      width: "80%",
                      color: colors.lightTxt,
                    }}
                  >
                    {booking.service.services.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: "popSemiBold",
                      marginLeft: "auto",
                      width: "20%",
                      color: colors.lightTxt,
                    }}
                  >
                    £{booking.service.services.price}
                  </Text>
                </View>
              )}
            </View>
            {toggledOptionalServices.length > 0 && (
              <View
                style={{
                  top: 10,
                  borderTopWidth: 1,
                  borderColor: colors.test,
                }}
              >
                <Text style={{ fontFamily: "popSemiBold", fontSize: 16 }}>
                  Extra Services
                </Text>
                <View style={{ alignItems: "flex-start" }}>
                  {toggledOptionalServices.map((service) => (
                    <View
                      style={{
                        flexDirection: "row",
                        width: "100%",
                      }}
                      key={service.name}
                    >
                      <Text
                        style={{
                          fontSize: 16,
                          fontFamily: "popSemiBold",
                          width: "80%",
                          color: colors.lightTxt,
                          paddingLeft: 5,
                        }}
                      >
                        {service.name}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          fontFamily: "popSemiBold",
                          marginLeft: "auto",
                          width: "20%",
                          color: colors.lightTxt,
                        }}
                      >
                        £{service.price}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
            )}
            {tipType !== "Z" && (
              <View
                style={{
                  top: 20,
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-start",
                  borderTopWidth: 1,
                  borderColor: colors.test,
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "popSemiBold",
                    marginLeft: "auto",
                    width: "80%",
                    color: colors.lightTxt,
                  }}
                >
                  Tip
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "popSemiBold",
                    marginLeft: "auto",
                    width: "20%",
                    color: colors.lightTxt,
                  }}
                >
                  £{tip}
                </Text>
              </View>
            )}
            {serviceFee > 0 && (
              <View
                style={{
                  top: 20,
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-start",
                  borderTopWidth: 1,
                  borderColor: colors.test,
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "popRegular",
                    marginLeft: "auto",
                    width: "80%",
                    color: colors.lightTxt,
                  }}
                >
                  service fee
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "popRegular",
                    marginLeft: "auto",
                    width: "20%",
                    color: colors.lightTxt,
                  }}
                >
                  £1.99
                </Text>
              </View>
            )}
            {pointsUsed && (
              <View
                style={{
                  top: 20,
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-start",
                  borderTopWidth: 1,
                  borderColor: colors.test,
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "popRegular",
                    marginLeft: "auto",
                    width: "80%",
                    color: colors.lightTxt,
                  }}
                >
                  Rubie Points used
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: "popRegular",
                    marginLeft: "auto",
                    width: "20%",
                    color: colors.lightTxt,
                  }}
                >
                  £{pointAmount}
                </Text>
              </View>
            )}
            <View
              style={{
                top: 20,
                flexDirection: "row",
                width: "100%",
                borderTopWidth: 2,
                borderColor: colors.test,
              }}
            >
              <Text
                style={{
                  fontSize: 24,
                  fontFamily: "popSemiBold",
                  color: colors.lightTxt,
                }}
              >
                Total
              </Text>
              <Text
                style={{
                  marginLeft: "auto",
                  fontSize: 24,
                  fontFamily: "popSemiBold",
                  color: colors.lightTxt,
                  right: 5,
                }}
              >
                £{total}
              </Text>
            </View>
            <View
              style={{
                top: 20,
                width: "100%",
                paddingBottom: 10,
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "popRegular",
                }}
              >
                Tip your service technician
              </Text>
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  style={tip === 1 ? styles.tipSelected : styles.tip}
                  onPress={() => addTip(1, "A")}
                >
                  <Text
                    style={tip === 1 ? styles.tipTxtSelected : styles.tipTxt}
                  >
                    £1
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={tip === 2 ? styles.tipSelected : styles.tip}
                  onPress={() => addTip(2, "B")}
                >
                  <Text
                    style={tip === 2 ? styles.tipTxtSelected : styles.tipTxt}
                  >
                    £2
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={tip === 5 ? styles.tipSelected : styles.tip}
                  onPress={() => addTip(5, "C")}
                >
                  <Text
                    style={tip === 5 ? styles.tipTxtSelected : styles.tipTxt}
                  >
                    £5
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {optionalServices && optionalServices.length > 0 && (
            <TouchableOpacity
              style={{
                backgroundColor: colors.test,
                // flex: 0.2,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
                width: "100%",
                flexDirection: "row",
                height: 40,
              }}
              onPress={see}
            >
              <View style={{ height: "100%", width: "20%" }}>
                <LottieView
                  autoPlay
                  loop
                  source={require("../assets/animations/offer.json")}
                  style={{ height: "100%", alignSelf: "flex-end" }}
                />
              </View>
              <Text
                style={{
                  color: colors.darkGray,
                  fontFamily: "popSemiBold",
                  fontSize: 18,
                  alignSelf: "center",
                  marginLeft: "auto",
                  width: "60%",
                }}
              >
                CHECK SPECIAL OFFERS
              </Text>
              <View
                style={{
                  height: "100%",
                  width: "10%",
                  marginLeft: "auto",
                  right: "15%",
                }}
              >
                <LottieView
                  autoPlay
                  loop
                  source={require("../assets/animations/offer.json")}
                  style={{ height: "100%", alignSelf: "flex-end" }}
                />
              </View>
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            backgroundColor: colors.white,
            margin: 10,
            elevation: 15,
            shadowRadius: 5,
            shadowOffset: {
              width: 5,
              height: 10,
            },
            shadowOpacity: 0.2,
            borderRadius: 10,
            height: "auto",
            padding: 10,
            top: 20,
          }}
        >
          <View style={{ flexDirection: "row", height: "auto" }}>
            <Text
              style={{
                fontFamily: "popSemiBold",
                fontSize: 16,
                alignSelf: "center",
              }}
            >
              Payment Methods
            </Text>
            <TouchableOpacity
              onPress={change}
              style={{
                backgroundColor: colors.white,
                borderRadius: 10,
                marginLeft: "auto",
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "popSemiBold",
                  color: colors.blue,
                  alignSelf: "center",
                }}
              >
                change
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              left: 25,
              width: "90%",
              height: "auto",
            }}
          >
            <TouchableOpacity delayPressIn={150}>
              <View style={styles.card}>
                {brand === "visa" ? (
                  <Image
                    style={styles.image}
                    tint="light"
                    source={require("../assets/png/Visa.png")}
                    resizeMode="contain"
                  />
                ) : (
                  <Image
                    style={styles.image}
                    tint="light"
                    source={require("../assets/png/master.png")}
                    resizeMode="contain"
                  />
                )}
                <View style={styles.detailsContainer}>
                  <Text style={styles.title} numberOfLines={1}>
                    **** **** **** {last4}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          onPress={confirm}
          style={{ justifyContent: "center", flex: 0.5, top: 30 }}
          disabled={confirmButton}
        >
          <View style={confirmButton ? styles.viewGray : styles.viewBlue}>
            <Text
              style={confirmButton ? styles.confirmTxtGray : styles.confirmTxt}
            >
              Book Rubie
            </Text>
          </View>
        </TouchableOpacity>
        <SwitchPaymentMethod
          methods={methods}
          modalVisible={modalVisible}
          loading={loading}
          closeModel={closeModel}
          pickCard={pickCard}
          savePaymentMethod={savePaymentMethod}
        />
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
  },
  notUsed: {
    backgroundColor: colors.blue,
    marginLeft: "auto",
    justifyContent: "center",
    width: "35%",
    right: 10,
    borderRadius: 5,
  },
  used: {
    backgroundColor: colors.test,
    marginLeft: "auto",
    justifyContent: "center",
    width: "35%",
    right: 10,
    borderRadius: 5,
  },
  ImageIconStyle: {
    width: 40,
    height: 40,
    top: 0,
    left: 15,
  },
  viewBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "center",
    flexDirection: "row",
  },
  viewGray: {
    backgroundColor: colors.light,
    borderRadius: 0,
    height: 76,
    margin: 0,
    justifyContent: "center",
    flexDirection: "row",
  },
  confirmTxt: {
    alignSelf: "center",
    color: colors.white,
    fontFamily: "popBold",
    fontSize: 20,
  },
  confirmTxtGray: {
    alignSelf: "center",
    color: colors.darkGray,
    fontFamily: "popBold",
    fontSize: 20,
  },
  card: {
    // overflow: "hidden",
    flexDirection: "row",
  },
  detailsContainer: {
    padding: 20,
    marginLeft: "auto",
    width: "85%",
  },
  image: {
    width: "20%",
    height: 40,
    alignSelf: "center",
    marginLeft: "auto",
  },
  tip: {
    width: 50,
    height: 50,
    backgroundColor: colors.newWhite,
    justifyContent: "center",
    margin: 5,
    borderRadius: 5,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.2,
  },
  tipSelected: {
    width: 50,
    height: 50,
    backgroundColor: colors.darkGray,
    justifyContent: "center",
    margin: 5,
    borderRadius: 5,
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.4,
  },
  tipTxt: {
    fontSize: 20,
    fontFamily: "popSemiBold",
    alignSelf: "center",
    color: colors.black,
  },
  tipTxtSelected: {
    fontSize: 20,
    fontFamily: "popSemiBold",
    alignSelf: "center",
    color: colors.white,
  },
  title: {
    alignSelf: "center",
    fontFamily: "popSemiBold",
    fontSize: 18,
  },
});

export default CheckoutScreen;
