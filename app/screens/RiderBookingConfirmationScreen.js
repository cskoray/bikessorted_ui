import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  Platform,
} from "react-native";
import MapView from "react-native-maps";
import { Avatar } from "react-native-paper";
import Constants from "expo-constants";
import * as Linking from "expo-linking";
import useLocation from "../hooks/useLocation";
import { round } from "mathjs";

import colors from "../config/colors";
import bookingApi from "../api/booking";
import routes from "../navigation/routes";

function RiderBookingConfirmationScreen({ route, navigation }) {
  const [rider, setRider] = useState();
  const [actionTaken, setActionTaken] = useState(false);
  const location = useLocation();
  const [latestLocation, setLatestLocation] = useState(location);
  const {
    bookingKey,
    amount,
    latitude,
    longitude,
    type,
    bikeType,
    tyreSize,
    name,
    phone,
    postcode,
    pic,
    createdDate,
    dueDate,
    bookingType,
    services,
  } = route.params;

  useEffect(() => {
    if (location) {
      setLatestLocation(location);
    }
  }, [location]);

  const accept = async () => {
    setActionTaken(true);
    const result = await bookingApi.acceptBooking(bookingKey);
    navigation.navigate(routes.RiderHome);
  };

  const close = () => {
    navigation.navigate(routes.RiderHome);
  };

  const switchToMapsApp = () => {
    Linking.openURL(
      Platform.OS === "ios"
        ? `googleMaps://app?saddr=${latestLocation.latitude}+${latestLocation.longitude}&daddr=${latitude}+${longitude}`
        : `google.navigation:q=${latitude}+${longitude}`
    );
  };

  return (
    <>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          latitudeDelta: 0.00222,
          longitudeDelta: 0.00421,
        }}
        mapType="mutedStandard"
        showsMyLocationButton
        zoomControlEnabled
        zoomEnabled
        provider={"google"}
        paddingAdjustmentBehavior="automatic"
        onPress={switchToMapsApp}
      >
        <MapView.Marker
          coordinate={{
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
          }}
        >
          <Image
            source={require("../assets/png/RubMap.png")}
            style={{
              height: 80,
            }}
            resizeMode="contain"
          />
        </MapView.Marker>
      </MapView>
      <TouchableOpacity
        style={{
          top: Constants.statusBarHeight,
          width: "100%",
          left: 0,
          flexDirection: "row",
          justifyContent: "flex-start",
          marginBottom: 20,
          height: 60,
          position: "absolute",
          zIndex: 9999,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
        </>
      </TouchableOpacity>
      <View
        style={{
          position: "absolute",
          backgroundColor: colors.darkGray,
          borderRadius: 15,
          top: "52%",
          width: "98%",
          height: "57%",
          alignSelf: "center",
          padding: 10,
        }}
      >
        <View style={{ flexDirection: "row", padding: 5 }}>
          <Avatar.Image
            source={{
              uri: pic,
            }}
            size={55}
          />
          <Text
            style={{
              fontSize: 20,
              fontFamily: "popSemiBold",
              color: colors.white,
              left: 15,
              alignSelf: "center",
            }}
          >
            {name}/{postcode} - {phone}
          </Text>
        </View>
        <View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Earning</Text>
            <Text style={styles.txt}>£{round(amount)}</Text>
            <Text style={styles.txt2}>on this ride</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Due to</Text>
            <Text style={styles.txt}>{dueDate}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Service wanted</Text>
            {services.map((service) => (
              <Text key={service} style={styles.txt}>
                {service}{" "}
              </Text>
            ))}
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Bicycle type</Text>
            <Text style={styles.txt}>{bikeType}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.txt2}>Tyre size</Text>
            <Text style={styles.txt}>{tyreSize} inch</Text>
          </View>
          {!actionTaken && (
            <View style={{ flex: 1, flexDirection: "row" }}>
              <TouchableOpacity style={styles.btnAccept} onPress={accept}>
                <Text style={styles.btntxt}>ACCEPT BOOKING</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 10,
    width: 60,
    height: 60,
    top: 0,
  },
  txt: {
    fontFamily: "popSemiBold",
    fontSize: 18,
    left: 10,
    padding: 5,
    color: colors.white,
  },
  txt2: {
    fontFamily: "popRegular",
    fontSize: 16,
    left: 10,
    padding: 5,
    color: colors.white,
    alignSelf: "center",
  },
  btnAccept: {
    flex: 1,
    backgroundColor: colors.blue,
    justifyContent: "center",
    height: 70,
    top: 15,
    borderRadius: 5,
  },
  btntxt: {
    color: colors.white,
    alignSelf: "center",
    fontFamily: "popSemiBold",
    fontSize: 24,
  },
});

export default RiderBookingConfirmationScreen;
