import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";

import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import stocksApi from "../api/stocks";

function RiderStocksScreen({ navigation }) {
  const [riderCurrentStocks, setRiderCurrentStocks] = useState();
  const [riderStockHistory, setRiderStockHistory] = useState();
  const [riderInventoryHistory, setRiderInventoryHistory] = useState();

  const close = () => {
    navigation.navigate(routes.RiderHome);
  };

  const order = async () => {
    navigation.navigate(routes.RiderOrderStocks);
  };

  useEffect(() => {
    const getRidersStocksInventory = async () => {
      const res = await stocksApi.getRidersCurrentStocks();
      if (res && res.data) {
        setRiderCurrentStocks(res.data);
      }
      const result = await stocksApi.getRidersStocksInventoryBookingHistory();
      if (result && result.data) {
        setRiderStockHistory(result.data.stocks);
        setRiderInventoryHistory(result.data.inventory);
      }
    };
    getRidersStocksInventory();
  }, []);

  return (
    <Screen style={styles.screen}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          flexDirection: "row",
          alignContent: "center",
          marginBottom: 20,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Stock Management
          </Text>
        </>
      </TouchableOpacity>
      <ScrollView>
        <TouchableOpacity
          style={{
            padding: 20,
            margin: 20,
            width: "90%",
            backgroundColor: colors.blue,
            justifyContent: "center",
            elevation: 15,
            shadowRadius: 5,
            shadowOffset: {
              height: 5,
            },
            shadowOpacity: 0.8,
            borderRadius: 10,
          }}
          onPress={order}
        >
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
            }}
          >
            Order New Stock
          </Text>
        </TouchableOpacity>
        {riderCurrentStocks && riderCurrentStocks.length > 0 && (
          <View style={styles.title}>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popSemiBold",
                fontSize: 22,
                alignSelf: "center",
              }}
            >
              Current Stocks
            </Text>
          </View>
        )}
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            top: 20,
            justifyContent: "center",
          }}
        >
          {riderCurrentStocks &&
            riderCurrentStocks.map((stock) => (
              <View style={styles.currentStats}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 19,
                  }}
                >
                  {stock.stockItemType}
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 19,
                  }}
                >
                  {stock.quantity} piece
                </Text>
              </View>
            ))}
        </View>
        {riderStockHistory && riderStockHistory.length > 0 && (
          <View style={styles.title}>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popSemiBold",
                fontSize: 22,
                alignSelf: "center",
              }}
            >
              Stocks Order History
            </Text>
          </View>
        )}
        {riderStockHistory &&
          riderStockHistory.map((stock) => (
            <View style={styles.userStats}>
              <View style={styles.stats}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  {stock.stockItemType}
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  {stock.quantity} piece
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  £{stock.amount}
                </Text>
              </View>
              <Text
                style={{
                  fontFamily: "popRegular",
                  fontSize: 19,
                  top: 5,
                }}
              >
                {stock.createdDate}
              </Text>
            </View>
          ))}
        {riderInventoryHistory && riderInventoryHistory.length > 0 && (
          <View style={styles.title}>
            <Text
              style={{
                color: colors.white,
                fontFamily: "popSemiBold",
                fontSize: 22,
                alignSelf: "center",
              }}
            >
              Inventory Order History
            </Text>
          </View>
        )}
        {riderInventoryHistory &&
          riderInventoryHistory.map((inventory) => (
            <View style={styles.userStats}>
              <View style={styles.stats}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  {inventory.inventoryItemType}
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  {inventory.quantity} piece
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  £{inventory.amount}
                </Text>
              </View>
              <Text
                style={{
                  fontFamily: "popRegular",
                  fontSize: 19,
                  top: 5,
                }}
              >
                {inventory.createdDate}
              </Text>
            </View>
          ))}
      </ScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  done: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 60,
    padding: 20,
    alignSelf: "center",
    width: "90%",
  },
  title: {
    top: 30,
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
  },
  stats: {
    flexDirection: "row",
  },
  userStats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
    top: 20,
  },
  currentStats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "40%",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
    top: 20,
  },
});

export default RiderStocksScreen;
