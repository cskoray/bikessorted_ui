import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import RiderBookingConfirmationScreen from "../screens/RiderBookingConfirmationScreen";
import RiderReassignConfirmationScreen from "../screens/RiderReassignConfirmationScreen";
import RiderBookingCancelledInformationScreen from "../screens/RiderBookingCancelledInformationScreen";
import RiderHomeScreen from "../screens/RiderHomeScreen";
import RiderBookingDetailScreen from "../screens/RiderBookingDetailScreen";
import RiderFinishedBookingDetailScreen from "../screens/RiderFinishedBookingDetailScreen";
import RiderNotMarkedAsFinishedBookingDetailScreen from "../screens/RiderNotMarkedAsFinishedBookingDetailScreen";
import RiderBookingsScreen from "../screens/RiderBookingsScreen";
import RiderStocksScreen from "../screens/RiderStocksScreen";
import RiderOrderStocksScreen from "../screens/RiderOrderStocksScreen";
import RiderWalletScreen from "../screens/RiderWalletScreen";
import RiderRemovePaymentMethodScreen from "../screens/RiderRemovePaymentMethodScreen";
import RiderAddStripeCardScreen from "../screens/RiderAddStripeCardScreen";

const Stack = createStackNavigator();

const RiderHomeNavigator = () => (
  <Stack.Navigator
    screenOptions={{ headerShown: false, gestureEnabled: false }}
  >
    <Stack.Screen name="RiderHomeScreen" component={RiderHomeScreen} />
    <Stack.Screen
      name="RiderBookingConfirmationScreen"
      component={RiderBookingConfirmationScreen}
    />
    <Stack.Screen
      name="RiderReassignConfirmationScreen"
      component={RiderReassignConfirmationScreen}
    />
    <Stack.Screen
      name="RiderBookingCancelledInformationScreen"
      component={RiderBookingCancelledInformationScreen}
    />
    <Stack.Screen
      name="RiderBookingDetailScreen"
      component={RiderBookingDetailScreen}
    />
    <Stack.Screen
      name="RiderFinishedBookingDetailScreen"
      component={RiderFinishedBookingDetailScreen}
    />
    <Stack.Screen
      name="RiderNotMarkedAsFinishedBookingDetailScreen"
      component={RiderNotMarkedAsFinishedBookingDetailScreen}
    />
    <Stack.Screen name="RiderBookingsScreen" component={RiderBookingsScreen} />
    <Stack.Screen name="RiderStocksScreen" component={RiderStocksScreen} />
    <Stack.Screen
      name="RiderOrderStocksScreen"
      component={RiderOrderStocksScreen}
    />
    <Stack.Screen name="RiderWalletScreen" component={RiderWalletScreen} />
    <Stack.Screen
      name="RiderRemovePaymentMethodScreen"
      component={RiderRemovePaymentMethodScreen}
    />
    <Stack.Screen
      name="RiderAddStripeCardScreen"
      component={RiderAddStripeCardScreen}
    />
  </Stack.Navigator>
);

export default RiderHomeNavigator;
