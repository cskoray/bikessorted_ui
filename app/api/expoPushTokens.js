import client from "./client";

const register = (token) => client.patch("/users/expo", token);

export default {
  register,
};
