import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";
import { format, round } from "mathjs";

import colors from "../config/colors";
import Screen from "../components/Screen";
import routes from "../navigation/routes";
import useApi from "../hooks/useApi";
import bookingApi from "../api/booking";
import ActivityIndicator from "../components/ActivityIndicator";

function RiderBookingsScreen({ navigation }) {
  const bookings = useApi(bookingApi.getRiderBookings);
  const [finishInProgress, setFinishInProgress] = useState(false);

  useEffect(() => {
    bookings.request();
  }, []);

  const finish = async (bookingKey) => {
    setFinishInProgress(true);
    const result = await bookingApi.finishBooking(bookingKey);
    if (result) {
      setFinishInProgress(false);
      navigation.navigate(routes.RiderHome);
    }
  };

  const detailActive = (booking) => {
    const bookingKey = booking.bookingKey;
    const amount = booking.totalAmount;
    const latitude = booking.latitude;
    const longitude = booking.longitude;
    const type = booking.type;
    const bikeType = booking.bikeType;
    const tyreSize = booking.tyreSize;
    const name = booking.name;
    const phone = booking.phone;
    const postcode = booking.postcode;
    const pic = booking.pic;
    const createdDate = booking.createdDate;
    const dueDate = booking.dueDate;
    const services = booking.services;
    const option = booking.option;
    const bikePics = booking.bikePics;
    const params = {
      bookingKey,
      amount,
      latitude,
      longitude,
      type,
      bikeType,
      tyreSize,
      name,
      phone,
      postcode,
      pic,
      createdDate,
      dueDate,
      services,
      option,
      bikePics,
    };
    navigation.navigate(routes.RiderBookingDetail, params);
  };

  // const detailOld = (booking) => {
  //   const bookingKey = booking.bookingKey;
  //   const amount = booking.totalAmount;
  //   const latitude = booking.latitude;
  //   const longitude = booking.longitude;
  //   const type = booking.type;
  //   const bikeType = booking.bikeType;
  //   const tyreSize = booking.tyreSize;
  //   const name = booking.name;
  //   const pic = booking.pic;
  //   const createdDate = booking.createdDate;
  //   const finishedDate = booking.finishedDate;
  //   const services = booking.services;
  //   const params = {
  //     bookingKey,
  //     amount,
  //     latitude,
  //     longitude,
  //     type,
  //     bikeType,
  //     tyreSize,
  //     name,
  //     pic,
  //     createdDate,
  //     finishedDate,
  //     services,
  //   };
  //   navigation.navigate(routes.RiderFinishedBookingDetail, params);
  // };

  // const detailNotMarkedAsFinished = (booking) => {
  //   const bookingKey = booking.bookingKey;
  //   const amount = booking.totalAmount;
  //   const latitude = booking.latitude;
  //   const longitude = booking.longitude;
  //   const type = booking.type;
  //   const bikeType = booking.bikeType;
  //   const tyreSize = booking.tyreSize;
  //   const name = booking.name;
  //   const phone = booking.phone;
  //   const postcode = booking.postcode;
  //   const pic = booking.pic;
  //   const createdDate = booking.createdDate;
  //   const dueDate = booking.dueDate;
  //   const services = booking.services;
  //   const option = booking.option;
  //   const bikePics = booking.bikePics;
  //   const params = {
  //     bookingKey,
  //     amount,
  //     latitude,
  //     longitude,
  //     type,
  //     bikeType,
  //     tyreSize,
  //     name,
  //     phone,
  //     postcode,
  //     pic,
  //     createdDate,
  //     dueDate,
  //     services,
  //     option,
  //     bikePics,
  //   };
  //   navigation.navigate(routes.RiderNotMarkedAsFinishedBookingDetail, params);
  // };

  const close = () => {
    navigation.navigate(routes.RiderHome);
  };

  return (
    <Screen style={styles.screen}>
      <ActivityIndicator visible={bookings.loading || finishInProgress} />
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          alignContent: "center",
          marginBottom: 20,
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            Bookings
          </Text>
        </>
      </TouchableOpacity>
      {bookings.data &&
        (bookings.data.active &&
        bookings.data.active.length === 0 &&
        bookings.data.old &&
        bookings.data.old.length === 0 &&
        bookings.data.notMarkedAsFinished &&
        bookings.data.notMarkedAsFinished.length === 0 ? (
          <View style={{ top: 50 }}>
            <Text
              style={{
                alignSelf: "center",
                fontFamily: "popRegular",
                fontSize: 19,
              }}
            >
              No service bookings yet
            </Text>
          </View>
        ) : (
          <ScrollView>
            <View style={{ marginBottom: 50 }}>
              {bookings.data.notMarkedAsFinished &&
                bookings.data.notMarkedAsFinished.length > 0 && (
                  <Text
                    style={{
                      left: 15,
                      fontFamily: "popSemiBold",
                      fontSize: 16,
                    }}
                  >
                    Bookings not marked as finished
                  </Text>
                )}
              {bookings.data.notMarkedAsFinished &&
                bookings.data.notMarkedAsFinished.map((booking) => (
                  <View
                    style={{
                      flexDirection: "row",
                      borderBottomWidth: 2,
                      borderColor: colors.light,
                    }}
                    key={booking.bookingKey}
                  >
                    <Image
                      source={require("../assets/png/ToolBox.png")}
                      style={styles.bikeImg}
                    />
                    <View
                      style={{
                        alignSelf: "center",
                        width: "50%",
                      }}
                    >
                      <Text>{booking.dueDateFormatted}</Text>
                      {booking.services.map((service) => (
                        <Text
                          style={{ fontSize: 14, fontFamily: "popSemiBold" }}
                        >
                          {service.name}{" "}
                        </Text>
                      ))}
                    </View>
                    <Text
                      style={{
                        alignSelf: "center",
                        fontSize: 14,
                        fontFamily: "popSemiBold",
                      }}
                    >
                      £{round(format(booking.totalAmount, { precision: 6 }), 2)}
                    </Text>
                    <TouchableOpacity
                      style={styles.btnAccept}
                      onPress={() => finish(booking.bookingKey)}
                    >
                      <Text style={styles.btntxt}>finish</Text>
                    </TouchableOpacity>
                  </View>
                ))}
              {bookings.data.active && bookings.data.active.length > 0 && (
                <Text
                  style={{ left: 15, fontFamily: "popSemiBold", fontSize: 16 }}
                >
                  Active bookings
                </Text>
              )}
              {bookings.data.active &&
                bookings.data.active.map((booking) => (
                  <TouchableOpacity onPress={() => detailActive(booking)}>
                    <View
                      style={{
                        flexDirection: "row",
                        borderBottomWidth: 2,
                        borderColor: colors.light,
                      }}
                      key={booking.bookingKey}
                    >
                      <Image
                        source={require("../assets/png/ToolBox.png")}
                        style={styles.bikeImg}
                      />
                      <View
                        style={{
                          alignSelf: "center",
                          width: "50%",
                        }}
                      >
                        <Text>{booking.dueDateFormatted}</Text>
                        {booking.services.map((service) => (
                          <Text
                            style={{ fontSize: 14, fontFamily: "popSemiBold" }}
                            key={service.name}
                          >
                            {service.name}{" "}
                          </Text>
                        ))}
                      </View>
                      <Text
                        style={{
                          alignSelf: "center",
                          fontSize: 14,
                          fontFamily: "popSemiBold",
                        }}
                      >
                        £
                        {round(
                          format(booking.totalAmount, { precision: 6 }),
                          2
                        )}
                      </Text>
                      <Image
                        source={require("../assets/png/Iconly-Light-outline-Arrow-RightCircle.png")}
                        style={{
                          resizeMode: "contain",
                          width: 23,
                          height: 26,
                          alignSelf: "center",
                          left: "40%",
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                ))}
              <View style={{ top: 30 }}>
                {bookings.data.old && bookings.data.old.length > 0 && (
                  <Text
                    style={{
                      left: 15,
                      fontFamily: "popSemiBold",
                      fontSize: 16,
                    }}
                  >
                    Past 5 bookings
                  </Text>
                )}
              </View>
              <View style={{ top: 30 }}>
                {bookings.data.old &&
                  bookings.data.old.map((booking) => (
                    <View
                      key={booking.bookingKey}
                      style={{
                        flexDirection: "row",
                        borderBottomWidth: 2,
                        borderColor: colors.light,
                      }}
                    >
                      <Image
                        source={require("../assets/png/ToolBox.png")}
                        style={styles.bikeImg}
                      />
                      <View
                        style={{
                          alignSelf: "center",
                          width: "50%",
                        }}
                      >
                        <Text>{booking.dueDateFormatted}</Text>
                        {booking.services.map((service) => (
                          <Text
                            style={{ fontSize: 14, fontFamily: "popSemiBold" }}
                          >
                            {service.name}{" "}
                          </Text>
                        ))}
                      </View>
                      <Text
                        style={{
                          alignSelf: "center",
                          fontSize: 14,
                          fontFamily: "popSemiBold",
                        }}
                      >
                        £
                        {round(
                          format(booking.totalAmount, { precision: 6 }),
                          2
                        )}
                      </Text>
                    </View>
                  ))}
              </View>
            </View>
          </ScrollView>
        ))}
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  bikeImg: {
    margin: 15,
    width: 47,
    height: 40,
    top: 0,
    borderRadius: 10,
  },
  btnAccept: {
    // flex: 0.5,
    backgroundColor: colors.blue,
    padding: 7,
    left: 5,
    alignSelf: "center",
    borderRadius: 5,
  },
  btntxt: {
    color: colors.white,
    alignSelf: "center",
    fontFamily: "popSemiBold",
    fontSize: 16,
  },
});

export default RiderBookingsScreen;
