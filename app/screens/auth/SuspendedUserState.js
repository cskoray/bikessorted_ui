import React from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import Screen from "../../components/Screen";
import colors from "../../config/colors";

export default function SuspendedUserState(props) {
  return (
    <Screen style={styles.limitContainer}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          left: 0,
          width: "50%",
          flexDirection: "row",
          justifyContent: "center",
          top: 20,
        }}
        onPress={props.close}
      >
        <>
          <Image
            source={require("../../assets/png/Iconly-Light-outline-Home.png")}
            style={styles.ImageIconStyle}
            resizeMode="contain"
          />
        </>
      </TouchableOpacity>
      <View
        style={{
          top: 50,
        }}
      >
        <Text
          style={{
            alignSelf: "center",
            fontFamily: "popRegular",
            fontSize: 16,
          }}
        >
          {props.emailTried} is suspended
        </Text>
        <Text
          style={{
            alignSelf: "center",
            top: 10,
            fontFamily: "popRegular",
            fontSize: 16,
          }}
        >
          Please contact
        </Text>
        <Text
          style={{
            alignSelf: "center",
            top: 20,
            fontFamily: "popBold",
            fontSize: 24,
          }}
        >
          info@rubiebikes.com
        </Text>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  limitContainer: {
    padding: 10,
    fontFamily: "popBold",
    fontSize: 24,
    color: colors.black,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
    backgroundColor: colors.newWhite,
    borderRadius: 20,
    right: "100%",
  },
});
