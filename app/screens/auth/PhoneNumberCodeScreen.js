import React from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";

import { Form, FormField } from "../../components/forms";

import Screen from "../../components/Screen";
import Button from "../../components/Button";
import Icon from "../../components/Icon";
import colors from "../../config/colors";

const validationSchema = Yup.object().shape({
  code: Yup.string().required().label("verification code"),
});

function PhoneNumberCodeScreen({ navigation }) {
  return (
    <Screen style={styles.container}>
      <Icon
        alignSelf={"left"}
        name="bicycle"
        size={40}
        backgroundColor={colors.primary}
      />

      <Form
        initialValues={{ code: "" }}
        onSubmit={(values) => console.log(values)}
        validationSchema={validationSchema}
      >
        <FormField
          autoCorrect={false}
          icon="check"
          name="code"
          placeholder="verification code"
        />
      </Form>
      <Button // SubmitButton
        style={styles.phone}
        title="VERIFY"
        onPress={() => navigation.navigate("CheckPhoneNumberActivatedScreen")}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingTop: 20,
  },
  phone: {
    backgroundColor: colors.primary,
  },
});

export default PhoneNumberCodeScreen;
