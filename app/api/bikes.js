import client from "./client";

const getBikes = () => client.get("/bikes");
const getBike = (bikeKey) => client.get(`/bikes/${bikeKey}/bike`);
const getBikeTypes = () => client.get("/bikes/bike-types");
const getTyreTypes = () => client.get("/bikes/tyre-types");

const deleteBike = (bikeKey) => client.delete(`/bikes/bike/${bikeKey}/remove`);

export const addBike = (bike, onUploadProgress) => {
  const data = new FormData();
  data.append("name", bike.name);
  data.append("tyreType", bike.tyreType.value);
  data.append("type", bike.type.value);

  bike.images.forEach((image, index) =>
    data.append("images", {
      name: "image" + index,
      type: "image/jpeg",
      uri: image,
    })
  );

  return client
    .post("/bikes", data, {
      onUploadProgress: (progress) =>
        onUploadProgress(progress.loaded / progress.total),
    })
    .then(console.log);
};

export default {
  addBike,
  getBikes,
  getBikeTypes,
  getTyreTypes,
  deleteBike,
  getBike
};
