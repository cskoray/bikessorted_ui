import client from "./client";

const getDetails = () => client.get("/users/details");

const getShopAdminDetails = () => client.get("/users/shop/admin/details");

export const addProfilePic = (pickerResult, onUploadProgress) => {
  const data = new FormData();
  data.append("images", {
    name: "image0",
    type: "image/jpeg",
    uri: pickerResult.uri,
  });

  return client
    .post("/users/profile/pic", data, {
      onUploadProgress: (progress) =>
        onUploadProgress(progress.loaded / progress.total),
    })
    .catch((err) => console.error(err));
};

export default {
  addProfilePic,
  getDetails,
  getShopAdminDetails,
};
