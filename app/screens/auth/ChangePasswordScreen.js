import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import * as Yup from "yup";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";

import Screen from "../../components/Screen";
import colors from "../../config/colors";
import authApi from "../../api/auth";
import useAuth from "../../auth/useAuth";
import authStorage from "../../auth/storage";

const validationSchema = Yup.object().shape({
  password: Yup.string().required().min(4).label("password"),
});

function ChangePasswordScreen({ route, navigation }) {
  const [user, setUser] = useState();
  const auth = useAuth();
  const [error, setError] = useState();

  const handleSubmit = async (userInfo) => {
    if (route && route.params) {
      const result = await authApi.updatePasswordCode(
        route.params.email,
        route.params.code,
        userInfo.password
      );
      if (result.ok) {
        const loginResult = await authApi.login(
          route.params.email,
          userInfo.password
        );
        auth.logIn(loginResult.data);
        const user = await authStorage.getUser();
        setUser(user);
      } else {
        alert("Something went wrong, please try again");
        navigation.navigate("Login");
      }
    }
  };

  return (
    <Screen style={styles.container}>
      <View style={{ alignSelf: "center" }}>
        <Form
          initialValues={{ password: "" }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <ErrorMessage error={error} visible={error} />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon={require("../../assets/png/Iconly-Light-outline-Lock.png")}
            name="password"
            placeholder="Password"
            secureTextEntry
            textContentType="password"
          />
          <View style={{ top: 50, backgroundColor: colors.blue }}>
            <SubmitButton
              title="Update password"
              style={styles.submitBtn}
              autoCapitalize="none"
            />
          </View>
        </Form>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingTop: 20,
    justifyContent: "center",
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
  policy: {
    color: colors.darkGray,
    justifyContent: "center",
    paddingHorizontal: 20,
    fontSize: 10,
    fontFamily: "popLight",
    alignSelf: "center",
  },
});

export default ChangePasswordScreen;
