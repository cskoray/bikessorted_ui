import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";

import colors from "../config/colors";
import HomeNavigator from "./HomeNavigator";
import DrawerContent from "../screens/drawer/DrawerContent";

const Drawer = createDrawerNavigator();

const UserNavigator = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <DrawerContent {...props} />}
      drawerStyle={{
        backgroundColor: colors.blue,
        width: "80%",
        shadowRadius: 5,
        shadowOffset: {
          width: 5,
          height: 10,
        },
        shadowOpacity: 0.2,
        flex: 1,
      }}
      hideStatusBar="true"
    >
      <Drawer.Screen name="Booking" component={HomeNavigator} />
    </Drawer.Navigator>
  );
};

export default UserNavigator;
