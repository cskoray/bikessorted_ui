import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
} from "react-native";
import routes from "../navigation/routes";
import colors from "../config/colors";
import Screen from "../components/Screen";
import ridersApi from "../api/riders";

function AdminUsersScreen({ navigation }) {
  const [userStatusCounts, setUserStatusCounts] = useState();
  const [riderStats, setRiderStats] = useState();

  const close = () => {
    navigation.navigate(routes.AdminHome);
  };

  useEffect(() => {
    const getUserStatusCounts = async () => {
      const result = await ridersApi.getUserStatusCounts();
      if (result && result.data) {
        setUserStatusCounts(result.data);
      }
    };
    const getRiderStats = async () => {
      const result = await ridersApi.getRiderStats();
      if (result && result.data) {
        setRiderStats(result.data);
      }
    };
    getUserStatusCounts();
    getRiderStats();
  }, []);

  return (
    <Screen style={styles.screen}>
      <ScrollView>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "100%",
            left: 0,
            flexDirection: "row",
            justifyContent: "flex-start",
            height: 60,
          }}
          onPress={close}
        >
          <>
            <Image
              source={require("../assets/png/ArrowCloseCircle.png")}
              style={styles.ImageIconStyle}
            />
          </>
        </TouchableOpacity>
        <View>
          {userStatusCounts && userStatusCounts.counts.length > 0 && (
            <View style={styles.title}>
              <Text
                style={{
                  color: colors.white,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                }}
              >
                Customer counts
              </Text>
            </View>
          )}
          {userStatusCounts &&
            userStatusCounts.counts.map((count) => (
              <View style={styles.userStats} key={count.status}>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                  }}
                >
                  {count.status}
                </Text>
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 22,
                    alignSelf: "center",
                    marginLeft: "auto",
                  }}
                >
                  {count.count}
                </Text>
              </View>
            ))}
          {riderStats && riderStats.riderStats.length > 0 && (
            <View style={styles.title}>
              <Text
                style={{
                  color: colors.white,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                }}
              >
                Rider Stats
              </Text>
            </View>
          )}
          {riderStats &&
            riderStats.riderStats.map((rider) => (
              <View style={styles.riderStats}>
                <View style={styles.stats} key={rider.email}>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "center",
                    }}
                  >
                    {rider.name}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "popSemiBold",
                      fontSize: 22,
                      alignSelf: "center",
                      marginLeft: "auto",
                    }}
                  >
                    Total rides: {rider.finishedBookings}
                  </Text>
                </View>
                <Text
                  style={{
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "flex-start",
                  }}
                >
                  {rider.email}
                </Text>
                <Text
                  style={{
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "flex-start",
                  }}
                >
                  {rider.phone}
                </Text>
                <Text
                  style={{
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "flex-start",
                  }}
                >
                  has {rider.rating} rating
                </Text>
                <Text
                  style={{
                    fontFamily: "popRegular",
                    fontSize: 22,
                    alignSelf: "flex-start",
                  }}
                >
                  Joined in {rider.joinedDate}
                </Text>
              </View>
            ))}
        </View>
      </ScrollView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.white,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
  stats: {
    flexDirection: "row",
  },
  riderStats: {
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
  userStats: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.newWhite,
    width: "90%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    borderRadius: 10,
  },
  title: {
    flexDirection: "row",
    padding: 15,
    margin: 10,
    backgroundColor: colors.black,
    width: "100%",
    alignSelf: "center",
    elevation: 15,
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
  },
});

export default AdminUsersScreen;
