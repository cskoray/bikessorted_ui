import React, { useState, useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, Image } from "react-native";
import LottieView from "lottie-react-native";
import { Animated, MarkerAnimated } from "react-native-maps";

import routes from "../navigation/routes";
import colors from "../config/colors";
import Constants from "expo-constants";
import ridersApi from "../api/riders";

function ShopAdminRidersScreen({ navigation }) {
  const lat = "51.56956539685336";
  const long = "-0.10939294141637967";
  const [riders, setRiders] = useState();
  var interval;

  const close = () => {
    clearInterval(interval);
    navigation.navigate(routes.ShopAdminHome);
  };

  const checkForRiderLocations = async () => {
    const result = await ridersApi.getRidersLocations();
    if (result && result.data) {
      setRiders(result.data);
      console.log(new Date());
    }
  };

  useEffect(() => {
    navigation.addListener("focus", () => {
      interval = setInterval(() => {
        checkForRiderLocations();
      }, 10000);
      return () => {
        clearTimeout(interval);
        clearInterval(interval);
      };
    });
  }, [lat, long]);

  return (
    <>
      {Platform.OS === "ios" ? (
        <Animated
          style={{ flex: 1 }}
          mapType="mutedStandard"
          zoomControlEnabled
          zoomEnabled
          provider={"google"}
          paddingAdjustmentBehavior="automatic"
          initialRegion={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.02222,
            longitudeDelta: 0.02421,
          }}
          mapType="mutedStandard"
        >
          {riders &&
            riders.riders.map((rider) => (
              <MarkerAnimated
                coordinate={{
                  latitude: parseFloat(rider.latitude),
                  longitude: parseFloat(rider.longitude),
                }}
                key={rider.lat}
              >
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 20,
                    width: "auto",
                    alignSelf: "center",
                    left: 10,
                    top: 3,
                    elevation: 15,
                    shadowRadius: 5,
                    shadowOffset: {
                      width: 5,
                      height: 10,
                    },
                    shadowOpacity: 0.2,
                    borderRadius: 20,
                    backgroundColor: colors.newWhite,
                    padding: 5,
                  }}
                >
                  {rider.name} {rider.lastUpdateDate}
                </Text>
                {rider.isFree !== "free" && (
                  <LottieView
                    autoPlay
                    loop
                    source={require("../assets/animations/blink.json")}
                    style={{
                      left: 5,
                      width: 35,
                      height: 35,
                      position: "absolute",
                      left: 25,
                      bottom: 5,
                    }}
                  />
                )}
                <Image
                  source={require("../assets/png/Repairman.png")}
                  style={{
                    height: 20,
                  }}
                  resizeMode="contain"
                />
              </MarkerAnimated>
            ))}
        </Animated>
      ) : (
        <Animated
          style={{ flex: 1 }}
          mapType="mutedStandard"
          showsMyLocationButton
          zoomControlEnabled
          zoomEnabled
          provider={"google"}
          paddingAdjustmentBehavior="automatic"
          initialRegion={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.12222,
            longitudeDelta: 0.12421,
          }}
          mapType="mutedStandard"
          region={{
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
            latitudeDelta: 0.12222,
            longitudeDelta: 0.12421,
          }}
        >
          {riders &&
            riders.riders.map((rider) => (
              <MarkerAnimated
                coordinate={{
                  latitude: parseFloat(rider.latitude),
                  longitude: parseFloat(rider.longitude),
                }}
                key={rider.lat}
              >
                <Text
                  style={{
                    fontFamily: "popSemiBold",
                    fontSize: 20,
                    width: 50,
                    alignSelf: "center",
                    left: 10,
                    top: 3,
                  }}
                >
                  {rider.name}
                </Text>
                <LottieView
                  autoPlay
                  loop
                  source={require("../assets/animations/blink.json")}
                  style={{
                    left: 5,
                    width: 35,
                    height: 35,
                    position: "absolute",
                    left: 25,
                    bottom: 5,
                  }}
                />
                <Image
                  source={require("../assets/png/Repairman.png")}
                  style={{
                    height: 25,
                  }}
                  resizeMode="contain"
                />
              </MarkerAnimated>
            ))}
        </Animated>
      )}
      <TouchableOpacity
        style={{
          top: Constants.statusBarHeight,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          justifyContent: "flex-start",
          // marginBottom: 20,
          height: 60,
          position: "absolute",
          zIndex: 9999,
          alignSelf: "center",
        }}
        onPress={close}
      >
        <>
          <Image
            source={require("../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popRegular",
              fontSize: 22,
              alignSelf: "center",
              left: 20,
            }}
          >
            all riders
          </Text>
        </>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.newWhite,
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  img: {
    alignSelf: "center",
    top: 20,
    width: 260,
    height: 220,
    resizeMode: "contain",
  },
});

export default ShopAdminRidersScreen;
