import React from "react";
import { View, TextInput, StyleSheet } from "react-native";

import defaultStyles from "../config/styles";
import Icon from "./Icon";
import colors from "../config/colors";

function AppTextInput({
  icon,
  width = "100%",
  backColor = colors.darkGray,
  noIcon = false,
  ...otherProps
}) {
  return (
    <View style={[styles.container, { width }]}>
      {!noIcon && (
        <View
          style={{
            backgroundColor: backColor,
            width: 47,
            height: 61,
            justifyContent: "center",
          }}
        >
          {icon && (
            <Icon
              name={icon}
              color={defaultStyles.colors.darkGray}
              style={styles.icon}
            />
          )}
        </View>
      )}
      <View
        style={{
          width: "85%",
          backgroundColor: colors.lightGray,
          justifyContent: "center",
        }}
      >
        <TextInput
          placeholderTextColor={defaultStyles.colors.gray}
          style={[styles.inpt, defaultStyles.text]}
          {...otherProps}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 0,
    flexDirection: "row",
    padding: 5,
    paddingTop: 10,
  },
  icon: {
    width: 11,
    height: 13,
  },
  inpt: { backgroundColor: colors.newWhite, height: 61, paddingLeft: 10 },
});

export default AppTextInput;
