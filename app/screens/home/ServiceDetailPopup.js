import React from "react";
import { BlurView } from "expo-blur";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Screen from "../../components/Screen";
import colors from "../../config/colors";

export default function ServiceDetailPopup(props) {
  const { cancelServiceDetailPopUpSelected, serviceDetailSelected } = props;

  return (
    <BlurView intensity={100} style={StyleSheet.absoluteFill} tint="dark">
      <Screen style={{ justifyContent: "flex-end" }}>
        <TouchableOpacity
          style={{
            padding: 0,
            margin: 0,
            width: "100%",
            left: 0,
            flexDirection: "row",
            alignSelf: "flex-start",
          }}
          onPress={cancelServiceDetailPopUpSelected}
        >
          <Image
            source={require("../../assets/png/ArrowCloseCircle.png")}
            style={styles.ImageIconStyle}
          />
        </TouchableOpacity>
        <View
          style={{
            backgroundColor: colors.darkGray,
            width: "100%",
            height: "80%",
            borderRadius: 45,
            top: 100,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{
                color: colors.newWhite,
                fontSize: 20,
                fontFamily: "popMedium",
                paddingTop: 40,
                left: 20,
              }}
            >
              {serviceDetailSelected.name}
            </Text>
            <Text
              style={{
                color: colors.newWhite,
                fontSize: 25,
                fontFamily: "popBold",
                paddingTop: 40,
                marginLeft: "auto",
                right: 10,
              }}
            >
              £{serviceDetailSelected.price}
            </Text>
          </View>
          <Text
            style={{
              color: colors.newWhite,
              fontSize: 20,
              fontFamily: "popMedium",
              paddingTop: 10,
              left: 20,
            }}
          >
            {serviceDetailSelected.description}
          </Text>
          <View>
            {serviceDetailSelected.descriptions.map((desc) => (
              <Text
                key={desc.description}
                style={{
                  color: colors.newWhite,
                  fontSize: 14,
                  fontFamily: "popRegular",
                  paddingTop: 15,
                  left: 20,
                }}
              >
                - {desc.description}
              </Text>
            ))}
          </View>
        </View>
      </Screen>
    </BlurView>
  );
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
});
