import React, { useState } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import * as Yup from "yup";
import { createStackNavigator } from "@react-navigation/stack";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";

import Screen from "../components/Screen";
import colors from "../config/colors";
import ActivityIndicator from "../components/ActivityIndicator";
import routes from "../navigation/routes";
import ridersApi from "../api/riders";
import useAuth from "../auth/useAuth";
import authStorage from "../auth/storage";
import NewRiderAddStripeCardScreen from "./NewRiderAddStripeCardScreen";

const validationSchema = Yup.object().shape({
  password: Yup.string().required().min(4).label("password"),
});

function AdminNewRiderNewPasswordScreen({ navigation }) {
  const [user, setUser] = useState();
  const [error, setError] = useState();
  const [saving, setSaving] = useState(false);
  const auth = useAuth();
  const Stack = createStackNavigator();

  const handleSubmit = async (userInfo) => {
    setSaving(true);
    const result = await ridersApi.setNewPasswordNewRider(userInfo.password);
    if (result && result.data) {
      setSaving(false);
      auth.logIn(result.data);
      const user = await authStorage.getUser();
      if (user) {
        setUser(user);
      }
    } else {
      setSaving(false);
      setError(result.data.error);
    }
  };

  const close = () => {
    navigation.navigate(routes.AdminHome);
  };

  return (
    <>
      {user && user.status === "CARD_RIDER" ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="NewRiderAddStripeCardScreen"
            component={NewRiderAddStripeCardScreen}
          />
        </Stack.Navigator>
      ) : (
        <Screen style={styles.container}>
          <ActivityIndicator visible={saving} />
          <TouchableOpacity
            style={{
              padding: 0,
              margin: 0,
              width: "100%",
              left: 0,
              flexDirection: "row",
              justifyContent: "flex-start",
              height: 60,
            }}
            onPress={close}
          >
            <>
              <Image
                source={require("../assets/png/ArrowCloseCircle.png")}
                style={styles.ImageIconStyle}
              />
            </>
          </TouchableOpacity>
          <View style={{ alignSelf: "center", top: 70 }}>
            <Text
              style={{
                color: colors.darkGray,
                fontFamily: "popSemiBold",
                fontSize: 18,
                alignSelf: "center",
                bottom: 40,
              }}
            >
              Please set new password
            </Text>
            <Form
              initialValues={{ password: "" }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <ErrorMessage error={error} visible={error} />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon={require("../assets/png/Iconly-Light-outline-Lock.png")}
                name="password"
                placeholder="Password"
                secureTextEntry
                textContentType="password"
              />
              <View style={{ top: 50, backgroundColor: colors.blue }}>
                <SubmitButton
                  title="Update password"
                  style={styles.submitBtn}
                  autoCapitalize="none"
                />
              </View>
            </Form>
          </View>
        </Screen>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  back: {
    backgroundColor: "#f4a261",
  },
  submitBtn: {
    backgroundColor: colors.blue,
    borderRadius: 0,
  },
  policy: {
    color: colors.darkGray,
    justifyContent: "center",
    paddingHorizontal: 20,
    fontSize: 10,
    fontFamily: "popLight",
    alignSelf: "center",
  },
  resendEnabled: {
    alignSelf: "center",
    color: colors.blue,
    fontFamily: "popRegular",
    fontSize: 19,
  },
  resendDisabled: {
    alignSelf: "center",
    color: colors.gray,
    fontFamily: "popRegular",
    fontSize: 19,
  },
});

export default AdminNewRiderNewPasswordScreen;
