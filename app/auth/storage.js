import * as SecureStore from "expo-secure-store";
import jwtDecode from "jwt-decode";

const key = "authToken";

const storeAppleLoginName = async (user) => {
  try {
    await SecureStore.setItemAsync("appleUser", user);
  } catch (error) {
    console.log("Error storing the apple user", error);
  }
};

const getAppleLoginName = async (user) => {
  try {
    return await SecureStore.getItemAsync("appleUser");
  } catch (error) {
    console.log("Error getting the apple user", error);
  }
};

const storeToken = async (authToken) => {
  try {
    await SecureStore.setItemAsync(key, authToken);
  } catch (error) {
    console.log("Error storing the auth token", error);
  }
};

const getToken = async () => {
  try {
    return await SecureStore.getItemAsync(key);
  } catch (error) {
    console.log("Error getting the auth token", error);
  }
};

const getUser = async () => {
  const token = await getToken();
  return token ? jwtDecode(token) : null;
};

const removeToken = async () => {
  try {
    await SecureStore.deleteItemAsync(key);
  } catch (error) {
    console.log("Error removing the auth token", error);
  }
};

export default {
  getToken,
  getUser,
  removeToken,
  storeToken,
  storeAppleLoginName,
  getAppleLoginName,
};
