import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import colors from "../../config/colors";

export default function RiderDrawerToggle(props) {
  const { navigation } = props;
  const toggleDrawer = () => {
    navigation.toggleDrawer();
  };
  return (
    <View
      style={{
        flexDirection: "row",
        flex: 0.3,
        backgroundColor: colors.darkGray,
        padding: 0,
        width: "100%",
      }}
    >
      <TouchableOpacity
        onPress={toggleDrawer}
        style={{ left: 5, width: 50, top: 15 }}
      >
        <Image
          source={require("../../assets/png/menu.png")}
          style={{
            width: "60%",
            height: "60%",
            margin: 5,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
}
