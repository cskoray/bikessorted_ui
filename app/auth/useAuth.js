import { useContext } from "react";
import jwtDecode from "jwt-decode";

import AuthContext from "./context";
import BookingContext from "./bookingContext";
import authStorage from "./storage";

export default useAuth = () => {
  const { user, setUser } = useContext(AuthContext);
  const { booking, setBooking } = useContext(BookingContext);

  const logIn = ({ accessToken }) => {
    const user = jwtDecode(accessToken);
    setUser(user);
    authStorage.storeToken(accessToken);
    setBooking({
      bike: {
        bikeKey: "-",
      },
      service: {
        serviceKeys: [],
      },
      optionalService: {
        optionalServiceKeys: [],
      },
      payment: {
        cardKey: "-",
      },
      location: {
        latitude: "-",
        longitude: "-",
      },
      postcode: "-",
      calendar: {
        dateTime: "-",
      },
      points: {
        use: "n",
        amount: 0,
      },
    });
  };

  const logOut = () => {
    setUser(null);
    authStorage.removeToken();
  };

  return { user, setUser, booking, setBooking, logIn, logOut };
};
