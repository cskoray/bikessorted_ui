import React, { useEffect, useRef, useState } from "react";
import {
  FlatList,
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
} from "react-native";

import useAuth from "../auth/useAuth";
import ActivityIndicator from "../components/ActivityIndicator";
import Button from "../components/Button";
import BikeCard from "../components/BikeCard";
import bikesApi from "../api/bikes";
import useApi from "../hooks/useApi";
import routes from "../navigation/routes";
import Screen from "../components/Screen";
import colors from "../config/colors";

function BikesScreen({ route, navigation }) {
  const [referer, setReferer] = useState();
  const { booking, setBooking } = useAuth();
  const getBikesApi = useApi(bikesApi.getBikes);
  const [refreshing, setRefreshing] = useState(false);
  const flatL = useRef();

  useEffect(() => {
    getBikesApi.request();
    if (route && route.params && route.params.refer) {
      setReferer(route.params.refer);
    }
  }, []);

  return (
    <Screen style={{ flex: 1 }}>
      <TouchableOpacity
        style={{
          padding: 0,
          margin: 0,
          width: "100%",
          left: 0,
          backgroundColor: colors.darkGray,
          flexDirection: "row",
          alignContent: "center",
        }}
        onPress={() => {
          if (referer && referer === "drawer") {
            navigation.navigate(routes.HOME);
          } else if (booking.service.type === "LATER") {
            navigation.navigate(routes.PICK_LOCATION);
          } else {
            navigation.navigate(routes.HOME);
          }
        }}
      >
        <>
          {referer === "drawer" ? (
            <>
              <Image
                source={require("../assets/png/ArrowCloseCircle.png")}
                style={styles.ImageIconStyle}
              />
              <Text
                style={{
                  color: colors.newWhite,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                  left: 20,
                }}
              >
                Edit bicycles
              </Text>
            </>
          ) : (
            <>
              <Image
                source={require("../assets/png/ArrowLeftCircle.png")}
                style={styles.ImageIconStyle}
              />
              <Text
                style={{
                  color: colors.newWhite,
                  fontFamily: "popRegular",
                  fontSize: 22,
                  alignSelf: "center",
                  left: 20,
                }}
              >
                Pick a bicycle
              </Text>
            </>
          )}
        </>
      </TouchableOpacity>
      <ActivityIndicator visible={getBikesApi.loading} />
      <TouchableOpacity
        onPress={() =>
          navigation.navigate(routes.BIKE, {
            refresh: () => getBikesApi.request(),
          })
        }
        style={{ height: 70, top: 10 }}
      >
        <View>
          <Image
            source={require("../assets/png/Iconly-Light-outline-Plus.png")}
            style={styles.addBikePlusStyle}
          />
          <Text
            style={{
              color: colors.darkGray,
              fontFamily: "popBold",
              fontSize: 16,
              alignSelf: "center",
              top: 10,
            }}
          >
            add new bicycle
          </Text>
        </View>
      </TouchableOpacity>
      {getBikesApi.error && (
        <>
          <Text>Couldn't retrieve your bicycles</Text>
          <Button title="Retry" onPress={() => getBikesApi.request()} />
        </>
      )}
      <View style={{ flex: 1 }}>
        {getBikesApi.data && getBikesApi.data.length > 0 ? (
          <FlatList
            data={getBikesApi.data}
            ref={flatL}
            onContentSizeChange={() => flatL.current.scrollToEnd()}
            refreshing={refreshing}
            onRefresh={() => getBikesApi.request()}
            style={{ width: "100%" }}
            keyExtractor={(bike) => bike.bikeKey}
            renderItem={({ item }) => (
              <BikeCard
                refresh={() => getBikesApi.request()}
                refer={referer}
                navigation={navigation}
                bikeKey={item.bikeKey}
                name={item.name}
                type={item.bikeType}
                tyreSize={item.tyreSize}
                imageUrl={item.bikePictures[0].name}
                thumbnailUrl={item.bikePictures[0].thumbnailUrl}
                onPress={() => {
                  if (!referer) {
                    booking.bike.bikeKey = item.bikeKey;
                    setBooking(booking);
                    if (booking.service.type === "LATER") {
                      navigation.navigate(routes.WALLET);
                    } else {
                      navigation.navigate(routes.PICK_LOCATION);
                    }
                  }
                }}
              />
            )}
          />
        ) : (
          <View>
            <Image
              source={require("../assets/png/bikes.png")}
              style={styles.addBikeImgStyle}
            />
            <Text
              style={{
                fontFamily: "popRegular",
                fontSize: 16,
                alignSelf: "center",
                top: 10,
              }}
            >
              You do not have any bikes yet
            </Text>
          </View>
        )}
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    justifyContent: "center",
  },
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  addBikeImgStyle: {
    width: 322,
    height: 226,
    alignSelf: "center",
  },
  addBikePlusStyle: {
    width: 18,
    height: 18,
    alignSelf: "center",
    top: 5,
    alignSelf: "center",
  },
});

export default BikesScreen;
