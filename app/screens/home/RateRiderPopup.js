import React, { useState } from "react";
import { BlurView } from "expo-blur";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  Platform,
  ImageBackground,
} from "react-native";
import { Avatar } from "react-native-paper";
import { Rating } from "react-native-ratings";

import Screen from "../../components/Screen";
import colors from "../../config/colors";

export default function RateRiderPopup(props) {
  const { rider, cancelRating, rateRider } = props;
  const [yesPressed, setYesPressed] = useState(false);
  const [noPressed, setNoPressed] = useState(false);
  const [yesNoSelection, setYesNoSelection] = useState("no");
  const [rating, setRating] = useState(4);

  const yes = () => {
    if (!yesPressed) {
      setYesNoSelection("yes");
      setYesPressed(true);
      setNoPressed(false);
      rateRider(rating, "yes", rider.name);
    }
  };
  const no = () => {
    if (!noPressed) {
      setNoPressed(true);
      setYesPressed(false);
      setYesNoSelection("no");
      rateRider(rating, "no", rider.name);
    }
  };

  return (
    <>
      {Platform.OS == "ios" ? (
        <BlurView intensity={100} style={StyleSheet.absoluteFill}>
          <>
            <Screen style={{ justifyContent: "flex-start" }}>
              <TouchableOpacity
                style={{
                  padding: 0,
                  margin: 0,
                  width: "100%",
                  left: 0,
                }}
                onPress={cancelRating}
              >
                <Image
                  source={require("../../assets/png/ArrowCloseCircle.png")}
                  style={styles.ImageIconStyle}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: "popBold",
                  left: 15,
                  alignSelf: "center",
                  top: "10%",
                }}
              >
                How was {rider.name}'s service?
              </Text>
              <View
                style={{
                  justifyContent: "center",
                  top: "10%",
                  padding: "5%",
                  margin: "5%",
                  backgroundColor: colors.white,
                  flex: 0.5,
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Avatar.Image
                    source={{
                      uri: rider.profilePicture,
                    }}
                    size={55}
                  />
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: "popSemiBold",
                      left: 15,
                      alignSelf: "center",
                    }}
                  >
                    {rider.name}
                  </Text>
                </View>
                <Rating
                  type="custom"
                  fractions={false}
                  defaultRating={1}
                  startingValue={4.5}
                  showRating={false}
                  style={{
                    alignSelf: "flex-start",
                    padding: 5,
                  }}
                  imageSize={38}
                  ratingBackgroundColor="#c8c7c8"
                  onFinishRating={(rate) => {
                    setRating(rate);
                    rateRider(rate, yesNoSelection, rider.name);
                  }}
                />
                <View style={{ flexDirection: "row", width: "100%", top: 10 }}>
                  <Text style={{ fontSize: 12, fontFamily: "popRegular" }}>
                    bad
                  </Text>
                  <Text
                    style={{
                      left: 120,
                      fontSize: 12,
                      fontFamily: "popRegular",
                    }}
                  >
                    very good
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: "popSemiBold",
                      top: 20,
                    }}
                  >
                    Rubie does not ask for cash!
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: 20,
                      fontFamily: "popSemiBold",
                      top: 20,
                    }}
                  >
                    Were you asked to do so?
                  </Text>
                  <View style={{ top: 20, flexDirection: "row" }}>
                    <TouchableOpacity
                      onPress={yes}
                      style={yesPressed ? styles.btnYesPressed : styles.btnYes}
                    >
                      <Text style={styles.btntxt}>yes</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={no}
                      style={noPressed ? styles.btnNoPressed : styles.btnNo}
                    >
                      <Text style={styles.btntxt}>no</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Screen>
          </>
        </BlurView>
      ) : (
        <Modal>
          <ImageBackground
            source={require("../../assets/png/backgroundBlur.jpg")}
            style={{
              flex: 1,
              resizeMode: "cover",
              justifyContent: "center",
              zIndex: 999999,
            }}
            blurRadius={20}
          >
            <>
              <Screen style={{ justifyContent: "flex-start" }}>
                <TouchableOpacity
                  style={{
                    padding: 0,
                    margin: 0,
                    width: "100%",
                    left: 0,
                  }}
                  onPress={cancelRating}
                >
                  <Image
                    source={require("../../assets/png/ArrowCloseCircle.png")}
                    style={styles.ImageIconStyle}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: "popBold",
                    left: 15,
                    alignSelf: "center",
                    top: "10%",
                  }}
                >
                  How was {rider.name}'s service?
                </Text>
                <View
                  style={{
                    justifyContent: "center",
                    top: "20%",
                    padding: "5%",
                    margin: "5%",
                    backgroundColor: colors.white,
                    elevation: 15,
                    shadowRadius: 5,
                    shadowOffset: {
                      width: 10,
                      height: 10,
                    },
                    shadowOpacity: 0.6,
                    borderRadius: 5,
                  }}
                >
                  <View style={{ flexDirection: "row", padding: 5 }}>
                    <Avatar.Image
                      source={{
                        uri: rider.profilePicture,
                      }}
                      size={55}
                    />
                    <Text
                      style={{
                        fontSize: 20,
                        fontFamily: "popSemiBold",
                        left: 15,
                        alignSelf: "center",
                      }}
                    >
                      {rider.name}
                    </Text>
                  </View>
                  <Rating
                    type="custom"
                    fractions={false}
                    defaultRating={1}
                    showRating={false}
                    style={{
                      alignSelf: "flex-start",
                      padding: 5,
                    }}
                    imageSize={38}
                    ratingBackgroundColor="#c8c7c8"
                    onFinishRating={(rate) => {
                      setRating(rate);
                      rateRider(rate, yesNoSelection, rider.name);
                    }}
                  />
                  <View
                    style={{ flexDirection: "row", width: "100%", top: 10 }}
                  >
                    <Text style={{ fontSize: 12, fontFamily: "popRegular" }}>
                      bad
                    </Text>
                    <Text
                      style={{
                        left: 120,
                        fontSize: 12,
                        fontFamily: "popRegular",
                      }}
                    >
                      very good
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        fontSize: 20,
                        fontFamily: "popSemiBold",
                        top: 20,
                      }}
                    >
                      Rubie does not ask for cash!
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        fontSize: 20,
                        fontFamily: "popSemiBold",
                        top: 20,
                      }}
                    >
                      Were you asked to do so?
                    </Text>
                    <View style={{ top: 20, flexDirection: "row" }}>
                      <TouchableOpacity
                        onPress={yes}
                        style={
                          yesPressed ? styles.btnYesPressed : styles.btnYes
                        }
                      >
                        <Text style={styles.btntxt}>yes</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={no}
                        style={noPressed ? styles.btnNoPressed : styles.btnNo}
                      >
                        <Text style={styles.btntxt}>no</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Screen>
            </>
          </ImageBackground>
        </Modal>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  ImageIconStyle: {
    margin: 15,
    width: 40,
    height: 40,
    top: 0,
  },
  emergencyDet: {
    marginTop: 90,
    borderRadius: 10,
    backgroundColor: "white",
    width: "90%",
    shadowRadius: 5,
    shadowOffset: {
      width: 5,
      height: 10,
    },
    shadowOpacity: 0.2,
    alignSelf: "center",
    flexDirection: "row",
  },
  add: {
    width: 30,
    height: 27,
    borderRadius: 5,
    backgroundColor: colors.darkGray,
    position: "absolute",
    alignSelf: "flex-start",
    left: 308,
  },
  addBlue: {
    width: 30,
    height: 27,
    borderRadius: 5,
    backgroundColor: colors.blue,
    position: "absolute",
    alignSelf: "flex-start",
    left: 308,
  },
  addIconStyle: { marginTop: 10, width: 14, height: 14, left: 8, bottom: 2 },
  emergencyImageIconStyle: {
    top: 15,
    left: 15,
    width: 130,
    height: 84,
    borderRadius: 10,
  },
  TimeIconStyle: { marginTop: 10, width: 24, height: 24 },
  emergencyDescTitle: {
    fontFamily: "popBold",
    fontSize: 20,
    paddingBottom: 10,
  },
  emergencyDesc: {
    fontFamily: "popRegular",
    fontSize: 12,
    paddingBottom: 10,
  },
  orderNow: {
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    margin: 0,
  },
  orderNowBlue: {
    backgroundColor: colors.blue,
    borderRadius: 0,
    margin: 0,
  },
  tyreSelectBtn: {
    alignContent: "flex-end",
    backgroundColor: colors.darkGray,
    borderRadius: 0,
    height: 76,
    margin: 0,
  },
  tyreSelectBtnBlue: {
    alignContent: "flex-end",
    backgroundColor: colors.blue,
    borderRadius: 0,
    height: 76,
    margin: 0,
  },
  btnYes: {
    flex: 0.4,
    backgroundColor: colors.gray,
    justifyContent: "center",
    height: 40,
    top: 15,
    borderRadius: 5,
    right: 10,
  },
  btnNo: {
    flex: 0.4,
    backgroundColor: colors.gray,
    justifyContent: "center",
    height: 40,
    top: 15,
    borderRadius: 5,
    marginLeft: "auto",
  },
  btnYesPressed: {
    flex: 0.4,
    backgroundColor: colors.blue,
    justifyContent: "center",
    height: 40,
    top: 15,
    borderRadius: 5,
    right: 10,
  },
  btnNoPressed: {
    flex: 0.4,
    backgroundColor: colors.blue,
    justifyContent: "center",
    height: 40,
    top: 15,
    borderRadius: 5,
    marginLeft: "auto",
  },
  btntxt: {
    color: colors.white,
    alignSelf: "center",
    fontFamily: "popSemiBold",
    fontSize: 24,
  },
});
