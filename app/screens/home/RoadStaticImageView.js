import React from "react";
import { Image, StyleSheet, View } from "react-native";

export default function RoadStaticImageView() {
  return (
    <View style={styles.bot}>
      <Image
        style={styles.image}
        source={require("../../assets/png/bikeRoad.png")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  bot: {
    flex: 1,
    height: 230,
    width: "100%",
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
