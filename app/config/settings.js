import Constants from "expo-constants";

const settings = {
  dev: {
    apiUrl: "http://192.168.0.19:9099/api/v1",
    publishableKey:
      "pk_test_51IsPmWHSCzYeW5gWPnhzhgPFEnlB59VWaEOmXbcdK2tgddGM6KpbDYDu9tUTllUNWvx1xsJ78XNZ08R91RdF9Hbk00zjzoIdSV",
  },
  staging: {
    apiUrl: "http://capando.com:9099/api/v1",
    publishableKey:
      "pk_test_51IsPmWHSCzYeW5gWPnhzhgPFEnlB59VWaEOmXbcdK2tgddGM6KpbDYDu9tUTllUNWvx1xsJ78XNZ08R91RdF9Hbk00zjzoIdSV",
  },
  prod: {
    apiUrl: "http://capando.com:9099/api/v1",
    publishableKey:
      "pk_test_51IsPmWHSCzYeW5gWPnhzhgPFEnlB59VWaEOmXbcdK2tgddGM6KpbDYDu9tUTllUNWvx1xsJ78XNZ08R91RdF9Hbk00zjzoIdSV",
  },
};

const getCurrentSettings = () => {
  if (__DEV__) return settings.dev;
  if (Constants.manifest.releaseChannel === "staging") return settings.staging;
  return settings.prod;
};

export default getCurrentSettings();
