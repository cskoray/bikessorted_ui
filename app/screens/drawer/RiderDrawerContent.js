import React, { useEffect } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import { Avatar, Title, Drawer } from "react-native-paper";

import { DrawerItem } from "@react-navigation/drawer";
import colors from "../../config/colors";
import profileApi from "../../api/profile";
import routes from "../../navigation/routes";
import useAuth from "../../auth/useAuth";

function DrawerContent(props) {
  const getDetailsApi = useApi(profileApi.getDetails);
  const auth = useAuth();

  useEffect(() => {
    getDetailsApi.request();
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
          <View style={{ flexDirection: "row", alignContent: "center" }}>
            <Avatar.Image
              source={{
                uri: getDetailsApi.data.profilePicture,
              }}
              size={50}
            />
            <View
              style={{
                marginLeft: 15,
                flexDirection: "column",
                alignContent: "center",
                width: "80%",
              }}
            >
              <Title style={styles.title}>{getDetailsApi.data.name}</Title>
              <View
                style={{
                  position: "absolute",
                  bottom: "1%",
                  right: "1%",
                  height: 90,
                  alignSelf: "flex-end",
                  padding: 10,
                  paddingTop: "10%",
                }}
              >
                {getDetailsApi.data.point > 0 && (
                  <Text
                    style={{
                      color: colors.newWhite,
                      fontSize: 15,
                      fontFamily: "popRegular",
                      alignSelf: "center",
                      width: "100%",
                      left: "1%",
                    }}
                  >
                    {getDetailsApi.data.badge}
                  </Text>
                )}
                <Image
                  source={{ uri: getDetailsApi.data.level }}
                  style={{
                    height: 50,
                    width: 50,
                  }}
                  resizeMode="contain"
                />
                {getDetailsApi.data.point > 0 && (
                  <Text
                    style={{
                      color: colors.newWhite,
                      fontSize: 15,
                      fontFamily: "popRegular",
                      alignSelf: "center",
                      width: "100%",
                      // padding: 5,
                      left: "1%",
                    }}
                  >
                    Rubie coins: {getDetailsApi.data.point}
                  </Text>
                )}
              </View>
            </View>
          </View>
          <Image
            source={require("../../assets/png/frame.png")}
            style={{
              height: 75,
              width: 75,
              margin: 0,
              position: "absolute",
              top: 48,
              left: 12,
            }}
            resizeMode="contain"
          />
          <View style={styles.row}></View>
        </View>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Home"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.RiderHome);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Bookings"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.RiderBookings);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Wallet"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular", top: 10 }}
            onPress={() => {
              const params = { refer: "drawer" };
              props.navigation.navigate(routes.RiderWallet, params);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section>
        {/* <Drawer.Section style={{ left: 10, top: 20 }}>
          <DrawerItem
            label="Stock Management"
            labelStyle={{ fontSize: 18, fontFamily: "popRegular" }}
            onPress={() => {
              props.navigation.navigate(routes.RiderStocks);
            }}
            inactiveTintColor={colors.newWhite}
          />
        </Drawer.Section> */}
      </View>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={() => (
            <Image
              source={require("../../assets/png/Iconly-Light-outline-Logout.png")}
              style={{ height: 15, width: 15 }}
              resizeMode="contain"
            />
          )}
          label="logout"
          labelStyle={{ fontSize: 13, fontFamily: "popRegular" }}
          onPress={() => auth.logOut()}
          inactiveTintColor={colors.newWhite}
        />
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    backgroundColor: colors.blue,
  },
  userInfoSection: {
    paddingTop: 70,
    paddingLeft: 30,
    backgroundColor: colors.darkGray,
    borderBottomColor: colors.newWhite,
    borderBottomWidth: 0.5,
  },
  title: {
    fontSize: 16,
    marginTop: 15,
    left: 10,
    fontWeight: "bold",
    color: colors.newWhite,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    color: colors.newWhite,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  bottomDrawerSection: {
    backgroundColor: colors.blue,
    height: 80,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

export default DrawerContent;
