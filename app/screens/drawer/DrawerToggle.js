import React from "react";
import { Image, TouchableOpacity, View, Text } from "react-native";
import colors from "../../config/colors";

export default function DrawerToggle(props) {
  const { navigation } = props;
  const toggleDrawer = () => {
    navigation.toggleDrawer();
  };
  return (
    <View
      style={{
        flexDirection: "row",
        flex: 0.42,
        backgroundColor: colors.darkGray,
        padding: 0,
        width: "100%",
      }}
    >
      <TouchableOpacity
        onPress={toggleDrawer}
        style={{ left: 5, width: 50, top: 15 }}
      >
        <Image
          source={require("../../assets/png/menu.png")}
          style={{
            width: "60%",
            height: "60%",
            margin: 5,
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={{ alignSelf: "center", left: 10, top: 5 }}>
        <Text
          style={{
            color: colors.newWhite,
            alignSelf: "center",
            fontFamily: "popRegular",
          }}
        >
          Why go to a bike shop? Let{" "}
          <Text
            style={{
              color: colors.newWhite,
              fontFamily: "popBold",
            }}
          >
            Rubie
          </Text>{" "}
          comes to you
        </Text>
      </View>
    </View>
  );
}
